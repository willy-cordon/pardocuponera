<?php include('/val/valuser.php'); ?>
<?//--------------------------------------------------------------------------------------------------------------
	
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
    require_once GLBRutaFUNC.'/zfvarias.php';

			
	$tmpl= new HTML_Template_Sigma();	
    $tmpl->loadTemplateFile('cupon.html');
    
    $camitm = (isset($_GET['id']))? trim($_GET['id']) : 0;


    $conn= sql_conectar();//Apertura de Conexion
   
    $percodigo 	= (isset($_SESSION[GLBAPPPORT.'PERCODIGO']))? trim($_SESSION[GLBAPPPORT.'PERCODIGO']) : '';
    $pernombre 	= (isset($_SESSION[GLBAPPPORT.'PERNOMBRE']))? trim($_SESSION[GLBAPPPORT.'PERNOMBRE']) : '';
    $perapelli 	= (isset($_SESSION[GLBAPPPORT.'PERAPELLI']))? trim($_SESSION[GLBAPPPORT.'PERAPELLI']) : '';
    $percorreo 	= (isset($_SESSION[GLBAPPPORT.'PERCORREO']))? trim($_SESSION[GLBAPPPORT.'PERCORREO']) : '';

	//--------------------------------------------------------------------------------------------------------------
	$pathimagenes='sistema/imges/data/campania/';

    $query = "SELECT CD.CAMREG,CD.CAMITM,CD.CAMARTCOD,CD.CAMARTDES,CD.CAMARTDSC,CD.CAMARTIMP,CD.CAMIMGURL,CD.CAMIMAGEN,CC.CAMVIGDDE,CC.CAMVIGHTA
                      FROM CAM_DETA CD
                      LEFT OUTER JOIN CAM_CABE CC ON CD.CAMREG=CC.CAMREG 
                      WHERE CD.CAMITM=$camitm";
            $Table = sql_query($query,$conn);

            $Table	= sql_query($query,$conn);
            for($i=0; $i<$Table->Rows_Count; $i++){
            $row	= $Table->Rows[$i];
            //CAM_DETA
            $camreg          = trim($row['CAMREG']);
            $camitm 		 = trim($row['CAMITM']);
            $camartcod 	     = trim($row['CAMARTCOD']);
            $camartdes 	     = trim($row['CAMARTDES']);
            $camartdsc 	     = trim($row['CAMARTDSC']);
            $camartimp  	 = trim($row['CAMARTIMP']);
            $camimgurl  	 = trim($row['CAMIMGURL']);
            $camimagen  	 = trim($row['CAMIMAGEN']);
            $camvigdde  	= trim($row['CAMVIGDDE']);
            $camvighta  	= trim($row['CAMVIGHTA']);



              //Cambiamos la fecha en el pormato dd/mm/aa
			$camvigdde = substr($camvigdde,8,2).'/'.substr($camvigdde,5,2).'/'.substr($camvigdde,0,4); //Fecha
			$camvighta = substr($camvighta,8,2).'/'.substr($camvighta,5,2).'/'.substr($camvighta,0,4); //Fecha
            
            //CAM_DETA
            $tmpl->setCurrentBlock('browser');	
                $tmpl->setVariable('camreg'			    , $camreg 		);
                $tmpl->setVariable('camitm'			    , $camitm 		);
                $tmpl->setVariable('camartcod'			, $camartcod 	);
                $tmpl->setVariable('camartdes'			, $camartdes 	);
                $tmpl->setVariable('camartdsc'			, $camartdsc 	);
                $tmpl->setVariable('camartimp'			, $camartimp 	);
                $tmpl->setVariable('camimgurl'			, $camimgurl 	);
                $tmpl->setVariable('camimagen'	, $pathimagenes.$camreg.'/'.$camimagen 	);
                $tmpl->setVariable('camvigdde'			, $camvigdde 	);
                $tmpl->setVariable('camvighta'			, $camvighta 	);
                
            $tmpl->parseCurrentBlock('browser');

            }

           
        sql_close($conn);	
      
      $tmpl->show();
	

