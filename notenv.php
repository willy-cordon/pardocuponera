<?php include('/val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';

    
    $JSTable	= '{"notificaciones": [ '; // nombre de la variable json
    $conn= sql_conectar();
	//Busco las notificaciones pendientes
	$query = "SELECT FIRST 1 NOTREG, NOTTITULO,NOTESTADO FROM NOT_CABE WHERE NOTESTADO = 1";
				
	$Table = sql_query($query,$conn);
	#Recorremos
	for($i=0; $i < $Table->Rows_Count; $i++){
		$row= $Table->Rows[$i];
		$notreg    = trim($row['NOTREG']); 
		$nottitulo = trim($row['NOTTITULO']);
	
		$JSTable .= '{	"notreg":"'.$notreg.'",
						"nottitulo":"'.$nottitulo.'",
						"notestado":"1"
						},';
	}
   
	//Busco las notificaciones leidas, solo 1
	$query = "SELECT FIRST 1 NOTREG, NOTTITULO,NOTESTADO FROM NOT_CABE WHERE NOTESTADO = 2";
				
	$Table = sql_query($query,$conn);
	#Recorremos
	for($i=0; $i < $Table->Rows_Count; $i++){
		$row= $Table->Rows[$i];
		$notreg    = trim($row['NOTREG']); 
		$nottitulo = trim($row['NOTTITULO']);
		
	
		$JSTable .= '{	"notreg":"'.$notreg.'",
						"nottitulo":"'.$nottitulo.'",
						"notestado":"2"
						},';
	}
	
	//if($Table->Rows_Count >0){ //Si hay registros
	$JSTable	= substr($JSTable, 0, strlen($JSTable)-1);
	//}
			
            
    $JSTable.=']}';
    echo $JSTable;
	
    sql_close($conn);
        
?>	
