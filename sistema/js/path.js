var Path_Validate	= '/sistema/val/'; 				//Ruta de Carpeta Archivos de Validacion
var Path_LookUp		= '/sistema/js/lookup/'; 		//Ruta de Carpeta de LookUp

var LkPage			= 10;				//Cantidad de Registros por Pagina
var WinCant			= 9;				//Cantidad de Ventanas Internas Abiertas
var WinMinWidth		= 80;				//Ancho de Ventana minimizada

var objSetFocus 	= null;
setTimeout("highLightFocus();",50);	

//-------------------------------------------------------------------------------------
function KeyEnter(e,objdst,Fnc){ //OBJETO SIGUIENTE CON "ENTER"
	if (!e) var e = window.event
  	if (e.keyCode) code = e.keyCode;
   	else if (e.which) code = e.which;   	
	if(code==13){
		if(objdst)			
			objdst.focus();		
		if(Fnc)
			eval(Fnc);
	}
}
//-------------------------------------------------------------------------------------
function KeyEnterExe(e,scr){ //EJECUCION DE SCRIPT CON "ENTER"
	if (!e) var e = window.event
  	if (e.keyCode) code = e.keyCode;
   	else if (e.which) code = e.which;   	
	if(code==13){
		eval(scr);
		return false;
	}
}
//-------------------------------------------------------------------------------------
function getUrlVars(){ //Leer variables de Url
	var vars = [], hash;
	var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	
	for(var i = 0; i < hashes.length; i++){
		hash = hashes[i].split('=');
		vars.push(hash[0]);
		vars[hash[0]] = hash[1];
	}
	return vars;
}
//-------------------------------------------------------------------------------------
function ViewMsg(dato){ //Seteo texto en barra de status
	window.top.Ext.example.msg('', dato, '', '');
}
//-------------------------------------------------------------------------------------
function roll(obj, highlightcolor, textcolor){
    obj.style.backgroundColor 	= highlightcolor;
    obj.style.color 			= textcolor;
}
//-------------------------------------------------------------------------------------
function OpenView(path_abm, winid, wintitle, winwidth, winheight, winmax, winmin, winmaxim){
	var desktop = window.top.MyDesktop.desktop;
	
	var win 	= window.top.wingroup.get(winid);
	
	max		= false; //Boton Maximizar
	min		= false; //Boton Minimizar
	maxim 	= false; //Ventana Maximizada
	
	if(winmax 	== 1) max 	= true;
	if(winmin 	== 1) min 	= true;
	if(winmaxim == 1) maxim = true;
	
	winwidth = parseInt(winwidth);
	winheight = parseInt(winheight);
	
    if(!win){
    	var auxDate = new Date();
    	var idaux = auxDate.getHours()+''+auxDate.getMinutes()+''+auxDate.getSeconds();
    	winid = winid.replace('GENID',idaux);    	
    	
    	if(path_abm.indexOf('?') == -1){
    		path_abm += '?W='+winid;    		
    	}else{
    		path_abm += '&W='+winid;
    	}

		var jsWinOpen = { 	
							"winid": 	winid,
							"title": 	wintitle,
							"width": 	winwidth,
							"height": 	winheight,
							"html": 	path_abm,
							"min": 		min,
							"max":		max,
							"maxim": 	maxim
						}

		localStorage["Benvido.WinOpen"] = JSON.stringify(jsWinOpen);
		
		win = desktop.createWindow({
			id			: winid,
			manager		: window.top.wingroup,
	        title		: wintitle,
	        width       : winwidth,
	        height      : winheight,
	        plain       : true, 
			minimizable	: min,
			maximizable	: max,
			maximized	: maxim,
	        html		: "<iframe src='"+path_abm+"' style='width:100%; height:100%;' frameborder=0 scrolling=no></iframe>"
	    });
    }
	win.show();				
}
//-------------------------------------------------------------------------------------
function CloseView(winid){		
	var win = window.top.wingroup.get(winid);
	if(win)
		win.close();
}
//-------------------------------------------------------------------------------------
function RefreshBrw(vent, ventAnt){		
	localStorage.clear();
	var winant = window.top.wingroup.get(ventAnt);	
	if(winant){
		var formulario = winant.body.dom.firstChild.contentWindow.FrmBsq;
		var pagina 	= winant.body.dom.firstChild.contentWindow;		
		if(formulario) formulario.submit();	
	}
	win = window.top.wingroup.get(vent);
	if(win){
		win.close();
	}
}
//-------------------------------------------------------------------------------------
function RefreshWind(vent){		
	win = window.top.wingroup.get(vent);
	if(win){
		win.body.dom.firstChild.contentDocument.location.reload(true);
	}
}
//-------------------------------------------------------------------------------------
function getDocument(vent){
	var winant 	= window.top.wingroup.get(vent);
	var wdoc 	= null;
	if(winant){
		var wdoc = winant.body.dom.firstChild.contentWindow;
	}
	return wdoc;
}
//-------------------------------------------------------------------------------------
function PrintPage(){
	$('#tableroexpo').hide();
	window.print();
	$('#tableroexpo').show();
}
//-------------------------------------------------------------------------------------
function RepExport(frm,tipo){
	var jfrm = $(frm);
	jfrm.find('#reptipo').val(tipo);
	jfrm.submit();
}
//-------------------------------------------------------------------------------------
function ViewParam(){
	var filtros = $('#filtros');
	
	if(typeof filtros.data('view') === "undefined"){
		filtros.data('view',false);
		filtros.hide();
	}else{	
		if(filtros.data('view') == true){
			filtros.data('view',false);
			filtros.hide();
		}else{
			filtros.data('view',true);
			filtros.show();
		}
	}
	
	$('.rpt_brw_detalle td').show();
	$('.rpt_brw_titulos th').show();
}
//-------------------------------------------------------------------------------------
function Paginar(opc,frm,pagFil){ //PAGINACION - Funcion para Paginar Browsers
	//Si selecciono ANT (retroceso), pero estoy en el inicio, no hago nada
	if(opc == 'ANT' && parseInt(frm.incre.value) == 0) return 0;
	
	opcVal = 1; //Opcion "Siguiente" - Avanzar (SIG)
	if(opc == 'ANT') opcVal = -1; //Opcion "Anterior" - Retroceder (ANT)

	//Controlo si hubo registros para continuar avanzando
	if(parseInt(frm.cntrow.value) == pagFil || opc == 'ANT'){
		frm.incre.value = parseInt(frm.incre.value) + (pagFil*opcVal);
		frm.submit();
	}			
}
//-------------------------------------------------------------------------------------
function TblOrder(field,Frm){	
	Frm.sorton.value = field;
	if (Frm.sortby.value == 'ASC'){
		Frm.sortby.value = 'DESC'
	}else{
		Frm.sortby.value = 'ASC'
	}

	Frm.submit();
}
//-------------------------------------------------------------------------------------
function OpenRepSis(Frm, id){
	var deviceType = localStorage.getItem("Benvido.DeviceType");
	
	if(deviceType == 'Phone'){
		OpenRep(Frm);
	}else{	
		var desktop 	= window.top.MyDesktop.desktop;
		var path 		= $(Frm).attr('action');
		var wintitle 	= $(Frm).find('#wintitle').val();
		var winpath 	= $(Frm).find('#winpath').val();
		var auxDate 	= new Date();
		var idaux 		= auxDate.getHours()+''+auxDate.getMinutes()+''+auxDate.getSeconds(); 
		var winname 	= 'ifr'+idaux;
		if(winpath != undefined){
			path = winpath+'/'+path;
		}
		
		winid = '00'+idaux;
		if(id != undefined) winid=id;
		
		win = desktop.createWindow({
			id			: winid,
			manager		: window.top.wingroup,
			title		: wintitle,
			width       : 800,
			height      : 500,
			plain       : false, 
			minimizable	: true,
			maximizable	: true,
			html		: "<div style='width:109%; height:100%;overflow:auto;-webkit-overflow-scrolling:touch'><iframe id='rep' name='"+winname+"' src='/sistema/blank.html' style='width:93%; height:100%;' frameborder=0 scrolling='auto'></iframe></div>"
		});
		
		$(Frm).attr('target',winname);
		$(Frm).submit();
		win.show();
	}
}
function OpenRep(Frm){			
	var date 	= new Date();
	var tim		= date.getMinutes()+''+date.getSeconds()+''+date.getMilliseconds();
	var page 	= Frm.action;
	var target	= Frm.target + tim;
	
	Frm.target = target;				
	win = window.open('',target,'');		
	Frm.submit();
}
//-------------------------------------------------------------------------------------
function convNumJs(valor){
	if(valor == '') valor='0';
	valor = valor.replace('.','');
	valor = valor.replace(',','.');
	return valor;
}
//-------------------------------------------------------------------------------------
function getTableJSON(idtable){ //Convierto la Tabla a JSON
	/*
	 {"row": 
		[ {	"index":"0",
			"content":{
				"campo1":"valor1", 
				"campo2":"valor2"
				}
			},
			{	"index":"1",
				"content":{
					"campo1":"valor4", 
					"campo2":"valor5"
					}
				} 
		], "rows":"0"};
	*/
	var rows 	= 	$("#" + idtable + " tbody tr");
	var tbljson = '{"row": [ ';
	var i 		= 0;
				
	rows.each(function (index) { //Recorro las Filas
		var fields = $(this).find('input');

		tbljson += '{ "index":"' + index +'", "content":{ '; 				
		fields.each(function (i, field){ //Recorro los Inputs			
			if(field.hasAttribute("datasend") == false || $(field).attr('datasend') == 'true'){				
				var name = field.name;				
				if(name.indexOf('1000') != -1){						
					pos = name.indexOf('1000');
					name = name.substring(0,pos); //Extraigo los regtable 100000
				}
				if(isJson(field.value)){
					tbljson += '"' + name + '":' + field.value + ' ,';
				}else{
					tbljson += '"' + name + '":"' + field.value + '",';	
				}				
			}
		});
		tbljson = tbljson.substr(0,tbljson.length-1);
		tbljson += '}},';
		i++; //Cantidad de Filas
	});
	tbljson = tbljson.substr(0,tbljson.length-1);
	tbljson += ' ], "rows":"'+i+'"}';
	
	return tbljson;
}

function esPar($numero){ 
   $resto = $numero%2; 
   if (($resto==0) && ($numero!=0)) { 
        return true 
   }else{ 
        return false 
   }
}   
//-------------------------------------------------------------------------------------
function checkOpcion(obj,objdest){
	if(obj.checked==true){
		$('#'+objdest).val('S'); 
	}else{
		$('#'+objdest).val('N'); 
	}
}
//-------------------------------------------------------------------------------------------
function highLightFocus(){
	/*if(countSetFocus < 50){
		if(objSetFocus.is( ":focus" )){
			if(objSetFocus.val().length > 0){
				objSetFocus.select();
				objSetFocus = null;
			}else{
				setTimeout("highLightFocus();",10);
				countSetFocus++;
			}
		}else{
				setTimeout("highLightFocus();",10);
				countSetFocus++;
			}
	}*/
	if(objSetFocus != null){
		if(objSetFocus.is( ":focus" )){
			if(objSetFocus.val().length > 0){
				objSetFocus.select();
				/*var textsel = window.getSelection().toString()
				if(textsel.length > 0){				
					objSetFocus=null;
				}*/
			}
		}
	}
	setTimeout("highLightFocus();",100);
}
//-------------------------------------------------------------------------------------
function keyFuncPrincipal(){
	if (!e) var e = window.event;
	if (e.keyCode) code = e.keyCode;
	else if (e.which) code = e.which;   	
	//code =  F1:112, F3:114, F4:115, F5:116, F6:117, F7:118, F8:119, F9:120, F10:121, F11:122, F12:123	
	
	switch(code){
		case 115: 
			//Abro la ultima ejecucion
			var jsWinOpen = JSON.parse(localStorage["Benvido.WinOpen"]);			
			OpenView(jsWinOpen.html, jsWinOpen.winid, jsWinOpen.title, jsWinOpen.width, jsWinOpen.height, jsWinOpen.max, jsWinOpen.min, jsWinOpen.maxim);
			
			return false; 
			break; //F4
	}	
}
//-------------------------------------------------------------------------------------
function valLetra(obj){
	var rx= new RegExp('[A-Z]'); 
	if(!rx.test(obj.value)){
		obj.value=''; 
		ViewMsg('Letra incorrecta.');
		obj.focus();
	} 
}
//-------------------------------------------------------------------------------------
function isJson(item) {
    item = typeof item !== "string"
        ? JSON.stringify(item)
        : item;

    try {
        item = JSON.parse(item);
    } catch (e) {
        return false;
    }

    if (typeof item === "object" && item !== null) {
        return true;
    }

    return false;
}
//-------------------------------------------------------------------------------------