//---------------------------------------------------------------------------------------------------
function CtrFormsValues(Frm){ //Control de Valores Oblig. en Formularios
	// Frm.elements[i].name -  Frm.elements[i].type - Frm.elements[i].value
	
	var frmerr 	= 0;
	var celem 	= Frm.elements.length; //Cantidad de Elementos
		
	//Recorro todos los Elementos del Form
	for (i=0 ; i < celem ; i++)	{		
		var elem = Frm.elements[i];
		
		if(elem.getAttribute('ctrlerr')){ //Atributo de Control
			if(elem.value == '' || elem.value == 0){ //Si esta vacio
				frmerr = 1; //Error
				i = celem;
				ViewMsg(elem.getAttribute('ctrlerr'));
				//alert(elem.getAttribute('ctrlerr')); //Mensaje de Error	
				//si exite el div que bloquea lo oculto
							
				elem.focus();				
			}
		} //fin - if(elem.getAttribute('ctrlerr'))		
	}	

	if(frmerr ==1){
		if($("#dvblqpantalla").length){
			$("#dvblqpantalla").hide();
		}
		if($("#dvbloq").length){
			$("#dvbloq").hide();
		}
	}
	
	return frmerr;
}
//---------------------------------------------------------------------------------------------------
