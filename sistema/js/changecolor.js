
//***************************************************************************************
function BsqWin(Form){
      window.open("","BsqWin","toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, copyhistory=no, width=350, height=535");
      Form.submit();      
}
//***************************************************************************************

//color cuando tome el foco
var color_con_foco = '#FFFF93';
//color cuando pierde el foco
var color_sin_foco = '#ffffff';
//corea el elemento que genera el evento, si es coloreable
function marcar(e) {
   if ( window.event != null)               //IE4+
      eventobj = window.event.srcElement;
   else if ( e != null )                   //N4+ o W3C compatibles
      eventobj = e.target;
   else
      return; 
   if(eventobj.style != null){
	   color_sin_foco= eventobj.style.backgroundColor; //guardo el color anterior
	   eventobj.style.backgroundColor = color_con_foco;
   }   

}
//corea el elemento que genera el evento, si es coloreable
function desmarcar(e) {
   if ( window.event != null)               //IE4+
      eventobj = window.event.srcElement;
   else if ( e != null )                   //N4+ o W3C compatibles
      eventobj = e.target;
   else
      return;
   
   eventobj.style.backgroundColor = color_sin_foco;   
   
}   
if (document.captureEvents) {                           //N4 requiere invocar la funcion captureEvents
   document.captureEvents(Event.FOCUS | Event.BLUR | Event.LOAD)
}
function ChgColorIni(frm){
   for (c = 0; c < frm.length; c++) {
	  if(frm.elements[c].type!='button'){
	      if(!frm.elements[c].onfocus)
	         frm.elements[c].onfocus =  marcar;      //manejador para onFoucs            
	      if(!frm.elements[c].onblur) //Si ya tiene un evento no lo coloco, se coloca manualmente
	          frm.elements[c].onblur = desmarcar;      //manejador para onBlur
	  }
      
   }     
}
