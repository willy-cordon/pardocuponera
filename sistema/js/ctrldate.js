function CtrlDate(Obj,e){    
   if (!e) var e = window.event
   if (e.keyCode) code = e.keyCode;
   else if (e.which) code = e.which;
     
  //delete, backspace, enter, up, down, left, right, tab, escape, inicio, fin  
  if((code==46||code==8||code==13||code==38||code==40||code==37||code==39||code==9||code==27||code==36||code==35||code==0))
      return true;       
  else if(("#$%&()'.").indexOf(String.fromCharCode(code)) > -1)
        return false;        
  else if(code<47 || code>57) //numeros 48-57, 47 barra
        return false; 
    
  var Largo= Obj.value.length;
  var Inicio= Obj.selectionStart;
  var Fin= Obj.selectionEnd;
  
  
  if(Largo == Inicio+Fin && Largo==10) //Limpio la Seleccion Completa
  	Obj.value='';
  
  if((Largo==2 || Largo==5) && code!=47){ //dia y mes (distinto de presion de la barra)
     Obj.value += '/';
     return true; 
  }else if((Largo>9 && Inicio==10) || (Largo>9 && Fin==Inicio)){
      return false;  
  }else if (Largo>9 && Fin!=Inicio){             
      for(var i=Inicio; i<Fin; i++){        
        if(Obj.value.substr(i,1)=='/')
          return false;  
      }
  }

  if (code==47){ //controlo que solo hayan 2 barras (solo si se presiona la barra)            
      if (Inicio==2 || Inicio==5){
          cont=0;
          for(var i=0; i<Largo; i++)
             if(Obj.value.substr(i,1)=='/') cont++;               
          if(cont>1){          
          	return false;
          }	
      }else{       	
        return false;
      }    
  }else if (Inicio==2 || Inicio==5) //para que sea numero debe ser distinto de la pos. de la barra
        return false;    
    
  //Valido la posicion del mes    
  //alert(Inicio+'-'+Largo)
  if(Inicio==9||Inicio==7||Inicio==4||Inicio==3||Inicio==1||Inicio==0){  
      var tmp= Obj.value.split('/');
      
      var Mes='';
      var Ano=''; //A�O
      if(tmp.length>1) Mes= tmp[1]; //MES
      if(tmp.length>2) Ano= tmp[2]; //A�O
      var Dia= tmp[0]; //DIA
      var Bisiesto=0; //Si es ano bisiesto
     
      if(Ano!=''){
      	if(Ano.length==1 && Ano!='2'){
      	    Ano += String.fromCharCode(code);
      	    Ano='20'+Ano;
      	}   
      	if(Ano.length==3)
      	    Ano += String.fromCharCode(code);      	    
      	
	    if(Dia>28 && Mes==2 && Ano.length==4){ //Verifico Ano bisiesto
		 	if((Ano % 4 == 0) && ((Ano % 100 != 0) || (Ano % 400 == 0))) 
    	  		Bisiesto=1; //Si
    	   	else
    	  		Bisiesto=2; //No
	    }
	    
      }
                  
      if (Mes!=''){
          if(Mes.length==1 && Inicio==3) Mes= String.fromCharCode(code)+Mes;      
          else if(Mes.length==1) Mes += String.fromCharCode(code);
          if(Dia.length==1 && Inicio==0) Dia= String.fromCharCode(code)+Dia;
          else if(Dia.length==1) Dia += String.fromCharCode(code);
                     
          if (Mes>12 || Mes==0){         
             Obj.value= Obj.value.substr(0,3);
             ViewMsg('Error en el Mes Ingresado. ('+Mes+')');
             return false;
          }
          
          //valido el dia, segun el mes
          if (Dia==0){
             Obj.value='';
             ViewMsg('Error en el Dia '+Dia+' para el Mes '+Mes);
             return false;
          }else if (Mes==2 && Dia>28 && Bisiesto==2){ //Mes de 28
             Obj.value='';
             ViewMsg('Error en el Dia '+Dia+' para el Mes '+Mes+'. El Mes tiene 28 Dias para el '+Ano);
             return false;
		  }else if (Mes==2 && Dia>29 && Bisiesto==1){ //Mes de 29
             Obj.value='';
             ViewMsg('Error en el Dia '+Dia+' para el Mes '+Mes+'. El Mes tiene 29 Dias para el '+Ano);
             return false;   
          }else if ((Mes==4||Mes==6||Mes==9||Mes==11) && Dia>30){ //Dias 30
             Obj.value='';
             ViewMsg('Error en el Dia '+Dia+' para el Mes '+Mes+'. El Mes tiene 30 Dias.');
             return false;
          }      
          else if ((Mes==1||Mes==3||Mes==5||Mes==7||Mes==8||Mes==10||Mes==12) && Dia>31){ //Dias 31
             Obj.value='';
             ViewMsg('Error en el Dia '+Dia+' para el Mes '+Mes+'. El Mes tiene 31 Dias.');
             return false;
          }
      }
  }
}

function ultDiaMes(Mes,Ano){
	var UltDia = 0;
	var Bisiesto=0; //Si es ano bisiesto
	
	if(Mes==2){ //Verifico Ano bisiesto
		if((Ano % 4 == 0) && ((Ano % 100 != 0) || (Ano % 400 == 0))) 
			Bisiesto=1; //Si
		else
			Bisiesto=2; //No
	}
	
	if(Mes==2 && Bisiesto==2){ //Febrero
		UltDia = 28;
	}else if (Mes==2 && Bisiesto==1){ //Febrero Bisiesto
		UltDia = 29;
	}else if (Mes==4||Mes==6||Mes==9||Mes==11){ //Dias 30
		UltDia = 30;
	}else if (Mes==1||Mes==3||Mes==5||Mes==7||Mes==8||Mes==10||Mes==12){ //Dias 31
		UltDia = 31;
	}	
	
	return UltDia;
}
