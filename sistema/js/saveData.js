function saveDataForm(){
	/*localStorage.clear();
	var pedidoCabecera 	= null;
	var pedidoDetalle 	= {"detalle":[]};
	
	var clicodint 		= $('#clicodint').val();
	var clicodigo 		= $('#clicodigo').val();
	var clidescri 		= $('#clidescri').val();
	var cligrpcod 		= $('#cligrpcod').val();
	var cliopcrut 		= $('#cliopcrut').val();
	var clivencod 		= $('#clivencod').val();
	var clivendes 		= $('#clivendes').val();
	var cliimpdoc 		= $('#cliimpdoc').val();
	var tipvtaaux 		= $('#tipvtaaux').val();
	var artlstcuo 		= $('#artlstcuo').val();
	var clicontac 		= $('#clicontac').val();
	var pedleyen1 		= $('#pedleyen1').val();
	var tipvtacod 		= $('#tipvtacod').val();
	var artlstint 		= $('#artlstint').val();
	var sellstcli 		= $('#sellstcli').val();
	var vencodigo 		= $('#vencodigo').val();
	var vendescri 		= $('#vendescri').val();
	var pedobserv 		= $('#pedobserv').val();
	var pedfch 			= $('#pedfch').val();
	var succodigo 		= $('#succodigo').val();
	var tipdoccod 		= $('#tipdoccod').val();
	var pednro	 		= $('#pednro').val();
	var impopc	 		= $('#impopc').val();
	var sucimpcrg		= $('#sucimpcrg').val();
	var sucstkgenopc	= $('#sucstkgenopc').val();
	var sucimpstkopc	= $('#sucimpstkopc').val();
	var sucstkgen		= $('#sucstkgen').val();
	var sucimpstk		= $('#sucimpstk').val();
	var tipdoccre		= $('#tipdoccre').val();
	var stkdepdde		= $('#stkdepdde').val();
	var pedfinan		= $('#pedfinan').val();
	var peditmtot		= $('#peditmtot').val();
	var pedsubtot		= $('#pedsubtot').val();
	var peddscpie		= $('#peddscpie').val();
	var pedivatot		= $('#pedivatot').val();
	var pedimptot		= $('#pedimptot').val();
	var pedtotal		= $('#pedtotal').val();
		
	pedidoCabecera = {  "clicodint":clicodint,
						"clicodigo":clicodigo,
						"clidescri":clidescri,
						"cligrpcod":cligrpcod,		
						"cliopcrut":cliopcrut,		
						"clivencod":clivencod,		
						"clivendes":clivendes,		
						"cliimpdoc":cliimpdoc,		
						"tipvtaaux":tipvtaaux,		
						"artlstcuo":artlstcuo,		
						"clicontac":clicontac,		
						"pedleyen1":pedleyen1,		
						"tipvtacod":tipvtacod,		
						"artlstint":artlstint,		
						"sellstcli":sellstcli,		
						"vencodigo":vencodigo,		
						"vendescri":vendescri,		
						"pedobserv":pedobserv,		
						"pedfch":	pedfch,	
						"succodigo":succodigo,		
						"tipdoccod":tipdoccod,		
						"pednro":	pednro,	 		
						"impopc":	impopc, 		
						"sucimpcrg":sucimpcrg,		
						"sucstkgenopc":sucstkgenopc,
						"sucimpstkopc":sucimpstkopc,	
						"sucstkgen":sucstkgen,	
						"sucimpstk":sucimpstk,	
						"tipdoccre":tipdoccre,	
						"stkdepdde":stkdepdde,	
						"pedfinan":pedfinan,	
						"peditmtot":peditmtot,	
						"pedsubtot":pedsubtot,	
						"peddscpie":peddscpie,	
						"pedivatot":pedivatot,	
						"pedimptot":pedimptot,	
						"pedtotal": pedtotal	
					};
										
	$.each($('input[id="artcodigo"][value!=""]'),function(){
		var row 			= $(this).parent().parent();
		var artcodint 		= row.find('#artcodint').val();
		var artcodigo		= row.find('#artcodigo').val();
		var artdescri		= row.find('#artdescri').val();
		var regtable		= row.find('#regtable').val();
		var blqline			= row.find('#blqline').val();
		var datchg			= row.find('#datchg').val();
		var artimpinc		= row.find('#artimpinc').val();
		var pedcnttot		= row.find('#pedcnttot').val();
		var cbocodint		= row.find('#cbocodint').val();
		var artstkmin		= row.find('#artstkmin').val();
		var artstkmax		= row.find('#artstkmax').val();
		var stksldfis		= row.find('#stksldfis').val();
		var artstkctr		= row.find('#artstkctr').val();
		var artstkfac		= row.find('#artstkfac').val();
		var stkcnt			= row.find('#stkcnt').val();
		var peditmlstint	= row.find('#peditmlstint').val();
		var peditmlstitm	= row.find('#peditmlstitm').val();
		var pedmodpre		= row.find('#pedmodpre').val();
		var stkdepret		= row.find('#stkdepret').val();
		var pedcodbar		= row.find('#pedcodbar').val();
		var artcodori		= row.find('#artcodori').val();
		var pedcantid		= row.find('#pedcantid').val();
		var pedcantidaux	= row.find('#pedcantidaux').val();
		var unicodigo		= row.find('#unicodigo').val();
		var unidescri		= row.find('#unidescri').val();
		var pedprecio		= row.find('#pedprecio').val();
		var pedivapor		= row.find('#pedivapor').val();
		var peddscmto		= row.find('#peddscmto').val();
		var pedincmto		= row.find('#pedincmto').val();
		var peddscpor		= row.find('#peddscpor').val();
		var pedlinnet		= row.find('#pedlinnet').val();
		var pedlintot		= row.find('#pedlintot').val();
		var pedlintotaux	= row.find('#pedlintotaux').val();
				
		var jsondet = { "artcodint":artcodint,
						"artcodigo":artcodigo,
						"artdescri":artdescri,
						"regtable":regtable,
						"blqline":blqline,
						"datchg":datchg,
						"artimpinc":artimpinc,
						"pedcnttot":pedcnttot,
						"cbocodint":cbocodint,
						"artstkmin":artstkmin,
						"artstkmax":artstkmax,
						"stksldfis":stksldfis,
						"artstkctr":artstkctr,
						"artstkfac":artstkfac,
						"stkcnt":stkcnt,
						"peditmlstint":peditmlstint,
						"peditmlstitm":peditmlstitm,
						"pedmodpre":pedmodpre,
						"stkdepret":stkdepret,
						"pedcodbar":pedcodbar,
						"artcodori":artcodori,
						"pedcantid":pedcantid,
						"pedcantidaux":pedcantidaux,
						"unicodigo":unicodigo,
						"unidescri":unidescri,
						"pedprecio":pedprecio,
						"pedivapor":pedivapor,
						"peddscmto":peddscmto,
						"pedincmto":pedincmto,
						"peddscpor":peddscpor,
						"pedlinnet":pedlinnet,
						"pedlintot":pedlintot,
						"pedlintotaux":pedlintotaux
					  };
		
		pedidoDetalle.detalle.push(jsondet);	
	});
	
	localStorage["Pedido.Cabecera"] = JSON.stringify(pedidoCabecera);
	localStorage["Pedido.Detalle"] 	= JSON.stringify(pedidoDetalle);
	*/
}

function loadDataForm(){
/*
	if(localStorage.length>0){		
		ViewMsg('Pedido Restaurado...');
		var pedidoCabecera 	= JSON.parse(localStorage["Pedido.Cabecera"]);
		var pedidoDetalle 	= JSON.parse(localStorage["Pedido.Detalle"]);
		
		$('#clicodint').val(pedidoCabecera.clicodint);
		$('#clicodigo').val(pedidoCabecera.clicodigo);
		$('#clidescri').val(pedidoCabecera.clidescri);
		EjecutarFinLKCli();		
		$('#cligrpcod').val(pedidoCabecera.cligrpcod);
		$('#cliopcrut').val(pedidoCabecera.cliopcrut);
		$('#clivencod').val(pedidoCabecera.clivencod);
		$('#clivendes').val(pedidoCabecera.clivendes);
		$('#cliimpdoc').val(pedidoCabecera.cliimpdoc);
		$('#tipvtaaux').val(pedidoCabecera.tipvtaaux);
		$('#artlstcuo').val(pedidoCabecera.artlstcuo);
		$('#clicontac').val(pedidoCabecera.clicontac);
		$('#pedleyen1').val(pedidoCabecera.pedleyen1);
		$('#tipvtacod').val(pedidoCabecera.tipvtacod);
		$('#artlstint').val(pedidoCabecera.artlstint);
		$('#sellstcli').val(pedidoCabecera.sellstcli);
		$('#vencodigo').val(pedidoCabecera.vencodigo);
		$('#vendescri').val(pedidoCabecera.vendescri);
		$('#pedobserv').val(pedidoCabecera.pedobserv);
		$('#pedfch').val(pedidoCabecera.pedfch);
		$('#succodigo').val(pedidoCabecera.succodigo);
		$('#tipdoccod').val(pedidoCabecera.tipdoccod);
		$('#pednro').val(pedidoCabecera.pednro);
		$('#impopc').val(pedidoCabecera.impopc);
		$('#sucimpcrg').val(pedidoCabecera.sucimpcrg);
		$('#sucstkgenopc').val(pedidoCabecera.sucstkgenopc);
		$('#sucimpstkopc').val(pedidoCabecera.sucimpstkopc);
		$('#sucstkgen').val(pedidoCabecera.sucstkgen);
		$('#sucimpstk').val(pedidoCabecera.sucimpstk);
		$('#tipdoccre').val(pedidoCabecera.tipdoccre);
		$('#stkdepdde').val(pedidoCabecera.stkdepdde);
		$('#pedfinan').val(pedidoCabecera.pedfinan);
		$('#peditmtot').val(pedidoCabecera.peditmtot);
		$('#pedsubtot').val(pedidoCabecera.pedsubtot);
		$('#peddscpie').val(pedidoCabecera.peddscpie);
		$('#pedivatot').val(pedidoCabecera.pedivatot);
		$('#pedimptot').val(pedidoCabecera.pedimptot);
		$('#pedtotal').val(pedidoCabecera.pedtotal);
			
		$.each(pedidoDetalle.detalle, function(i){
			$('input[id="artcodint"]').eq(i).val( this.artcodint );
			$('input[id="artcodigo"]').eq(i).val( this.artcodigo );			
			$('input[id="artdescri"]').eq(i).val( this.artdescri );
			$('input[id="regtable"]').eq(i).val( this.regtable );
			$('input[id="blqline"]').eq(i).val( this.blqline );
			$('input[id="datchg"]').eq(i).val( this.datchg );
			$('input[id="artimpin"]').eq(i).val( this.artimpinc	);
			$('input[id="pedcntto"]').eq(i).val( this.pedcnttot	);
			$('input[id="cbocodin"]').eq(i).val( this.cbocodint	);
			$('input[id="artstkmi"]').eq(i).val( this.artstkmin	);
			$('input[id="artstkma"]').eq(i).val( this.artstkmax	);
			$('input[id="stksldfi"]').eq(i).val( this.stksldfis	);
			$('input[id="artstkct"]').eq(i).val( this.artstkctr	);
			$('input[id="artstkfa"]').eq(i).val( this.artstkfac	);
			$('input[id="stkcnt"]').eq(i).val( this.stkcnt );
			$('input[id="peditmlstint"]').eq(i).val( this.peditmlstint );
			$('input[id="peditmlstitm"]').eq(i).val( this.peditmlstitm );
			$('input[id="pedmodpre"]').eq(i).val( this.pedmodpre );
			$('input[id="stkdepret"]').eq(i).val( this.stkdepret );
			$('input[id="pedcodbar"]').eq(i).val( this.pedcodbar );
			$('input[id="artcodori"]').eq(i).val( this.artcodori );
			$('input[id="pedcantid"]').eq(i).val( this.pedcantid );
			$('input[id="pedcantidaux"]').eq(i).val( this.pedcantidaux );
			$('input[id="unicodigo"]').eq(i).val( this.unicodigo );
			$('input[id="unidescri"]').eq(i).val( this.unidescri );
			$('input[id="pedprecio"]').eq(i).val( this.pedprecio );
			$('input[id="pedivapor"]').eq(i).val( this.pedivapor );
			$('input[id="peddscmto"]').eq(i).val( this.peddscmto );
			$('input[id="pedincmto"]').eq(i).val( this.pedincmto );
			$('input[id="peddscpor"]').eq(i).val( this.peddscpor );
			$('input[id="pedlinnet"]').eq(i).val( this.pedlinnet );
			$('input[id="pedlintot"]').eq(i).val( this.pedlintot );
			$('input[id="pedlintotaux"]').eq(i).val( this.pedlintotaux );
		
		});		
	}
	*/
}