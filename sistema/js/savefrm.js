//----------------------------------------------------------------------------------------------------
function RespuestaXML(){
  /* Formato:
      <respuesta>
          <errcod></errcod> 
          <msg></msg>
      </respuesta>    
  */     
  /*if(XMLHttp.readyState == 1) //Para pasar las variables por POST
      XMLHttp.setRequestHeader('Content-Type','application/x-www-form-urlencoded'); */ 
	
  if (XMLHttp.readyState == 4){    
        var respuesta	= XMLHttp.responseText;       
        var msg			= ''; 
        var errcod		= 0;
        var errmsg		= '';
        var sqlerr		= '';
        var screxe		= '';
        var xmlobjerr	= '';
        var xmlobj		= '';
        
        //Limpio el mensaje de error
        if(respuesta.indexOf('<errdb>') != -1){ //Verifico si hay error, y limpio
        	posinierrdb = respuesta.indexOf('<errdb>');
            posfinerrdb = respuesta.indexOf('</errdb>')+8;
            errdb 		= respuesta.substr(posinierrdb,posfinerrdb-posinierrdb);
            var xmlobjerr = StrToXml(errdb);
        }else{        	
        	var xmlobj = StrToXml(respuesta);
        }                
        
        //----------------------------------------------------------------------------------------   
        if(!XMLCtrError(xmlobjerr)){ //Evaluo si es Error de BD
        	if(!XMLCtrConfirm(xmlobj)){ //Evaluo si es pagina de Confirmacion
        	 	var xmlobj = StrToXml(respuesta);
        		
				for(i=0; i < xmlobj.getElementsByTagName('respuesta').length; i++){
						var obj = xmlobj.getElementsByTagName('respuesta')[i];
						
						for(j=0; j < obj.childNodes.length; j++){
							var node = obj.childNodes.item(j);	
							
	  						if(node.nodeName == 'msg'){
	  							if(node.childNodes.length > 0){	  								
	  								var msg = node.childNodes.item(i).data; //Mensaje
	  							}
	  						}else if(node.nodeName == 'errcod'){
	  							if(node.childNodes.length > 0){
	  								var errcod = node.childNodes.item(i).data; //Err Codigo
		  						}else{
	  								alert('SaveFrm: XML - errcod - null')
	  							}
	  						}else if(node.nodeName == 'screxe'){
	  							if(node.childNodes.length > 0){
	  								var screxe = node.childNodes.item(i).data; //Script de Ejecucion Correcta
		  						}
	  						}
						}
				}
			
				msg = msg.replace(/^\s*|\s*$/g,"");
			
				if(msg==''){ 
					$('#dvblqpantalla').hide(2000);
					alert('SaveFrm-Error: '+respuesta);	 //si ocurre cualquier tipo de error.
				}
				else if (msg!='sin_msg')  ViewMsg(msg);  //alert(msg);		
				
				if(screxe!=''){						
					screxe= screxe.replace('...','&');
					eval(screxe);
				}
			
        	}//Fin Confirm   
        	
        }//Fin Error sql
  }

}
//----------------------------------------------------------------------------------------------------
function Save(Frm,Pagina){   
  XMLHttp= CrearInstancia();    
  if (XMLHttp){
    url= Pagina;    
    XMLHttp.onreadystatechange = RespuestaXML;    
    XMLHttp.open('POST', url, true);

   var Datos=''; 
   for(i=0; i<Frm.length; i++){
       if (Frm[i].name!='')
	   Datos += Frm[i].name +'='+ Frm[i].value +'&';
   } 
      
   Datos = Datos.replace(/\+/g, '#MAS#');
   
   //XMLHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');
   XMLHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');  
   XMLHttp.send(Datos);     
  }
}
//----------------------------------------------------------------------------------------------------
