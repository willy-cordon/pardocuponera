function CrearInstancia(){
  XMLHttp= false;
  var navegador= navigator.appName;
  
  if (window.XMLHttpRequest){
      return new XMLHttpRequest();
  }else if (window.ActiveXObject){
      if(navegador=='Microsoft Internet Explorer') 
          return new ActiveXObject("Microsoft.XMLHTTP");
      else{  
	      var versiones= ["Msxml2.XMLHTTP.7.0","Msxml2.XMLHTTP.6.0","Msxml2.XMLHTTP.5.0",
	                      "Msxml2.XMLHTTP.4.0","MSXML2.XMLHTTP.3.0","MSXML2.XMLHTTP",
	                      "Microsoft.XMLHTTP"];
	      for(var i=0;i<versiones.length;i++){
	          try{
	              XMLHttp= new ActiveXObject(versiones[i]);
	              if(XMLHttp){
	              	alert(versiones[i]);
	                  return XMLHttp;
	                  break;
	              }
	          }catch (e) {};
	      }
	  }                    
  }
} 
//----------------------------------------------------------------------------------------------
function XMLCtrError(xmlobj){ //Control de Errores de SQL
	
	if(xmlobj != ''){		
		var errmsg 	= '';
		var sqlerr 	= '';		
	    var errdb	= xmlobj.getElementsByTagName('errdb');		
	    var enter	= String.fromCharCode(13) + String.fromCharCode(10);
		
	   if(errdb.length>0){ //Si existe Error de Base de Datos      
		   for(i=0; i<errdb[0].childNodes.length-1; i++){
		   		switch(errdb[0].childNodes.item(i).nodeName){
		   		    case 'errmsg': 
		   		            if(errdb[0].childNodes.item(i).childNodes.item(0))
		   		                errmsg= errdb[0].childNodes.item(i).childNodes.item(0).data; 
		   		            break; 
		   		    case 'sqlerr': 	   		             
		   		             if(errdb[0].childNodes.item(i).childNodes.item(0))
		   		                 sqlerr= enter+enter+'SQL: \n' + errdb[0].childNodes.item(i).childNodes.item(0).data;
		   		             break; 
		   		}	   		
		   }	
		   ViewMsg(errmsg);
		   $('#dvblqpantalla').hide(2000);
		   //alert(errmsg + sqlerr);	   
		   return true; //hay error
	   }else{		   
	      return false;
	   }
	}else {		
		return false;
	}
}
//----------------------------------------------------------------------------------------------
function XMLCtrConfirm(xmlobj){ //Control si es Confirmacion de Pagina
	var confirm= xmlobj.getElementsByTagName('confirm_page');
	if(confirm.length>0){		
		var w = parent.window;
		if(w.innerWidth == undefined){ //IE
			var whg	= (w.document.body.clientHeight/2) - 80;
			var wwd = (w.document.body.clientWidth/2) - 240;
		}else{
			var whg	= (w.innerHeight/2) - 80;
			var wwd = (w.innerWidth/2) - 240;
		}	
		
		var objifr = document.createElement('iframe');		
		var objdiv = document.createElement('iframe');
		
		objdiv.setAttribute("id","DivConfirm");				
		objdiv.style.position	= "absolute";
		objdiv.style.opacity 	= 0.2;
		objdiv.style.width 	 	= "100%";
		objdiv.style.height	 	= "100%";
		objdiv.style.background	= "#E6E6E6";
		objdiv.style.filter		= "alpha(opacity=80)";
				
		objifr.setAttribute("id", "IfrConfirm");
		objifr.setAttribute("frameborder", "1");
		objifr.setAttribute("scrolling", "no");
		objifr.setAttribute("height", "110");
		objifr.setAttribute("width", "300");
		objifr.style.position	= "absolute";
		objifr.style.left 		= wwd;
		objifr.style.top  		= whg;
		objifr.setAttribute("src", Path_Validate+'confirm.php');
		
		document.body.insertBefore(objifr,document.body.firstChild);	
		document.body.insertBefore(objdiv,document.body.firstChild);
		
		//Deshabilito todos los Select, debido a que IE no permite poner el Div por encima
		/*var doc 		= parent.document;
		var cantforms 	= doc.forms.length;
		for(i=0; i<cantforms; i++){
			var frm	= doc.forms[i];
			alert(frm.elements[0].name)
			//var elem = frm.elements[i];
			//frm.elements[i].name
			
			//alert(frm.elements[0].length)
			
		}*/
		
		//window.open(Path_Validate+'confirm.php','confirm','width=200,height=100,top=150,left=150');	
		return true;
	}else
		return false;
		
}
//----------------------------------------------------------------------------------------------------
function StrToXml(Str,BsqText){
    if (typeof DOMParser == "undefined") { //Internet Explorer
         var strxml = new ActiveXObject("MSXML.DomDocument");
         strxml.loadXML(Str);
	 }else  //Otros		
         var strxml = (new DOMParser()).parseFromString(Str, "text/xml");
     
	return strxml;
}
//----------------------------------------------------------------------------------------------
function RunPage(Page,Datos){
  XMLHttp= CrearInstancia();    
  if (XMLHttp){
    url= Page;
    XMLHttp.onreadystatechange= GoRunPage;
    XMLHttp.open('POST', url, true);  
	XMLHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');
    XMLHttp.send(Datos);
  }	
}
//----------------------------------------------------------------------------------------------
function GoRunPage(){
	if (XMLHttp.readyState == 4){      
		var resp= XMLHttp.responseText;
		msg=''; errcod=0;        
		errmsg=''; sqlerr=''; screxe='';
	
		var xmlobj = StrToXml(resp);        
		//----------------------------------------------------------------------------------------         
		if(!XMLCtrError(xmlobj)){ //Evaluo si es Error de BD                				
			for(i=0; i < xmlobj.getElementsByTagName('respuesta').length; i++){
					var obj = xmlobj.getElementsByTagName('respuesta')[i];
					for(j=0; j < obj.childNodes.length; j++){
						var node = obj.childNodes.item(j);
						if(node.nodeName == 'msg')
							var msg = node.childNodes.item(i).data; //Mensaje
						else if(node.nodeName == 'errcod')
							var errcod = node.childNodes.item(i).data; //Err Codigo
						else if(node.nodeName == 'screxe')	 
							var screxe = node.childNodes.item(i).data; //Script de Ejecucion Correcta
					} 
			}		
			if(msg=='') alert('Error: '+resp);	 //si ocurre cualquier tipo de error.
			else alert(msg);	
			if(errcod==0 && screxe!='') eval(screxe);			        	
        }//Fin Error sql
	}
}