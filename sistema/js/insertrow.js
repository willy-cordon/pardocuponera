function InsertRow(Tabla,Cells,Valores,Clase,Font,WitdhCell,AlignCell,ColspanCell){ 
    var x=document.getElementById(Tabla).insertRow(document.getElementById(Tabla).rows.length);
    var vvalor= Valores.split('||');      
    var vanchos= WitdhCell.split('||');
    var valign= AlignCell.split('||');
    var vcolspan= ColspanCell.split('||');
      
    for(i=0; i<Cells; i++){
        var y=x.insertCell(i);
        var dato= vvalor[i];   
                 
        y.className=Clase;
        
        if(vanchos[i]){
          if(vanchos[i]!='0%')
	          y.width= vanchos[i];
	      else
	      	  y.width= '1%';
        }
      
        if(Font!='') 
            dato= '<font '+Font+'>'+dato+'</font>';
            
        if(valign[i]){            
            if (valign[i]!='' && valign[i]!=' ')            
                y.align= valign[i];
        }
        if(vcolspan[i]){
            if (vcolspan[i]!='' && vcolspan[i]!=' ')                  
                y.colSpan= vcolspan[i];
            
        }    
      
        y.innerHTML= dato; 
    }
}
