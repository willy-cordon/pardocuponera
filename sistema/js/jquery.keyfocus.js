$.fn.keyFocus = function(){	
	this.keypress(function(e){
		if (!e) e = window.event;
		if (e.keyCode) 		code = e.keyCode;
		else if (e.which) 	code = e.which; 

		if(code == 13){
			$(this).focusNextInputField();	
		}
	});
	
};		

$.fn.focusNextInputField = function() {
    return this.each(function() {
        var fields = $(this).parents('form:eq(0),body').find('input,textarea').not(':hidden').not(':disabled');
        var index = fields.index( this );
        if ( index > -1 && ( index + 1 ) < fields.length ) {
            fields.eq( index + 1 ).focus();
        }
        return false;
    });
};