function setFocus(){
	$(function() {
		var focusables = $(":focusable");   
		focusables.keyup(function(e) {
			$(this).attr("autocomplete", "off");
			var thiselm = $(this);
			var current = focusables.index(this);
			var next	= null;
			var crtlKey = e.ctrlKey;   //devuelve si está presionado el control
			
			switch(e.keyCode){
				//----------------ENTER O FLECHA DERECHA --------------------
				case 13:
				case 39:
					var i = 1;
					if(thiselm.is("select")) {
						if(e.keyCode==13){ 
							return true;
						}
					}
					if(thiselm.is("li")) {
						if(e.keyCode==39){ 
							return false;
						}
					}
					
					if(!crtlKey){ //si no está presionado el boton control
						
						if(thiselm.is("a")){//si estoy parado en un enlace (a)						
							do{
								//sigo hasta encontrar un input
								next = focusables.eq(current+i).length ? focusables.eq(current+i) : focusables.eq(0);
								i   += 1;
							}while($(next).is("li") || $(next).is("div") || $(next).is("a") );
						}else{						
							do{
								next = focusables.eq(current+i).length ? focusables.eq(current+i) : focusables.eq(0);
								i   += 1;
							}while($(next).is("li") || $(next).is("div"));
						}
					}else{
						//presionando el control 
						do{ //paso al siguiente input o al siguiente enlace (a)
							next = focusables.eq(current+i).length ? focusables.eq(current+i) : focusables.eq(0);
							i   += 1;
						}while($(next).is("li") || $(next).is("div"));						
					}
					break;
				//----------------FLECHA IZQUIERDA --------------------
				case 37:
					var i = 1;
					if(thiselm.is("li")) {
						return false;
					}
					if(!crtlKey){ //si no está presionado el boton control
						
						if(thiselm.is("a")){//si estoy parado en un enlace (a)						
							do{
								//sigo hasta encontrar un input
								next = focusables.eq(current-i).length ? focusables.eq(current-i) : focusables.eq(0);
								i   += 1;
							}while($(next).is("li") || $(next).is("div") || $(next).is("a") );
						}else{	
							do{
								next = focusables.eq(current-i).length ? focusables.eq(current-i) : focusables.eq(0);
								i   += 1;
							}while($(next).is("li") || $(next).is("div"));
							
							if($(next).is("a")){
								next = $("ul li.ui-state-active").find("a");
							}
						}
					}else{
						//presionando el control 
						do{ //paso al siguiente input o al siguiente enlace (a)
							next = focusables.eq(current-i).length ? focusables.eq(current-i) : focusables.eq(0);
							i   += 1;
						}while($(next).is("li") || $(next).is("div"));						
					}
					
					break;
				//---------------- FLECHA ARRIBA Y ABAJO --------------------	
				case 38:
				case 40:
					//obtengo la fila previa/siguiente a la fila del foco inicial
					if(thiselm.is("select")) return true;
					var prerow = null;
					if(e.keyCode == 38){
						//si presiono flecha arriba en la primer fila, voy a la ultima
						if(thiselm.closest("tr").is("table tbody :first-child")){
							prerow = thiselm.parents("table tbody").find("tr:last");
						}else{
							//sino, voy a la fila superior inmediata
							prerow = thiselm.parents('tr').prev();
						}
					}else{
					
						//si presiono flecha abjo en la ultima fila, voy a la primera
						if(thiselm.closest("tr").is("table tbody :last-child")){
							prerow = thiselm.parents("table tbody").find("tr:first");
						}else{
							//sino, voy a la fila inferior inmediata
							prerow = thiselm.parents('tr').next();
						}
					}
					
					var idelem	= thiselm.attr("id");				//obtengo el nombre del elemento de foco inicial.
												
					if(prerow.find('#'+idelem).length > 0){							
						var input 	= prerow.find('#'+idelem);	//busco el input en la fila previa
						input 		= input.get(0);						//transformo el elemento Jquery en DOM
						
						//asigno el foco
						current = focusables.index(input);
						next = focusables.eq(current).length ? focusables.eq(current) : focusables.eq(this);	
					}
					break;
			}
			
			//SI TENGO EL PROXIMO ELEMENTO PARA FOCO
			if(next){
				if(next.is("a")) next.click();
				
				next.focus();
				next.select();
			}
			return false;
		});
		focusables.keydown(function(e) {
			if($(this).is("select")){
			
				if( e.keyCode == 39 || e.keyCode==37){
					return false;
				}					
			}
		});
		return false;
	});
}
function setFunctionKey(){
	$(function() {
		$(":focusable").keyup(function(e) {
			switch(e.keyCode){
				case 27://boton ESC 
					if($("#Cancelar").length){
						$("#Cancelar").click();
					}else{
						console.log("no existe btn cancelar. Controlar id");
					}
					break;
				case 119: //F8
					if($("#Guardar").length){
						$("#Guardar").click();
					}else{
						console.log("no existe btn guardar. Controlar id");
					}
					break;
			}
		});
	});
}