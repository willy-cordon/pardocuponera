//--------------------------------------------------------------------------------------
//Tecla ENTER - JQuery - Salto de Campo
$(document).ready(function() {
	$("input:text, select").keypress( function (e){			
		if(e.which == 13) {
			var n = $("input:text:not(:disabled), select").length;					
			var nextIndex = $('input:text:not(:disabled), select').index(this) + 1;
	     	if(nextIndex < n){
	     		var obj = $('input:text:not(:disabled), select')[nextIndex];
	     		obj.focus();
	     	}else{
	     		$('input:text:not(:disabled):first').focus();
	     	}
		}
	});
	/* Causa error con lookup desborde de llamadas a la funcion en js.
	$('input:text').focus(function() {
        $(this).select();
    });
	$('input:text').dblclick(function() {
        $(this).select();
    });
	*/
});
//--------------------------------------------------------------------------------------