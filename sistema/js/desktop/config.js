
MyDesktop = new Ext.app.App({
	init :function(){
		Ext.QuickTips.init();
	},
 
	getModules : function(){
	
	var modules = new Array();
        for (var i=0 ; i < applicationItems.menuItems.length; i++) {
        		
            modules.push(eval ("new " + applicationItems.menuItems[i].itemName +"()"));    
            modules[i].launcher.text     	= applicationItems.menuItems[i].titu;
            modules[i].launcher.windowId 	= applicationItems.menuItems[i].ident;
            modules[i].launcher.dire     	= applicationItems.menuItems[i].dir;
			modules[i].launcher.path     	= applicationItems.menuItems[i].path;
            modules[i].launcher.width    	= applicationItems.menuItems[i].witdth;
            modules[i].launcher.height   	= applicationItems.menuItems[i].height;
            modules[i].launcher.titpant  	= applicationItems.menuItems[i].titpant;
            modules[i].launcher.winmax  	= applicationItems.menuItems[i].winmax;
            modules[i].launcher.winmin  	= applicationItems.menuItems[i].winmin;
            modules[i].launcher.winmaxim  	= applicationItems.menuItems[i].winmaxim;
            
            //  Check for Sub menus within the Menu Array structure 
             if( typeof(applicationItems.menuItems[i].subMenu) != 'undefined' ) {
                                 
                 /* var subMenus = getSubModules(MenuItems.items[i].subMenu); */ 
                 var subMenus = new Array();
                 for (var j=0 ; j < applicationItems.menuItems[i].subMenu.length; j++){
                       var item = eval(("new " + applicationItems.menuItems[i].subMenu[j].itemName +"()"));                           
                       if( typeof(applicationItems.menuItems[i].subMenu[j].subMenu) != 'undefined' ) {
                              var tempArray = applicationItems.menuItems[i].subMenu[j].subMenu;
                              var menuLevel2 = new Array();
                              for (var k=0 ; k < tempArray.length; k++){
                                      var item2 = eval (("new " + tempArray[k].itemName +"()"));    
                                      item2.launcher.text     	= tempArray[k].titu;
                                      item2.launcher.windowId 	= tempArray[k].ident;
                                      item2.launcher.dire     	= tempArray[k].dir;
									  item2.launcher.path     	= tempArray[k].path;
                                      item2.launcher.width    	= tempArray[k].witdth;
                                      item2.launcher.height   	= tempArray[k].height;
                                      item2.launcher.titpant  	= tempArray[k].titpant;
                                      item2.launcher.winmax  	= tempArray[k].winmax;
                                      item2.launcher.winmin  	= tempArray[k].winmin;
                                      item2.launcher.winmaxim  	= tempArray[k].winmaxim;                                      
                                                                     
                                      menuLevel2.push(item2.launcher);                   
                              }
                              item.launcher.handler = function() {
                                         return false;
                                  };
                                  item.launcher.menu = {            
                                         items : menuLevel2    
                                 };                      
                       }
                       item.launcher.text     	= applicationItems.menuItems[i].subMenu[j].titu;
                       item.launcher.windowId 	= applicationItems.menuItems[i].subMenu[j].ident;
                       item.launcher.dire     	= applicationItems.menuItems[i].subMenu[j].dir;
					   item.launcher.path     	= applicationItems.menuItems[i].subMenu[j].path;
                       item.launcher.width    	= applicationItems.menuItems[i].subMenu[j].witdth;
                       item.launcher.height   	= applicationItems.menuItems[i].subMenu[j].height;
                       item.launcher.titpant  	= applicationItems.menuItems[i].subMenu[j].titpant; 
                       item.launcher.winmax  	= applicationItems.menuItems[i].subMenu[j].winmax; 
                       item.launcher.winmin  	= applicationItems.menuItems[i].subMenu[j].winmin; 
                       item.launcher.winmaxim  	= applicationItems.menuItems[i].subMenu[j].winmaxim; 
                       subMenus.push(item.launcher);
                 }                 

                 modules[i].launcher.handler = function() {
                     return false;
                 };
                
                 modules[i].launcher.menu = {            
                         items : subMenus    
                 };
            }
          
        }
        
        return modules;
    },
    // config for the start menu
 // config for the start menu
    getStartConfig : function(){
        return { 
        	title: usuario,
            toolItems: [{
                text:'Cerrar',                
                scope:this,                                                   
                handler: function() {
            		Ext.MessageBox.show({
					   title: 		'SISTEMA',
					   msg: 		'Desea Salir del Sistema?',
					   buttons:  	{yes: "Si", no: "No"},
					   fn:			SistemClose,					           
					   icon: 		Ext.MessageBox.QUESTION
					});   				
    			}
            }]
        };
    }    
});

var windowIndex = 0;
var wingroup 	= new Ext.WindowGroup();
wingroup.zseed 	= 1;

MyDesktop.BogusModule = Ext.extend(Ext.app.Module, {
    init : function(){
        this.launcher = {
            text: 'Primero',
            iconCls:'bogus',
            handler : this.createWindow,
            scope: this,
            width: 100,
            height: 900,
            titpant: '',
            dire:'',
            windowId:windowIndex
        }
    },

    createWindow : function(src){        
        var desktop = MyDesktop.modules[0].app.getDesktop();        
        //var win = desktop.getWindow(src.windowId);
        var win = wingroup.get(src.windowId);
        
        winmax 		= false; //Boton Maximizar
        winmin 		= false; //Boton Minimizar
        winmaxim 	= false; //Ventana Maximizada
        
        if(src.winmax 	== 1) winmax 	= true;
        if(src.winmin 	== 1) winmin 	= true;
        if(src.winmaxim == 1) winmaxim 	= true;
        
        if(!win){
			var jsWinOpen = { 	
								"winid": 	src.windowId,
								"title": 	src.titpant,
								"width": 	src.width,
								"height": 	src.height,
								"html": 	src.path,
								"min": 		winmin,
								"max":		winmax,
								"maxim": 	winmaxim
							}
			localStorage["Benvido.WinOpen"] = JSON.stringify(jsWinOpen);	
			
            win = desktop.createWindow({
                id: 			src.windowId,
                manager: 		wingroup,
                title: 			src.titpant,
                width: 			parseInt(src.width),
                height: 		parseInt(src.height),
                html: 			src.dire,
                iconCls: 		'bogus',
                minimizable:	winmin,
                maximizable:	winmax,
                maximized:		winmaxim,
                shim:			false,
                animCollapse:	false,
                constrainHeader:true
            });
        }
        win.show();
    }
});

function SistemClose(resp){	
	if(resp == 'yes'){
		$('#dvwinclose').load('../val/unsession.php',function(){
			document.getElementById('closewin').value="";

			var imgfondo = $('#fondo').val();
			var colorfondo = $('#colorfondo').val();
			var coloremp = $('#colorempresa').val();
			
			var param = '';
			
			if(coloremp	== 'N'){
				if(imgfondo != ''){
					param += 'FD='+imgfondo;
				}
				if(colorfondo != ''){
					colorfondo = colorfondo.replace('#','');
					if(param == ''){
						param += 'CL='+colorfondo;
					}else{
						param += '&CL='+colorfondo;
					}
				}
			}
			
			window.location = '../login.php?'+param;
		
		});
	}
}
