function MaxLength(Obj,Largo,e) {		      		
	var strCheck = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ .()_-,@';
	var whichCode = (window.Event) ? e.which : e.keyCode;		
	if (whichCode == 13) return true;  // Enter
    if (whichCode == 8) return true;  // Borrar
    if (whichCode == 0) return true;  // Delete

	key = String.fromCharCode(whichCode);  // Get key value from key code
    key = key.toUpperCase();
    
	if (strCheck.indexOf(key) == -1) return false;  // Not a valid key
	len = Obj.value.length;
		
	if (len > Largo) return false;	
        else return true;
}
