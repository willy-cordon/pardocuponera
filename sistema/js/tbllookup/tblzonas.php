<?php include('../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  	//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	

	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";
	//--------------------------------------------------------------------------------------------------------------		
		
	//Datos entregados por LookUp
	$buscar 	= trim($_POST['buscar']); 	//Filtro del Campo de Buscar (lookup abierto)
	$pagnro 	= trim($_POST['pagnro']);	//Nro de Pagina (lookup abierto)
	$pagin 		= trim($_POST['pagin']);	//Cantidad de Registros por Pagina (lookup abierto)	
	$pkvalue 	= trim($_POST['pkvalue']);	//Busqueda puntual (Valor).
	$buscar 	= strtoupper($buscar);
	//----------------------------------------------------------
	
	//Filtros
	$where	= '';
	if($buscar != '' && $pkvalue == ''){
		$where = " AND (UPPER(ZONCODIGO) CONTAINING '$buscar' OR UPPER(ZONDESCRI) CONTAINING '$buscar') ";		
	}	
	if($pkvalue != ''){
		$where = " AND ZONCODIGO = '$pkvalue' ";
	}
	//----------------------------------------------------------
	
	$JSTable	= '{"row": ['; //Tabla JSON
	
	$conn= sql_conectar();//Apertura de Conexion
	
	$query = "SELECT ZONCODINT, ZONCODIGO, ZONDESCRI
				FROM ZON_MAEST 
				WHERE EMPCODIGO = $empcodigo AND ESTCODIGO=1 $where
				ORDER BY ZONCODIGO";			
	
	$query = PaginarSql($pagnro, $query, $pagin);
	$Table = sql_query($query,$conn);
	for($i=0; $i < $Table->Rows_Count; $i++){
		$row= $Table->Rows[$i];
		
		$zoncodint	= trim($row['ZONCODINT']); 	//Codigo Interno
		$zoncodigo	= trim($row['ZONCODIGO']); 	//Codigo
		$zondescri	= trim($row['ZONDESCRI']); 	//Descripcion
		
		
		$JSTable .= '{	"index":"'.$i.'",
						"content":{
							"ZONCODINT":"'.$zoncodint.'",
							"ZONCODIGO":"'.$zoncodigo.'", 
							"ZONDESCRI":"'.$zondescri.'"
							}
						},';
		
	}
	if($Table->Rows_Count >0){ //Si hay registros
		$JSTable	= substr($JSTable, 0, strlen($JSTable)-1);
	}
	$JSTable 	.= '], "rows":"'.$Table->Rows_Count.'"}';
	echo $JSTable;
	
	sql_close($conn);
	
	/* ESTRUCTURA DE TABLA
	 {"row": 
		[ {	"index":"0",
			"content":{
				"campo1":"valor1", 
				"campo2":"valor2"
				}
			},
			{	"index":"1",
				"content":{
					"campo1":"valor4", 
					"campo2":"valor5"
					}
				} 
		], "rows":"0"};
	*/
	//--------------------------------------------------------------------------	
?>	