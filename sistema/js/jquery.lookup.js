(function($){
	$.fn.lookup = function(options){	
		var LKpathSistem 	= null;
		var LKPagin			= null;
		var LKpagnro		= null;		
		var LKimgfind		= null;		
		var LKobjJQ			= null;
		var LKobj			= null;
		var LKtable			= null;				
		var LKdiag			= null;
		var LKrowsel 		= null;
		var LKrows			= null;		
		var defaults 		= null;
		var datafilters		= null;
		var lkdialogisopen  = false;
			
				
		/* ----- Options ------
			tblfile: Archivo Php generador de Tabla
			fldequals: Campos de Igualacion 					 
		*/
			
		
		/* ----- Configuracion de Campos - Options -----
		 [{field : "CAMPO1", 
				config : {
					width: 20,
					hidden: true,
					title: 'CodInterno',
					align: left			
				}
			},{field : "CAMPO2", 
				config : {
 					width: 20,
 					hidden: false,
					title: 'Codigo',
					align: left
	 			}
			},{field : "CAMPO3", 
				config : {
 					width: 200,
 					hidden: false,
					title: 'Nombre',
					align: left	
 				}
			}]
		 */
		
		/* ----- Campos para Igualacion - Options -----
			[
			 {fldorig : "ARTCODINT", 
				flddest : "artcodint"},
			 {fldorig : "ARTCODIGO", 
				flddest : "artcodigo"},
			 {fldorig : "ARTDESCRI", 
				flddest : "artdescri"}
			]
		*/
		var LKpathSistem 	= '/sistema/js/tbllookup/';		//Ruta de Sistema
		var LKimgfind		= '/sistema/imges/play.png';


		LKPagin		= 50;	//Paginacion (Cantidad de Registros por Pagina)
		LKpagnro	= 0;		
		LKtable		= {"ErrorMsg":"","row":[{"index":"0","content":{"sin datos":""}}], "rows":"0"};	
		LKrowsel 	= -1;	//Fila seleccionada
		LKrows		= Array();		
		defaults 	= {tblfile: null, fldequals: null, fields: null, params: null, debug: false, funcend: null, width: 450, height: 400, viewlookup: true, pkfield:null, title: 'Buscador...', cache: true};
		datafilters	= {buscar: '', pagnro: LKpagnro, pagin: LKPagin, pkvalue: ''};
				
		$.extend(defaults, options);
		
		if(!$(this).data('lookup')){
			$(this).data('lookup','ok');
		}
		
		//Inicializacion
		var init = function(){	
			LKobjJQ 	= this.$;
			LKdiag		= null;			
		};
		
		this.each(function(){
			function clean(){
				LKrowsel 	= -1;
				LKpagnro	= 0;
				LKrows		= Array();
				datafilters	= {buscar: '', pagnro: LKpagnro, pagin: LKPagin, pkvalue: ''};
				LKtable		= {"ErrorMsg":"","row":[{"index":"0","content":{"sin datos":""}}], "rows":"0"};
				if($('#datahtml').length > 0){					
					$('#datahtml').remove();					
				}
			};
			
			function findTable(filters){				
				$.extend(datafilters, filters);
				LKobjJQ.ajax({
					type: "POST",		
					contentType: "application/x-www-form-urlencoded; charset=UTF-8;",
					//dataType: 'json',
					url: LKpathSistem + defaults.tblfile, 	
					data: datafilters
				}).done(function( rsp ) {												
					//rsp = Base64.decode(rsp);
					
					if(rsp.indexOf('</errdb>')!=-1){
						var aux = rsp.split('</errdb>');
						rsp = aux[1];
					}
					if(rsp.indexOf('</b><br />')!=-1){
						var aux = rsp.split('</b><br />');
						rsp = aux[1];
					}
					
					LKrowsel	= -1;
					if(defaults.debug == true){
						alert(rsp);
					}		
											
					LKtable		= LKobjJQ.parseJSON(rsp);	//Tabla JSON					
					if(lkdialogisopen == true){
						var winidactive = window.top.wingroup.getActive().id;
						if(defaults.cache == true){
							localStorage["Benvido.LookUp.Data."+LKobj.attr('id')+winidactive]		= rsp;					
							localStorage["Benvido.LookUp.PagNro."+LKobj.attr('id')+winidactive] 	= parseInt(LKpagnro);
							localStorage["Benvido.LookUp.Buscar."+LKobj.attr('id')+winidactive] 	= datafilters.buscar;
							localStorage["Benvido.LookUp.RowSel."+LKobj.attr('id')+winidactive] 	= parseInt(LKrowsel);
						}
					}
					
					if(datafilters.pkvalue == ''){ //Solo si no se aplico filtro puntual
						//Elimino la tabla
						$('#datahtml').remove();	
						genLookUp(); //Genero la Vista
					
						//Genero el Array con las Filas					
						$('tr[id=trlookup]').each(function(i){
							LKrows[i] = $(this);					
						});
					}else{
						if(LKobj){						
							LKobjgroup 	= LKobj.attr("lookupgroup"); //Tag Agrupador
						}
					
						if(typeof LKtable.ErrorMsg != 'undefined' && LKtable.ErrorMsg != ''){
							ViewMsg(LKtable.ErrorMsg);
							LKobj.val('');
							LKobj.focus();
							objSetFocus = LKobj;
							countSetFocus 	= 0;
							setTimeout("highLightFocus();",20);
						}
						//Recorro las Filas					
						if(LKtable.rows == -1){ //Si no hay registros
							/*LKobjJQ.each($('input[lookupgroup|="'+LKobjgroup+'"]'), function(index, objhtml){
								objhtml.value = '';							
							});*/
							LKobjJQ.each($('input[lookupgroup|="'+LKobjgroup+'"]'), function(index, objhtml){
								LKobjJQ.each(defaults.fldequals, function(index, obj) {								
									if(obj.flddest == objhtml.id){
										objhtml.value = '';
										objhtml.title = '';
									}
								});				
							});
							if(defaults.funcend != null){
								eval(defaults.funcend);
							}
						}else{ //A0902911
							var lkgroup = LKobjgroup;
							LKobjJQ.each(LKtable.row, function(index, row) {
								//Recorro los campos
								//Busco el Valor devuelto por el campo y lo asigno
								LKobjJQ.each(row.content, function(field, value) {
									//Busco el Campo de Igualacion
									fieldequal = '';
									LKobjJQ.each(defaults.fldequals, function(index, objequal) {										
										if(field == objequal.fldorig){
											fieldequal = objequal.flddest;
										}
									});	
									
									if(fieldequal != ''){	
										if(isJson(value)){
											value = JSON.stringify(value);
										}else{
											value = value.replace('&quot;','"'); //Commillas dobles    
										}
										$('input[id="'+fieldequal+'"][lookupgroup|="'+lkgroup+'"]').val(value);
									}																		
								});
																
								if(lkgroup.indexOf('_') > -1){ //Tiene detalle
									var vaux 	= lkgroup.split('_');
									var numaux	= parseInt(vaux[1]);
									lkgroup = vaux[0] + '_' + (numaux+1);
								}								
							});		
																			
							if(defaults.funcend != null){
								eval(defaults.funcend);
							}
						}
					}
					
				});
			};
			
			function openDialog(){
				lkdialogisopen = true;
				LKrowsel	= -1;
				//Ventana
				LKdiag = $('<div></div>').dialog({ 
										         title: defaults.title, 
										         modal: true,									         
										         height: defaults.height,
										         width: defaults.width,
										         beforeClose: function (event, ui) {
																	genEquals(null);
												                }									            
			         							});
				//Abro la ventana de Dialogo					
				LKdiag.dialog('open');
			};
			
			//Generador de Ventana de Seleccion (LOOKUP)
			function genLookUp(){
				var rows 		= null;
				var databody 	= null;
				var datainpfind	= null;
				var dataimgfind	= null;
				var datatable 	= null;
				var datathead	= null;
				var datatr		= null;
				var datath		= null;
				var name 		= null;
				var title 		= null;
				var hidden 		= null;
				var width 		= null;
				var align 		= null;
				var datainput 	= null;
				var datavalue	= null;
				var datatd 		= null;
				
				//Elimino el body de la ventana de dialogo
				LKdiag.find('#datahtml').remove();
				
				//Genero el Contenido del LookUp ----------------------------------------------
				rows 		= LKtable.rows; //Cantidad de Filas
				datahtml 	= $('<html></html>'); 
				datahtml.attr('id','datahtml');
				datahead 	= $('<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /></head>'); 				
				databody 	= $('<body></body>'); 
				databody.attr('id','databody');
				databody.css('height','100%');
				databody.css('width','100%');
				
				//Casillero de Busqueda
				datainpfind	= $('<input></input>');
				datainpfind.attr('id','txtbuscar');
				datainpfind.attr('type','text');
				datainpfind.attr('style','width:90%;');
				datainpfind.addClass('LK_inp_buscar');
				datainpfind.attr('value',datafilters.buscar);			
				datainpfind.keypress(function(event){
					if(event.which == 13){ //Tecla Enter
						if(LKrowsel == -1 && datainpfind.value != ''){	
							findTable({buscar: this.value, pagnro: 0}); //Ejecuto la Busqueda
						}else{ //En caso de haber seleccionado un registros
							genEquals(LKrows[LKrowsel].parent());
							LKdiag.dialog('close'); 
							if(defaults.funcend != null){
								eval(defaults.funcend);
							}
						}
					}				
				});	
				datainpfind.keydown(function(event){
					if(event.which == 46){ //Limpieza de input (delete)
						datainpfind.val('');					
					}			
					if(event.which == 40 || event.which == 38){ //Cursor Abajo - Arriba
						setCursors(event.which);
					}
					if(event.which == 37 || event.which == 39){ //Cursor Izquierda - Derecha
						setPagin(event.which);
					}
					
					//Inicializacion de Posicion de selector
					if(event.which != 40 && event.which != 38 && event.which != 13){ //Diferente a los cursores, Arriba y Abajo
						LKrowsel	= -1;
					}
				});
				databody.append(datainpfind);
				
				//Icono de Busqueda
				dataimgfind	= $('<img></img>');
				dataimgfind.attr('src',LKimgfind);
				dataimgfind.click(function(){					
					findTable({buscar: $('#txtbuscar').val(), pagnro: 0 }); //Ejecuto la Busqueda
				});
				databody.append(dataimgfind);
							
				
				//Tabla con Datos
				datadiv		= $('<div></div>');
				datadiv.attr('id','dvlktable');
				datadiv.attr('style','height:310px; overflow: scroll; overflow-x: hidden; overflow-y: auto;');				
				datatable 	= $('<table></table>');	//Inicio de Tabla
				datatable.attr('border','0');
				datatable.css('width','100%');
				datatable.attr('id','tbllookup');
				
				//Inicio de Titulos
				datathead = $('<thead></thead>');
				datathead.addClass('LK_th_titles');
				datatr = $('<tr></tr>');
				
				//Recorro buscando titulos
				LKobjJQ.each(defaults.fields, function(index, obj) {
					name 	= obj.field;
					title 	= obj.config.title;
					hidden 	= obj.config.hidden;
					width 	= obj.config.width;
					datath	= null;
					
					if(hidden == true){
						datath = $('<th style="display:none;">'+ title +'</th>');
					}else{
						datath = $('<th>'+ title +'</th>');
					}
					datathead.append(datath);
				});			
				datatable.append(datathead);
				
				//Recorro las Filas
				if(typeof LKtable.ErrorMsg != 'undefined' && LKtable.ErrorMsg != ''){
					ViewMsg('Error:' + LKtable.ErrorMsg);
				}else{				
					LKobjJQ.each(LKtable.row, function(index, obj) {
						datatbody 	= $('<tbody></tbody>'); //Fila Cuerpo
						datatbody.addClass('LK_tb_rows');
						datatr		= $('<tr></tr>'); //Fila
						datatr.addClass('LK_tr_rows');
						datatr.attr('style','cursor:hand; cursor:pointer;');
						datatr.attr('id','trlookup');
						
						//Recorro los campos
						LKobjJQ.each(obj.content, function(field, value) {
							
							//Busco datos del campo
							LKobjJQ.each(defaults.fields, function(index, obj) {
								name 	= obj.field;
								title 	= obj.config.title;
								hidden 	= obj.config.hidden;
								align	= 'left';
								if(obj.config.align){
									align 	= obj.config.align;
								}
								var width 	= obj.config.width;						
								
								if(name == field){ //Si cocide el campo
									if(isJson(value)){
										value = JSON.stringify(value);										
										var re = new RegExp('"', 'g');
										value = value.replace(re, '&quot;');
									}else{
										value = value.replace('&quot;','"'); //Commillas dobles
									}

									datainput 	= $('<input id="' + name + '" value="' + value + '" type="hidden"></input>'); //Input Hidden
									datavalue	= $('<label>' + value + '</label>'); //Valor Visible
									datatd 		= null; //Celda
																
									if(hidden == true){
										datatd = $('<td style="display:none;"></td>');
									}else{
										datatd = $('<td></td>').click(function (){ 
																		genEquals($(this).parent());
																		LKdiag.dialog('close');
																		if(defaults.funcend != null){
																			eval(defaults.funcend);
																		}
																	  });
									}
									
									datatd.append(datainput); 	//Input Hidden
									datatd.append(datavalue);	//Valor Visible
									datatd.css('text-align',align);
									datatd.css('width', width +'px');									
									datatr.append(datatd);
								}
							});					
						});
						
						datatbody.append(datatr); //Agrego Fila
						datatable.append(datatbody); //Agrego Fila Cuerpo a Tabla
					});	
				}
				
				databody.find("#txtbuscar").select();

				datadiv.append(datatable);
				databody.append(datadiv);				
				datahtml.append(datahead);
				datahtml.append(databody);
				//-----------------------------------------------------------------------------			
				LKdiag.append(datahtml);				
				datainpfind.focus();
			};
			
			//Ejecucion de Igualdades, frente a seleccion
			function genEquals(objtr){				
				if(objtr != null){
					var objfocus	= null;			
					objfocus	= 0; //si se dio el foco (flag)
					LKobjJQ.each($('input[lookupgroup|="'+LKobjgroup+'"]'), function(index, objhtml){
						LKobjJQ.each(defaults.fldequals, function(index, obj) {
							//Comparo que sean el mismo nombre
							if(obj.flddest == objhtml.id){
								if(objtr != null){
									valor = objtr.find('#'+obj.fldorig).val();

									objhtml.value = valor;
									objhtml.title = objtr.find('#'+obj.fldorig).val();
								}
							}
						});				
					});
					if(LKobj){
						LKobj.focus();
					}
				}
			};
			
			//Cursores - Arriba / Abajo
			function setCursors(nrokey){
				var winidactive = window.top.wingroup.getActive().id;
				$('tr[id=trlookup]').removeClass().addClass('LK_tr_rows');
				
				if(nrokey == 38){	//Cursor Arriba					
					LKrowsel--;
					if(LKrowsel < 0){ 
						LKrowsel=0;
					}
					if(LKrows[LKrowsel]){
						LKrows[LKrowsel].addClass('LK_tr_selected');						
						localStorage["Benvido.LookUp.RowSel."+LKobj.attr('id')+winidactive] = parseInt(LKrowsel);
					}
					
					var pos = parseInt($('#dvlktable').scrollTop()) - 15; 					
					$("#dvlktable").animate({
						scrollTop: pos
					}, 1);
				}	
				if(nrokey == 40){ 	//Cursor Abajo					
					LKrowsel++;
					if(LKrowsel >= LKrows.length-1){
						LKrowsel = LKrows.length-1;					
					}
					if(LKrows[LKrowsel]){
						LKrows[LKrowsel].addClass('LK_tr_selected');;
						localStorage["Benvido.LookUp.RowSel."+LKobj.attr('id')+winidactive] = parseInt(LKrowsel);
					}
										
					var pos = LKrowsel * 15; //$('#dvlktable').scrollTop();						
					$("#dvlktable").animate({
						scrollTop: pos
					}, 1);

				}					
			};
			
			//Paginacion - Izuiqerda / Derecha
			function setPagin(nrokey){
				LKrowsel	= -1;
				if(nrokey == 39){	//Cursor Derecha	
					LKpagnro += LKPagin;
					findTable({buscar: $('#txtbuscar').val(), pagnro: LKpagnro }); //Ejecuto la Busqueda
				}
				if(nrokey == 37){	//Cursor Izquierda
					LKpagnro -= LKPagin;
					if(LKpagnro < 0){
						LKpagnro = 0;
					}				
					findTable({buscar: $('#txtbuscar').val(), pagnro: LKpagnro }); //Ejecuto la Busqueda
				}
			};
			
			
			//Click de LookUp
			$(this).click(function(){				
				if($(this).attr('disabled')!='disabled'){
					if(this.tagName == 'IMG' || this.tagName == 'A'){
						var winidactive = window.top.wingroup.getActive().id;
						
						var filters = null;
					
						LKobj		= $(this);
						LKobjgroup 	= LKobj.attr("lookupgroup"); //Tag Agrupador			
						clean();
						
						openDialog();
						
						filters	= datafilters; 
						//Recorro todos los campos que poseen el grupo, para enviar como data
						LKobjJQ.each($('input[lookupgroup|="'+LKobjgroup+'"]'), function(index, objhtml){					
							filters[objhtml.name] = objhtml.value;						
						});
						
						//Si existen mas parametros a tener en cuenta
						if(defaults.params != null){
							for(var i=0; i< defaults.params.length; i++){
								filters[defaults.params[i]] = $('#'+defaults.params[i]).val();
							}				
						}
						$.extend(datafilters, filters);
						
						if(defaults.cache == true){
							var dataPrev 	= localStorage["Benvido.LookUp.Data."+LKobj.attr('id')+winidactive];
							var dataPagNro 	= localStorage["Benvido.LookUp.PagNro."+LKobj.attr('id')+winidactive];
							var dataBuscar 	= localStorage["Benvido.LookUp.Buscar."+LKobj.attr('id')+winidactive];
							var dataRowSel  = localStorage["Benvido.LookUp.RowSel."+LKobj.attr('id')+winidactive];
						}
						
						if(typeof dataPrev !== 'undefined'){
							if(dataPrev != ''){
								LKpagnro	= parseInt(dataPagNro);
								datafilters.buscar = dataBuscar;
								LKrowsel	= parseInt(dataRowSel);
								
								LKtable		= LKobjJQ.parseJSON(dataPrev);	//Tabla JSON
								
								if(datafilters.pkvalue == ''){ //Solo si no se aplico filtro puntual
									//Elimino la tabla
									$('#datahtml').remove();	
									genLookUp(); //Genero la Vista
								
									//Genero el Array con las Filas					
									$('tr[id=trlookup]').each(function(i){
										LKrows[i] = $(this);					
									});
									LKrows[LKrowsel].addClass('LK_tr_selected');
									
									var pos = LKrowsel * 15;								
									$("#dvlktable").animate({
										scrollTop: pos
									}, 1);
								}
							}else{									
								genLookUp(); //Genero la Vista
							}
						}else{					
							genLookUp(); //Genero la Vista
						}
					}
				}else{
					ViewMsg('Boton Desactivado');
				}
			});
			
			$(this).keydown(function(event){
				var winidactive = window.top.wingroup.getActive().id;
				
				var filters = null;
				
				LKobj		= $(this);
				LKobjgroup 	= LKobj.attr("lookupgroup"); //Tag Agrupador			
				clean();
				
				if(event.which == 113 && defaults.viewlookup){ //Tecla - F2
					openDialog();
					
					filters	= datafilters; 
					//Recorro todos los campos que poseen el grupo, para enviar como data
					LKobjJQ.each($('input[lookupgroup|="'+LKobjgroup+'"]'), function(index, objhtml){					
						filters[objhtml.name] = objhtml.value;						
					});
					
					//Si existen mas parametros a tener en cuenta
					if(defaults.params != null){
						for(var i=0; i< defaults.params.length; i++){
							filters[defaults.params[i]] = $('#'+defaults.params[i]).val();
						}				
					}
						
					$.extend(datafilters, filters);
					
					if(defaults.cache == true){
						var dataPrev 	= localStorage["Benvido.LookUp.Data."+LKobj.attr('id')+winidactive];
						var dataPagNro 	= localStorage["Benvido.LookUp.PagNro."+LKobj.attr('id')+winidactive];
						var dataBuscar 	= localStorage["Benvido.LookUp.Buscar."+LKobj.attr('id')+winidactive];
						var dataRowSel  = localStorage["Benvido.LookUp.RowSel."+LKobj.attr('id')+winidactive];
					}
					
					if(typeof dataPrev !== 'undefined'){
						if(dataPrev != ''){
							LKpagnro	= parseInt(dataPagNro);
							datafilters.buscar = dataBuscar;
							LKrowsel	= parseInt(dataRowSel);
							LKtable		= LKobjJQ.parseJSON(dataPrev);	//Tabla JSON
							
							if(datafilters.pkvalue == ''){ //Solo si no se aplico filtro puntual
								//Elimino la tabla
								$('#databody').remove();	
								genLookUp(); //Genero la Vista
							
								//Genero el Array con las Filas					
								$('tr[id=trlookup]').each(function(i){
									LKrows[i] = $(this);					
								});
								if(LKrowsel>0){																	
									LKrows[LKrowsel].addClass('LK_tr_selected');
								}
								
								var pos = LKrowsel * 15;								
								$("#dvlktable").animate({
									scrollTop: pos
								}, 1);
							}
						}else{					
							genLookUp(); //Genero la Vista
						}
					}else{					
						genLookUp(); //Genero la Vista
					}			
				}			
			});
			
			$(this).change(function(){
				lkdialogisopen = false;
				clean();
				
				var filters = null;
				
				LKobj		= $(this);
				LKobjgroup 	= $(this).attr("lookupgroup"); //Tag Agrupador
				
				pkfield = null;
				if(defaults.pkfield == null){
					pkfield = $(this);
				}else{
					pkfield = $('input[lookupgroup|="'+LKobjgroup+'"][id="'+defaults.pkfield+'"]');
				}
				
				if($(this).val() != ''){			
					datafilters.pkvalue = pkfield.val();
					
					filters	= datafilters; 
					//Recorro todos los campos que poseen el grupo, para enviar como data
					LKobjJQ.each($('input[lookupgroup|="'+LKobjgroup+'"]'), function(index, objhtml){					
						filters[objhtml.name] = objhtml.value;						
					});
					
					//Si existen mas parametros a tener en cuenta
					if(defaults.params != null){
						for(var i=0; i< defaults.params.length; i++){
							filters[defaults.params[i]] = $('#'+defaults.params[i]).val();						
						}				
					}
					
					findTable(filters);					
				}else{
					LKobjJQ.each($('input[lookupgroup|="'+LKobjgroup+'"]'), function(index, objhtml){
						LKobjJQ.each(defaults.fldequals, function(index, obj) {								
							if(obj.flddest == objhtml.id){
								objhtml.value = '';
								objhtml.title = '';
							}
						});				
					});
					if(defaults.funcend != null){
						eval(defaults.funcend);
					}
				}
			});
		});
		
		init();
	};	
	
	
})(jQuery);