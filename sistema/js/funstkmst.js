//-------------------------------------------------------------------------------------------
function setVarsGlobals(){
	//LookUp de fbantes de Stock
	tblfldStkCmp =	[{field : "STKCMPCOD",		config 	: {width: 10,	hidden: false,	title: 'Codigo'}},
	     		 	 {field : "STKCMPDES",		config 	: {width: 300,	hidden: false,	title: 'Comprobante'}},
	     		 	 {field : "STKDEPDDE",		config 	: {width: 5,	hidden: true,	title: 'Dep.Dde.Cod.'}},
	     		 	 {field : "STKDEPDESDDE",	config 	: {width: 5,	hidden: true,	title: 'Dep.Dde.Des.'}},
	     		 	 {field : "STKDEPHTA",		config 	: {width: 5,	hidden: true,	title: 'Dep.Hta.Cod.'}},
	     		 	 {field : "STKDEPDESHTA",	config 	: {width: 5,	hidden: true,	title: 'Dep.Hta.Des.'}},
	     		 	 {field : "STKCMPUNIC"	,	config 	: {width: 5,	hidden: true,	title: 'Util.Unid.Compra'}},
	     		 	 {field : "STKCMPCST"	,	config 	: {width: 5,	hidden: true,	title: 'Perm.Costo'}},
					 {field : "STKCMPVTA"	,	config 	: {width: 5,	hidden: true,	title: 'Comp.Venta'}},
					 {field : "STKREQOBS"	,	config 	: {width: 5,	hidden: true,	title: 'Req.Obs.'}},
					 {field : "STKGENNUM"	,	config 	: {width: 5,	hidden: true,	title: 'Gen.Num.Rem.'}},
					 {field : "STKCMPPRO"	,	config 	: {width: 5,	hidden: true,	title: 'Ing.Proveed'}},
					 {field : "STKCMPCLI"	,	config 	: {width: 5,	hidden: true,	title: 'Ing.Cliente'}},
					 {field : "STKREQLOT"	,	config 	: {width: 5,	hidden: true,	title: 'Requiere lote'}},
					 {field : "STKBSQVTA"	,	config 	: {width: 5,	hidden: true,	title: 'Bsq.Det.Vta'}} ];
			
	tblequalsStkCmp =	[{fldorig : "STKCMPCOD"		,flddest : "stkcmpcod"},
					 	 {fldorig : "STKCMPDES"		,flddest : "stkcmpdes"},
					 	 {fldorig : "STKDEPDDE"		,flddest : "stkcmpdepdde"},
					 	 {fldorig : "STKDEPDESDDE"	,flddest : "stkcmpdepddedes"},
					 	 {fldorig : "STKDEPHTA"		,flddest : "stkcmpdephta"},
					 	 {fldorig : "STKDEPDESHTA"	,flddest : "stkcmpdephtades"},
					 	 {fldorig : "STKCMPUNIC"	,flddest : "stkcmpunic"},
					 	 {fldorig : "STKCMPCST"		,flddest : "stkcmpcst"},
						 {fldorig : "STKCMPVTA"		,flddest : "stkcmpvta"},
						 {fldorig : "STKREQOBS"		,flddest : "stkreqobs"},
						 {fldorig : "STKGENNUM"		,flddest : "stkgennum"},
						 {fldorig : "STKCMPPRO"		,flddest : "stkcmppro"},
						 {fldorig : "STKCMPCLI"		,flddest : "stkcmpcli"},
						 {fldorig : "STKREQLOT"		,flddest : "stkreqlot"},
						 {fldorig : "STKBSQVTA"		,flddest : "stkbsqvta"} ];
		
	//LookUp de Linea de Pedido
	tblfldStkLin =	[{field : "ARTCODINT",config 	: {width: 5,	hidden: true,	title: 'Cod.Int.'}},
	     			 {field : "ARTCODIGO",config 	: {width: 5,	hidden: false,	title: 'Codigo'}},
	     			 {field : "ARTDESCRI",config 	: {width: 350,	hidden: false,	title: 'Descripcion'}},
	     			 {field : "STKUNICOD",config 	: {width: 0,	hidden: true,	title: 'Unid.Alm.'}},
	     			 {field : "UNICODIGO",config 	: {width: 5,	hidden: true,	title: 'Unid.Cod' }},
	     			 {field : "UNIDESCRI",config 	: {width: 0,	hidden: true,	title: 'Unid.Des.'}},
	     			 {field : "STKCNVUC",config 	: {width: 0,	hidden: true,	title: 'Cst.Cnv.UC'}},
	     			 {field : "STKCSTUC",config 	: {width: 0,	hidden: true,	title: 'Cst.UC'}},
	     			 {field : "STKCNVMA",config 	: {width: 0,	hidden: true,	title: 'Cst.Cnv.MA'}},
	     			 {field : "STKCSTMA",config 	: {width: 0,	hidden: true,	title: 'Cst.MA'}},
	     			 {field : "STKDEPDDE",config 	: {width: 0,	hidden: true,	title: 'Dep.Dde.Cod.'}},
	     			 {field : "STKDEPDDEDES",config : {width: 0,	hidden: true,	title: 'Dep.Dde.Des.'}},
	     			 {field : "STKDEPHTA",config 	: {width: 5,	hidden: true,	title: 'Dep.Hta.Cod.'}},
	     			 {field : "STKDEPHTADES",config : {width: 0,	hidden: true,	title: 'Dep.Hta.Des.'}},
	     			 {field : "STKMOVCNT",config 	: {width: 0,	hidden: true,	title: 'Cant.'}},
					 {field : "STKCNVCNT",config 	: {width: 0,	hidden: true,	title: 'Cant.Cnv'}},
	     			 {field : "ARTCODORI",config 	: {width: 0,	hidden: true,	title: 'Cod.Orig.'}},
	     			 {field : "STKCODBAR",config 	: {width: 0,	hidden: true,	title: 'Stk.Cod.Bar.'}} ];
			
	tblequalsStkLin =	[{fldorig : "ARTCODINT",	flddest : "artcodint"},
						 {fldorig : "ARTCODIGO",	flddest : "artcodigo"},
						 {fldorig : "ARTDESCRI",	flddest : "artdescri"},
						 {fldorig : "STKUNICOD",	flddest : "stkunicod"},
						 {fldorig : "UNICODIGO",	flddest : "unicodigo"},
						 {fldorig : "UNIDESCRI",	flddest : "unidescri"},
						 {fldorig : "STKCNVUC",		flddest : "stkcnvuc"},
						 {fldorig : "STKCSTUC",		flddest : "stkcstuc"},
						 {fldorig : "STKCNVMA",		flddest : "stkcnvma"},
						 {fldorig : "STKCSTMA",		flddest : "stkcstma"},
						 {fldorig : "STKDEPDDE",	flddest : "stkdepdde"},
						 {fldorig : "STKDEPDDEDES",	flddest : "stkdepddedes"},
						 {fldorig : "STKDEPHTA",	flddest : "stkdephta"},
						 {fldorig : "STKDEPHTADES",	flddest : "stkdephtades"},
						 {fldorig : "STKMOVCNT",	flddest : "stkmovcnt"},
						 {fldorig : "STKCNVCNT",	flddest : "stkcnvcnt"},
						 {fldorig : "ARTCODORI",	flddest : "artcodori"},
						 {fldorig : "STKCODBAR",	flddest : "stkcodbar"} ];
	
	datparamsStkLin = ['stkcmpcod','stkcmpunic','stkcmpcst'];
	
	//LookUp de Proveedores
	tblfldPro =	[{field : "PROCODINT",config : {width: 5,	hidden: true,	title: 'Cod.Int.'}},
				 {field : "PROCODIGO",config : {width: 10,	hidden: false,	title: 'Codigo'}},
				 {field : "PRODESCRI",config : {width: 300,	hidden: false,	title: 'Descripcion'}}];
			
	tblequalsPro =	[{fldorig : "PROCODINT",flddest : "procodint"},
					 {fldorig : "PROCODIGO",flddest : "procodigo"},
					 {fldorig : "PRODESCRI",flddest : "prodescri"} ];
	
	//LookUp de Depositos
	tblfldDep =	[{field : "STKDEPCOD",	config 	: {width: 10,	hidden: false,	title: 'Codigo'}},
	     		 {field : "STKDEPDES",	config 	: {width: 300,	hidden: false,	title: 'Deposito'}} ];
			
	tblequalsDepDde =	[{fldorig : "STKDEPCOD"		,flddest : "stkdepdde"},
					 	 {fldorig : "STKDEPDES"		,flddest : "stkdepddedes"} ];
	tblequalsDepHta =	[{fldorig : "STKDEPCOD"		,flddest : "stkdephta"},
					 	 {fldorig : "STKDEPDES"		,flddest : "stkdephtades"} ];
	
	//LookUp de Clientes
	tblfldCli =	[{field : "CLICODINT",config : {width: 5,	hidden: true,	title: 'Cod.Int.'}},
				 {field : "CLICODIGO",config : {width: 10,	hidden: false,	title: 'Codigo'}},
				 {field : "CLIDESCRI",config : {width: 300,	hidden: false,	title: 'Descripcion'}},
				 {field : "CLIDIRECC",config : {width: 200,	hidden: false,	title: 'Direccion'}}];
			
	tblequalsCli =	[{fldorig : "CLICODINT",flddest : "clicodint"},
					 {fldorig : "CLICODIGO",flddest : "clicodigo"},
					 {fldorig : "CLIDESCRI",flddest : "clidescri"} ];	
					 
					 
	//LookUp de Buscador de Documentos de Venta
	tblfldDocVta =	[{field : "DOCREASUC",	config : {width: 5,		hidden: false,	title: 'Suc.'}},
					{field : "DOCREALET",	config : {width: 1,		hidden: false,	title: ' '}},
					{field : "DOCREANRO",	config : {width: 100,	hidden: false,	title: 'Numero'}},
					{field : "CLICODINT",	config : {width: 2,		hidden: true,	title: 'CodInt'}},
					{field : "CLICODIGO",	config : {width: 10,	hidden: false,	title: 'Cod.'}},
					{field : "CLIDESCRI",	config : {width: 300,	hidden: false,	title: 'Cliente'}},
					{field : "DOCLEYEN1",	config : {width: 300,	hidden: false,	title: 'Leyenda'}},
					{field : "DOCNRO",		config : {width: 300,	hidden: true,	title: 'DocInt'}},
					{field : "DOCFCH",		config : {width: 170,	hidden: false,	title: 'Fecha'}} ];
			
	tblequalsDocVta =	[{fldorig : "DOCREASUC",flddest : "docreasuc"},
						 {fldorig : "DOCREALET",flddest : "docrealet"},
						 {fldorig : "DOCREANRO",flddest : "docreanro"},
						 {fldorig : "DOCFCH",flddest : "docfch"},
						 {fldorig : "DOCLEYEN1",flddest : "docleyen1"},
						 {fldorig : "DOCNRO",flddest : "docnro"} ];
}
//-------------------------------------------------------------------------------------------
function loadPage(){
	//- - - - - - - - - - - - - - - - - - - - - - - - - - -  
	//LookUp de Comprobantes de Stock			
	$('img[id="imglkstkcmp"]').lookup( {tblfile: 'tblstkcomputil.php', fields : tblfldStkCmp, fldequals : tblequalsStkCmp, funcend: 'DocVta(); Proveed(); Clientes(); RemitoManual(); ChkDepositos(); ChkLote();' } );
	$('input[id="stkcmpcod"]').lookup( {tblfile: 'tblstkcomputil.php', fields : tblfldStkCmp, fldequals : tblequalsStkCmp, funcend: 'DocVta(); Proveed(); Clientes(); RemitoManual(); ChkDepositos(); ChkLote();' } );
	//- - - - - - - - - - - - - - - - - - - - - - - - - - -
	 
	//Lookup de Clientes
	$('img[id="imglkcliente"]').lookup( {tblfile: 'tblclientes.php', fields : tblfldCli, fldequals : tblequalsCli, funcend: 'bsqDocumentoVta();'} );
	$('input[id="clicodigo"]').lookup( {tblfile: 'tblclientes.php', fields : tblfldCli, fldequals : tblequalsCli, funcend: 'bsqDocumentoVta();' } );
	
	//Lookup de Buscador de Docs.Venta
	$('img[id="imglkdocventa"]').lookup( {tblfile: 'tbldocsventarem.php', fields : tblfldDocVta, fldequals : tblequalsDocVta, funcend: 'bsqDocumentoVta();' } );
	
	//Lookup de Proveedores
	$('img[id="imglkproveedor"]').lookup( {tblfile: 'tblproveedores.php', fields : tblfldPro, fldequals : tblequalsPro } );
	$('input[id="procodigo"]').lookup( {tblfile: 'tblproveedores.php', fields : tblfldPro, fldequals : tblequalsPro } );

	$('#stkreasuc').autoNumeric({aSep: '', mDec: '0',  vMin: '0', vMax: '9999' });
	$('#stkreanro').autoNumeric({aSep: '', mDec: '0', vMin: '0', vMax: '99999999' });
			
	 
     $('#stkcmpcod').focus(function(){
    	 ArrValAnt[this.id] = $(this).val(); 
     });
          
    /*$('input[type=text]').not('#stkmovobs').focus(function (){
 		this.select();	
 	});**/	
 	$('input[type=text]').not('#stkmovobs').click(function (){
 		this.select();	
 	});
	
	//Loteo en Sistema de Stock	
	/*if(Conf_StkLoteo == 0){
		$('input[id="stkmovlot"]').hide();
		$('td[id="stkloterow"]').hide();
		$('#stklotecol').hide();
	}*/
}
//-------------------------------------------------------------------------------------------
function setFuncGrid(tbl){
	
	tbl.find('#stkcnvcnt').autoNumeric({aSep: '.', aDec: ',', mDec: Conf_CfgDecimCantidad, vMin: '-999999.9999', vMax: '999999.9999'  });
	tbl.find('#stkcnvuc').autoNumeric({aSep: '.', aDec: ',', mDec: Conf_CfgDecimCosto, vMin: '0', vMax: '999999.9999'  });
	
	tbl.find('#stkcnvcnt').lookup( {tblfile: 'stklinvalida.php', fields : tblfldStkLin, fldequals : tblequalsStkLin, params: datparamsStkLin, funcend: 'ArtLinVal(LKobjgroup);', viewlookup:false, pkfield:"artcodigo" } );
	
	//LookUp de Articulos
	tbl.find('#imglkarticulo').lookup( {tblfile: 'stklinvalida.php', fields : tblfldStkLin, fldequals : tblequalsStkLin, params: datparamsStkLin, funcend: 'ArtLinVal(LKobjgroup);' } );
	tbl.find('#artcodigo').lookup( {tblfile: 'stklinvalida.php', fields : tblfldStkLin, fldequals : tblequalsStkLin, params: datparamsStkLin, funcend: 'ArtLinVal(LKobjgroup);' } );
		
	//Validacion al Ingresar Cantidad	
	/*tbl.find('#stkcnvcnt').change(function (){	//Cambio de Cantidad
		$(this).parent().parent().find('#artcodigo').change();		
	});*/
	
	//Validacion al Ingresar Costo	
	tbl.find('#stkcnvuc').change(function (){	//Cambio de Costo
		$(this).parent().parent().find('#artcodigo').change();		
	});

	//LookUp de Depositos Desde
	tbl.find('#imglkdepdde').lookup( {tblfile: 'tbldepositos.php', fields : tblfldDep, fldequals : tblequalsDepDde, funcend: 'valDetalle(LKobj);' } );
	tbl.find('#stkdepdde').lookup( {tblfile: 'tbldepositos.php', fields : tblfldDep, fldequals : tblequalsDepDde, funcend: 'valDetalle(LKobj);' } );

	//LookUp de Depositos Hasta
	tbl.find('#imglkdephta').lookup( {tblfile: 'tbldepositos.php', fields : tblfldDep, fldequals : tblequalsDepHta, funcend: 'valDetalle(LKobj);' } );
	tbl.find('#stkdephta').lookup( {tblfile: 'tbldepositos.php', fields : tblfldDep, fldequals : tblequalsDepHta, funcend: 'valDetalle(LKobj);' } );
	
	tbl.find('input[type=text]').focus(function (){
		this.select();	
	});	
	tbl.find('input[type=text]').click(function (){
		this.select();	
	});
}
//-------------------------------------------------------------------------------------------
function setKeyFocus(e, strobj){
	if (!e) e = window.event;
	if (e.keyCode) 		code = e.keyCode;
	else if (e.which) 	code = e.which; 

	if(code == 13){
		$('#'+strobj).focus();
	}
}
//-------------------------------------------------------------------------------------------
function setKeyFocusGrid(e, strobj, row){
	if (!e) e = window.event;
	if (e.keyCode) 		code = e.keyCode;
	else if (e.which) 	code = e.which; 

	if(code == 13){
		row.find('#'+strobj).focus();		
	}
}
//-------------------------------------------------------------------------------------------
function setKeyFocusGrid_Articulo(e, obj){	
	var stkmovlot	= null;
	var stkcnvcnt	= null;
	var setfocus	= null;
	var row			= null;
	var rownext		= null;
	
	if (!e) e = window.event;
	if (e.keyCode) 		code = e.keyCode;
	else if (e.which) 	code = e.which; 

	if(code == 13){
		row 		= obj.parent().parent().parent();
		rownext		= obj.parent().parent().parent().next();
				
		stkmovlot	= row.find('#stkmovlot');		
		stkcnvcnt	= row.find('#stkcnvcnt');	
		setfocus	= 0;
			
		if(stkmovlot.is(":visible")){
			stkmovlot.focus();	
			setfocus=1;	
		}
		if(stkcnvcnt.is(":visible") && setfocus == 0){
			stkcnvcnt.focus();
			setfocus=1;	
		}
	}
}
//-------------------------------------------------------------------------------------------
function setKeyFocusGrid_Cantid(e, obj){	
	var artcodigo	= null;
	var stkcnvuc	= null;	
	var stkdepdde	= null;
	var stkdephta	= null;
	var setfocus	= null;
	var row			= null;
	var rownext		= null;
	
	if (!e) e = window.event;
	if (e.keyCode) 		code = e.keyCode;
	else if (e.which) 	code = e.which; 

	if(code == 13){
		row 		= obj.parent().parent().parent();
		rownext		= obj.parent().parent().parent().next();
				
		artcodigo	= row.find('#artcodigo');		
		stkcnvuc	= row.find('#stkcnvuc');
		stkdepdde	= row.find('#stkdepdde');
		stkdephta	= row.find('#stkdephta');	
		setfocus	= 0;
			
		if(stkcnvuc.is(":not(:disabled)")){
			stkcnvuc.focus();	
			setfocus=1;	
		}
		if(stkdepdde.is(":visible") && setfocus == 0){
			stkdepdde.focus();
			setfocus=1;	
		}
		if(stkdephta.is(":visible") && setfocus == 0){
			stkdephta.focus();
			setfocus=1;	
		}
		if(setfocus==0)	{
			rownext.find('#artcodigo').focus();
		}
	}
}
//-------------------------------------------------------------------------------------------
function setKeyFocusGrid_Costo(e, obj){	
	var stkdepdde	= null;
	var stkdephta	= null;
	var setfocus	= null;
	var row			= null;
	var rownext		= null;
	
	if (!e) e = window.event;
	if (e.keyCode) 		code = e.keyCode;
	else if (e.which) 	code = e.which; 

	if(code == 13){
		row 		= obj.parent().parent().parent();
		rownext		= obj.parent().parent().parent().next();
				
		stkdepdde	= row.find('#stkdepdde');
		stkdephta	= row.find('#stkdephta');				
		setfocus	= 0;
			
		if(stkdepdde.is(":visible")){
			stkdepdde.focus();
			setfocus=1;	
		}
		if(stkdephta.is(":visible") && setfocus == 0){
			stkdephta.focus();
			setfocus=1;	
		}
		if(setfocus==0)	{
			rownext.find('#artcodigo').focus();
		}
	}
}
//-------------------------------------------------------------------------------------------
function setKeyFocusGrid_DepDde(e, obj){
	var stkdephta	= null;
	var setfocus	= null;
	var row			= null;
	var rownext		= null;
	
	if (!e) e = window.event;
	if (e.keyCode) 		code = e.keyCode;
	else if (e.which) 	code = e.which; 

	if(code == 13){
		row 		= obj.parent().parent().parent();
		rownext		= obj.parent().parent().parent().next();
		
		stkdephta	= row.find('#stkdephta');				
		setfocus	= 0;
		
		if(stkdephta.is(":visible")){
			stkdephta.focus();
			setfocus=1;	
		}
		if(setfocus==0)	{
			rownext.find('#artcodigo').focus();
		}
	}
}
//-------------------------------------------------------------------------------------------
function setKeyFocus_FchMov(e, obj){
	var stkmovobs	= null;
	var docreasuc	= null;
	var stkreasuc	= null;
	var procodigo	= null;
	
	if (!e) e = window.event;
	if (e.keyCode) 		code = e.keyCode;
	else if (e.which) 	code = e.which; 

	if(code == 13){		
		stkmovobs	= $('#stkmovobs');
		docreasuc	= $('#docreasuc');
		stkreasuc	= $('#stkreasuc');
		procodigo	= $('#procodigo');
		
		if(stkreasuc.is(":visible")){
			stkreasuc.focus();
		}else if(docreasuc.is(":visible")){
			docreasuc.focus();
		}else if(procodigo.is(":visible")){
			procodigo.focus();
		}else{
			stkmovobs.focus();
		}
	}
}
//-------------------------------------------------------------------------------------------
function setKeyFocusGrid_DepHta(e, obj){	
	var setfocus	= null;
	var row			= null;
	var rownext		= null;
	
	if (!e) e = window.event;
	if (e.keyCode) 		code = e.keyCode;
	else if (e.which) 	code = e.which; 

	if(code == 13){
		row 		= obj.parent().parent().parent();
		rownext		= obj.parent().parent().parent().next();
		
		rownext.find('#artcodigo').focus();		
	}
}
//-------------------------------------------------------------------------------------------
function setKeyFocus_StkReaNro(e, obj){
	var stkmovobs	= null;
	var docreasuc	= null;	
	var procodigo	= null;
	
	if (!e) e = window.event;
	if (e.keyCode) 		code = e.keyCode;
	else if (e.which) 	code = e.which; 

	if(code == 13){		
		stkmovobs	= $('#stkmovobs');		
		docreasuc	= $('#docreasuc');
		procodigo	= $('#procodigo');
		
		if(docreasuc.is(":visible")){
			docreasuc.focus();
		}else if(procodigo.is(":visible")){
			procodigo.focus();
		}else{
			stkmovobs.focus();
		}
	}
}
//-------------------------------------------------------------------------------------------
function ChkDepositos(){ //Checkeo el Depositos	
	var comp 		= null;
	var stkcmpdde	= null;
	var stkcmphta	= null;
	var stkcmpcst	= null;	
	
	comp 		= $('#stkcmpcod').val();		//Comprobante - Codigo
	stkcmpdde	= $('#stkcmpdepdde').val();		//Comprobante - Deposito Desde
	stkcmphta	= $('#stkcmpdephta').val();		//Comprobante - Deposito Desde
	stkcmpcst	= $('#stkcmpcst').val();		//Permite Ingreso de Costo	

	$('th[id^="stkdepddecol"]').show();			//Columnas - Dep Desde
	$('td[id^="stkdepdderow"]').show();			//Fila - Dep Desde
	
	$('th[id^="stkdephtacol"]').show();			//Columnas - Dep Hasta
	$('td[id^="stkdephtarow"]').show();			//Fila - Dep Hasta
	
	if(comp != ''){		
		//Seteo Dep.Dde
		if(stkcmpdde != '---' && stkcmpdde != ''){
			$('th[id^="stkdepddecol"]').hide();	//Columnas
			$('td[id^="stkdepdderow"]').hide();	//Fila
		}
		if(stkcmphta != '---' && stkcmphta != ''){
			$('th[id^="stkdephtacol"]').hide();	//Columnas
			$('td[id^="stkdephtarow"]').hide();	//Fila
		}
		
		$.each($('input[id="artcodint"]'),function(i){
			var row = $(this).parent().parent();
			
			if(this.value == ''){
				row.find('#stkdepdde').val('');
				row.find('#stkdephta').val('');
			}else{				
				if(stkcmpdde != '---' && stkcmpdde != ''){
					row.find('#stkdepdde').val(stkcmpdde);
				}
				if(stkcmphta != '---' && stkcmphta != ''){
					row.find('#stkdephta').val(stkcmphta);					
				}
			}
			
		});
	}
		
	//Permite el Ingreso de Costo de Linea
	if(stkcmpcst == '1'){
		$('input[id="stkcnvuc"]').removeAttr('disabled');
	}else{
		$('input[id="stkcnvuc"]').attr('disabled', 'disabled');
	}
	
}
//-------------------------------------------------------------------------------------------
function ChkLote(){ //Checkeo el Depositos	
	var stkreqlot	= null;
	
	comp 		= $('#stkcmpcod').val();		//Comprobante - Codigo
	stkreqlot	= $('#stkreqlot').val();		//Comprobante - Deposito Desde
	
	$('th[id^="stklotecol"]').show();			//Columnas - Dep Desde
	$('td[id^="stkloterow"]').show();			//Fila - Dep Desde
	
	
	if(stkreqlot ==  '0' || stkreqlot == 0 || stkreqlot == ''){		
		
		$('th[id^="stklotecol"]').hide();	//Columnas
		$('td[id^="stkloterow"]').hide();	//Fila
		
		$.each($('input[id="artcodint"]'),function(i){
			var row = $(this).closest("tr");
			row.find('#stkmovlot').val('');
		});
	}
}
//-------------------------------------------------------------------------------------------
function ArtLinVal(LKobjgroup){	
	var stkcmpdde	= null;
	var stkcmphta	= null;	
	var stkdepdde 	= null;
	var stkdephta 	= null;
	
	stkcmpdde	= $('#stkcmpdepdde');		//Comprobante - Deposito Desde
	stkcmphta	= $('#stkcmpdephta');		//Comprobante - Deposito Desde
	
	stkdepdde = $('input[id="stkdepdde"][lookupgroup|="'+LKobjgroup+'"]');
	stkdephta = $('input[id="stkdephta"][lookupgroup|="'+LKobjgroup+'"]');
	
	if(stkdepdde.val() == '' && stkcmpdde.val() != ''){
		stkdepdde.val( stkcmpdde.val() );
	}
	if(stkdephta.val() == '' && stkcmphta.val() != ''){
		stkdephta.val( stkcmphta.val() );
	}
}
//-------------------------------------------------------------------------------------------	
function addAllReg(cant){	
	var i = null;
	for(i=0; i<cant; i++){
		TblAddRegistro();
	}
	ChkDepositos();
}
//-------------------------------------------------------------------------------------------
function delRegistro(strreg){ //Eliminacion de Detalle de pedido
	$('input[lookupgroup="'+strreg+'"]').val('');	
}	
//-------------------------------------------------------------------------------------------
function TblAddRegistro(){
	var datatable 	= null;
	var dataulttr	= null;	
	var ultreg		= null;
	var htmlulttr	= null;	
	var rexp 		= null;
	
	datatable 	= $('#TblABM');							//Tabla
	dataulttr	= $('#TblABM tbody:last').clone(true);	//Ultima Fila en Tabla	
	ultreg		= parseInt(datatable.find('input[id^="regtable"]').filter(':last').val());	//Valor de Registro de Ultima Fila (regtable)
	htmlulttr	= dataulttr.html();												//Contenido HTML del TR, ultima fila.		

	//Cambio el Viejo Siguiente Registro por uno nuevo
	rexp = new RegExp(ultreg+1,"g"); //Se aplica una expresion regular, para que reemplaze todas las apariciones.
	htmlulttr = htmlulttr.replace(rexp, ultreg+2);
				
	//Cambio el Viejo Registro por uno nuevo			
	rexp = new RegExp(ultreg,"g"); //Se aplica una expresion regular, para que reemplaze todas las apariciones.
	htmlulttr = htmlulttr.replace(rexp, ultreg+1);
	
	dataulttr.html(htmlulttr);										
	datatable.append(dataulttr);
	
	$.each(dataulttr.find('input[id!="regtable"]'), function (){
		this.value = "";
	});
	
	setFuncGrid(dataulttr);
}
//-------------------------------------------------------------------------------------------
function guardarStockMsg(){
	var msgtxt		= null;
	var botones		= null;
	var permcli		= null; //permite el ingreso de cliente: 1= SI 0=NO
	var cliente 	= null;
	
	reqobs 		= parseInt($('#stkreqobs').val());
	permcli 	= $('#stkcmpcli').val();
	cliente 	= $('#clicodint').val();
	
	if(reqobs > 0 && $('#stkmovobs').val().trim().length < reqobs ){
		ViewMsg('Se necesita mas informacion en la observacion');
		$('#stkmovobs').focus();
	}else if(permcli == 1 && cliente == ''){
		ViewMsg('El cliente es Obligatorio');
		$("#clicodigo").focus();
	}else{
		msgtxt 	= 'Confirma el Movimiento de Stock?';		
		botones	= {yes: "Si", no: "No"};
		
		CloseView('{winid_vsldta}'); 
		if(CtrFormsValues(document.FrmMst)==0){		
			Ext.MessageBox.show({
			   title:'GENERAR MOVIMIENTO DE STOCK',
			   msg: msgtxt,
			   buttons: botones,
			   fn: generaRemitoMsg,					           
			   icon: Ext.MessageBox.QUESTION
		   });
		}
	}
}
//-------------------------------------------------------------------------------------------
function generaRemitoMsg(resp){
	var stkgennum = null;
	
	if(resp == 'yes'){
		stkgennum = $('#stkgennum').val();
		if(stkgennum == 'P'){ //Pregunto si genera numero de Remito
			Ext.MessageBox.show({
			   title:'GENERA REMITO',
			   msg: 'Desea generar un numero de Remito?',
			   buttons: {yes: "Si", no: "No", cancel: "Cancelar"},
			   fn: generaRemito,					           
			   icon: Ext.MessageBox.QUESTION
		   });
		}else{ //No se pregunta por generar o no remito
			if(stkgennum == 'S'){
				generaRemito('yes');	
			}else{
				generaRemito('no');

			}
			
		}
	}	
}
//-------------------------------------------------------------------------------------------
function generaRemito(resp){
	if(resp == 'yes'){
		$('#stkimp').val('S');		
	}else{
		$('#stkimp').val('N');
	}
	if(resp != 'cancel'){
		guardarStock('yes');
	}
}
//-------------------------------------------------------------------------------------------
function guardarStock(resp){
	if(resp == 'yes'){
		if(CtrFormsValues(document.FrmMst)==0){		
			$('#tblstklinJSON').val( getTableJSON('TblABM') );									 
			Save(document.FrmMst,'grb.php');
		}
	}
}
//-------------------------------------------------------------------------------------------
function chgComprobanteMsg(obj){
	if(ArrValAnt[obj.id] != ''){
		var msgtxt	= null;
		var botones	= null;
	
		msgtxt 	= 'Confirma el Cambio de Comprobante?, <br> El mismo eliminara todo el detalle ingresado';		
		botones	= {yes: "Si", no: "No"};
		
		CloseView('{winid_vsldta}'); 
		if(CtrFormsValues(document.FrmMst)==0){		
			Ext.MessageBox.show({
	           title:'MODFICAR COMPROBANTE DE STOCK',
	           msg: msgtxt,
	           buttons: botones,
	           fn: chgComprobante,					           
	           icon: Ext.MessageBox.QUESTION
	       });
		}
	}
}
//-------------------------------------------------------------------------------------------
function chgComprobante(resp){
	if(resp == 'yes'){
		$('input[lookupgroup^="stklin_"]').val('');	
	}else{
		$('#stkcmpcod').val(ArrValAnt['stkcmpcod']);
		ArrValAnt['stkcmpcod'] = '';
		$('#stkcmpcod').change();
	}
}
//-------------------------------------------------------------------------------------------
function DocVta(){	
	if($('#stkcmpvta').val() == 1){
		$('#trdocvta').show();
		$('tbody[id="brw"]:gt(0)').hide();
		$('tbody[id="brw"]:eq(0)').hide();
		$('input[id="artcodigo"]:gt(0)').attr('readonly',true);
		$('input[id="artcodigo"]:eq(0)').attr('readonly',true);
		$('img[id="imglkarticulo"]:gt(0)').hide();
		$('img[id="imglkarticulo"]:eq(0)').hide();		
		$('#docreasuc').val('');
		$('#docrealet').val('');
		$('#docreanro').val('');
		$('#docdata').val('');	
	}else if($('#stkbsqvta').val() == 1){
		$('#trdocvta').hide();
		$('tbody[id="brw"]:gt(0)').hide();
		$('tbody[id="brw"]:eq(0)').hide();
		$('input[id="artcodigo"]:gt(0)').attr('readonly',true);
		$('input[id="artcodigo"]:eq(0)').attr('readonly',true);
		$('img[id="imglkarticulo"]:gt(0)').hide();
		$('img[id="imglkarticulo"]:eq(0)').hide();		
		$('#docreasuc').val('');
		$('#docrealet').val('');
		$('#docreanro').val('');
		$('#docdata').val('');
	}else{
		$('#trdocvta').hide();
		$('tbody[id="brw"]:gt(0)').show();
		$('tbody[id="brw"]:eq(0)').show();
		$('input[id="artcodigo"]:gt(0)').attr('readonly',false);
		$('input[id="artcodigo"]:eq(0)').attr('readonly',false);
		$('img[id="imglkarticulo"]:gt(0)').show();
		$('img[id="imglkarticulo"]:eq(0)').show();
	}
}
//-------------------------------------------------------------------------------------------
function Proveed(){
	if($('#stkcmppro').val() == 1){
		$('#trproveedor').show();	
	}else{
		$('#trproveedor').hide();
	}
}
//-------------------------------------------------------------------------------------------
function Clientes(){
	if($('#stkcmpcli').val() == 1){
		$('#trclientes').show();	
	}else{
		$('#trclientes').hide();
	}
}
//-------------------------------------------------------------------------------------------
function bsqDocumentoVta(){
	if($('#stkbsqvta').val() == 1 || $('#stkcmpvta').val() == 1){
		var docreasuc = null;
		var docrealet = null;
		var docreanro = null;
		var clicodint = null;
		
		docreasuc = $('#docreasuc').val();
		docrealet = $('#docrealet').val();
		docreanro = $('#docreanro').val();
		clicodint = $('#clicodint').val();

		//Busco el Documento de Ventas
		if((docreasuc != '' && docrealet != '' && docreanro != '') || clicodint != ''){
			$.ajax({
			  type: "POST",
			  url: "getdocvta.php",
			  data: { 
						docreasuc: docreasuc,
						docrealet: docrealet,
						docreanro: docreanro,
						clicodint: clicodint
					}		  
			}).done(function( rsp ) {					
				//console.log(rsp)
				tbllin = $.parseJSON(rsp);
				
				//Agrego Filas
				//addAllReg(30);
				$('tbody[id="brw"]:eq(0)').show();
				$('tbody[id="brw"]:gt(0)').show();
				$('input[lookupgroup^="stklin_"]').val('');	
				
				var docfch 		= $('#docfch');
				var docleyen1	= $('#docleyen1');
				var docnro 		= $('#docnro');
				var datatable 	= $('#TblABM');
				var clicodint 	= $('#clicodint');
				var clicodigo 	= $('#clicodigo');
				var clidescri 	= $('#clidescri');
							
				var i 			= 0;
				docfch.val('');
				docleyen1.val('');
				docnro.val('');
				
				$.each(tbllin, function() {		
					if(this.clicodigo){
						if(this.docnro == ''){
							$('#docreasuc').val('');
							$('#docrealet').val('');
							$('#docreanro').val('');
						}
						
						docnro.val(this.docnro);					
						docfch.val(this.docfch);
						docleyen1.val(this.docleyen1);
						clicodint.val(this.clicodint);
						clicodigo.val(this.clicodigo);
						clidescri.val(this.clidescri);
					
						$('input[id="docitm"]:eq('+i+')').val(this.docitm);
						$('input[id="artcodint"]:eq('+i+')').val(this.artcodint);
						$('input[id="artcodigo"]:eq('+i+')').val(this.artcodigo);
						$('input[id="artdescri"]:eq('+i+')').val(this.artdescri);
						$('input[id="stkmovcnt"]:eq('+i+')').val(this.doccantid);
						$('input[id="stkcnvcnt"]:eq('+i+')').val(this.doccantid);
						$('input[id="stkunicod"]:eq('+i+')').val(this.unicodigo);
						$('input[id="unicodigo"]:eq('+i+')').val(this.unicodigo);
						$('input[id="unidescri"]:eq('+i+')').val(this.unidescri);
						
						$('input[id="stkcstuc"]:eq('+i+')').val(this.artlstcuc);
						$('input[id="stkcstma"]:eq('+i+')').val(this.artlstcma);
						$('input[id="stkcnvuc"]:eq('+i+')').val(this.artlstcuc);
						$('input[id="stkcnvma"]:eq('+i+')').val(this.artlstcma);				
						$('input[id="stkmovlot"]:eq('+i+')').val(this.stkmovlot);
						
						i++;
					}
				});
				
				if(i == 0){
					ViewMsg('No hay Detalle de Documento');
					$('#docnro').val('');
					$('#docreasuc').val('');
					$('#docrealet').val('');
					$('#docreanro').val('');
					$('#clicodint').val('');
					$('#clicodigo').val('');
					$('#clidescri').val('');
				}
				
				//Oculto las filas siguientes			
				$('tbody[id="brw"]:eq('+i+')').hide();
				$('tbody[id="brw"]:gt('+i+')').hide();
							
				ChkDepositos();
			});			
		}else{
			if(docreasuc == '' || docreanro == '' || docrealet == ''){
				$('#docnro').val('');
			}
			
			$('tbody[id="brw"]:eq(0)').hide();
			$('tbody[id="brw"]:gt(0)').hide();
						
			ChkDepositos();
		}	
	}
}
//-------------------------------------------------------------------------------------------
function valDetalle(LKobj){
	var row = LKobj.parent().parent();
	row.find('#stkcnvcnt').change();
}
//-------------------------------------------------------------------------------------------
function RemitoManual(){
	if($('#stkgennum').val()=='M'){//Ingreso de Nro.Remito Manual
		$('#trremitonro').show();
	}else{
		$('#trremitonro').hide();
	}
}
//-------------------------------------------------------------------------------------------