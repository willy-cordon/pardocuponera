//-----------------------------------------------------------------------------------
var TblColorDel		= 'red none repeat scroll 0% 0%';	//Color de Registro a Eliminar
var TblEstadoVDel	= 'VD';								//Estado de Registro a Eliminar (Reg. existente)
var TblEstadoSDel	= 'SD';								//Estado de Registro a Eliminar (Reg. existente modificado)
var TblEstadoNDel	= 'ND';								//Estado de Registro a Eliminar (Reg. nuevo)
var TblEstadoNew	= 'I';								//Estado de Registro Nuevo
var TblEstadoDefNew	= 'DEF';							//Estado de Registro Nuevo por Defecto
var TblEstadoSav	= 'S';								//Estado de Registro Existente Guardar (Save)
var TblEstadoEx		= 'EX';								//Estado de Registro Existente
var TblCantReg		= 0;								//Cantidad de Registros Permitidos en la Tabla, si es 0, no controla

//-----------------------------------------------------------------------------------
function TblAddReg(Frm,NameTable){	//Insercion de Registro en Tabla
	var TblABM 	= document.getElementById(NameTable);	
	
	var ultrow 	= TblABM.rows.length-1;						//Total de filas de la Tabla (puede incluir otros datos)
	var totcell = TblABM.rows.item(ultrow).cells.length; 	//Cantidad de Celdas
	var totreg	= 0;										//Cantidad de Filas de la Tabla para ABM
	var ultreg  = 0;										//Ultimo Nro de Registro
	
	if(ultrow > 0){
		totreg	= Frm.elements['regtable[]'].length; 				//Cantidad de Filas de la Tabla para ABM
		ultreg	= parseInt(Frm.elements['regtable[]'].item(totreg-1).value);	//Ultimo Nro de Registro
	}	
	
	//Controlo la cant. de registros. La tabla utiliza rango 100000 > 9999999
	if(ultreg >= TblCantReg+100000 && TblCantReg!=0){
		//No inserta registro, porque supero el maximo
		ViewMsg('Supera la Cantidad Maxima de Registros Permitidos ('+TblCantReg+')');
	}else{	//Sino inserto uno nuevo
	
		var newrow 	= TblABM.insertRow(ultrow+1); //Nueva Fila
		for(var i=0; i<totcell; i++ ){ //Creo las Nuevas Celdas			 
			HTMLcell 			= TblABM.rows.item(ultrow).cells.item(i).innerHTML;
			newcell				= newrow.insertCell(i);
	
			//Cambio el Registro de la Tabla		
			HTMLcell	= HTMLcell.replace(eval("/"+(ultreg+1)+"/gi"), parseInt(ultreg)+2);		
			HTMLcell	= HTMLcell.replace(eval("/"+ultreg+"/gi"), parseInt(ultreg)+1);		
			newcell.innerHTML	= HTMLcell; //Creo la Celda igual a la ultima		
		}		
	}	
}
//-----------------------------------------------------------------------------------
function TblDelReg(reg,Frm,NameTable){	 //Eliminacion de Registros de Tabla
	var TblABM 	= document.getElementById(NameTable);	
	var totreg	= Frm.elements['regtable[]'].length;	

	for(var i=0; i<totreg; i++){ //Recorro todos los registro
		if(Frm.elements['regtable[]'].item(i).value == reg){ //Elimino el Registro	
			estreg = Frm.elements['estregtable[]'].item(i);
			
			switch(estreg.value){
				case TblEstadoVDel: //Estado de Reg. Existente Eliminado
					TblABM.rows.item(i+1).style.background 	= '';
					estreg.value							= '';
					break;
					
				case TblEstadoNDel: //Estado de Reg. Nuevo Eliminado
					TblABM.rows.item(i+1).style.background 	= '';
					estreg.value							= TblEstadoNew; //Vuelve a estar apto para insertarse
					break;
					
				case TblEstadoNew: //Estado de Reg. Nuevo a Insertar
					TblABM.rows.item(i+1).style.background 	= TblColorDel;
					estreg.value							= TblEstadoNDel;
					break;
				
				case TblEstadoSav: //Estado de Reg. Existente a Guardar
					TblABM.rows.item(i+1).style.background 	= TblColorDel;
					estreg.value							= TblEstadoSDel;
					break;
					
				case TblEstadoSDel: //Estado de Reg. Existente a Guardar Eliminado
					TblABM.rows.item(i+1).style.background 	= '';
					estreg.value							= TblEstadoSav;
					break;
					
				default : //Registro Existente
					TblABM.rows.item(i+1).style.background 	= TblColorDel;
					estreg.value							= TblEstadoVDel;				
					break;					
			}			
		}			
	}	
}
//-----------------------------------------------------------------------------------
function TblSetSave(reg,Frm,NameTable){ //Coloco el Estado en Apto para Guardar
	var TblABM 	= document.getElementById(NameTable);	
	var ultrow 	= TblABM.rows.length-1;
	var totreg	= 0;
	
	if(ultrow > 0){
		totreg = Frm.elements['regtable[]'].length;
	}

	for(var i=0; i<totreg; i++){ //Recorro todos los registro
		if(Frm.elements['regtable[]'].item(i).value == reg){ //Elimino el Registro				
			estreg = Frm.elements['estregtable[]'].item(i);
			
			switch(estreg.value){					
				case TblEstadoNDel: //Estado de Reg. Nuevo Eliminado
					TblABM.rows.item(i).style.background 	= '';
					estreg.value							= TblEstadoNew; //Vuelve a estar apto para insertarse
					break;
				
				case TblEstadoNew: //Estado de Reg. Nuevo Eliminado					
					break;
					
				case TblEstadoDefNew: //Estado de Reg. Nuevo reg por Defecto en blanco		
					TblABM.rows.item(i).style.background 	= '';
					estreg.value							= TblEstadoNew;
					break;
					
				case '': //Estado de Reg. Existente		
					TblABM.rows.item(i).style.background 	= '';
					estreg.value							= TblEstadoSav;
					break;
					
				default: //Registro Existente
					TblABM.rows.item(i).style.background 	= '';
					estreg.value							= TblEstadoSav;					
					break;					
			}	
			
		}			
	}
	
	if(totreg == 0){
		TblABM.rows.item(i).style.background 	= '';
		Frm.elements['estregtable[]'].value		= TblEstadoNew;
	}
	
}
//-----------------------------------------------------------------------------------
function TblLKCopy(reg,FrmDatos,Frm){ //Copia datos al registro de la tabla posicionado
	for(var i=0; i < FrmDatos.elements.length; i++){
		var objname = FrmDatos.elements.item(i).name + reg;
		if(Frm.elements[objname]){
			if(FrmDatos.elements.item(i).value == '---'){
				Frm.elements[objname].value = '';
			}else{			
				Frm.elements[objname].value = FrmDatos.elements.item(i).value;
			}
		}
	}
}
//-----------------------------------------------------------------------------------
