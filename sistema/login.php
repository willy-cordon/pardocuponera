<?php
	if(!isset($_SESSION)) session_start();
	
	define('GLBAPPPORT', $_SERVER['SERVER_PORT']);
	
	require_once "func/zglobals.php";	
	require_once "func/zfvarias.php";
	require_once "func/zdatabase.php"; 
	require_once "func/sigma.php";
	require_once "val/valsession.php";

	$_SESSION[GLBAPPPORT.'USRCODBVDSIS'] 	= '';
	$_SESSION[GLBAPPPORT.'USRNOMBVDSIS'] 	= '';
	$_SESSION[GLBAPPPORT.'USRGRPBVDSIS'] 	= '';
	$_SESSION[GLBAPPPORT.'EMPCODBVDSIS'] 	= '';
	$_SESSION[GLBAPPPORT.'EMPNOMBVDSIS'] 	= '';
	$_SESSION[GLBAPPPORT.'IDIINTBVDSIS'] 	= '';
	$_SESSION[GLBAPPPORT.'MENU-NIV1']		= '';
	$_SESSION[GLBAPPPORT.'MENU-NIV2']		= '';
	$_SESSION[GLBAPPPORT.'MENU-NIV3']		= '';
	
	//----------------------------------------------------------------------------------------------
	//Valido KEY
	require_once "val/valkey.php";	
	
	
	if(ValKey() != 'BENVIDO10450420SISTEMAS'){		
		echo "Error de Registracion!";
		exit;
	}	
	//----------------------------------------------------------------------------------------------	
	
	$tmpl= new HTML_Template_Sigma();	
	
	$idicodigo	=	'';
	$pathview= GLBRutaVIEW;
	eval("\$pathview = \"$pathview\";");
	
	$tmpl->loadTemplateFile($pathview.'/login.html');
	
	$tmpl->setVariable('gblpathjs' , GLBRutaJS 	);  //Path JS	
	$tmpl->setVariable('gblpathsty', GLBRutaSTY ); 	//Estilo	
	$tmpl->setVariable('gblpathimg', GLBRutaIMG );  //Path Imagenes
	
	$tmpl->setVariable('user','');
	$tmpl->setVariable('pass',''); 
		
	$imgfondo = '';
	$colorfondo = '';
	
	if(isset($_GET['CL'])){
		$colorfondo = '#'.trim($_GET['CL']);
	}
	
	if(isset($_GET['FD'])){
		$imgfondo = trim($_GET['FD']);
		
		$tmpl->setVariable('urlfondo',"background:url('imges/$imgfondo.png') no-repeat  right top; background-color:$colorfondo; margin-right:20px;"); 
	}else{
		if($colorfondo != ''){			
			$tmpl->setVariable('urlfondo',"background-color: $colorfondo;"); 
		}else{
			$tmpl->setVariable('urlfondo',"background: url(imges/bg1.png) repeat;");
			//$tmpl->setVariable('urlfondo','background-color: #3d71b8;'); 
		}
	}
	
	$tmpl->setVariable('imgfondo',$imgfondo); 
	$tmpl->setVariable('colorfondo',$colorfondo); 
	
	if (isset($_POST['user'])){  // LOGUEO
		$err    = 0; 		
		$empres = ''; 
		$user   = $_POST['user']; //Usuario 
	    $pass   = $_POST['pass']; //Contraseña
		$imgfondo   	= $_POST['fondo']; //Fondo Imagen
		$colorfondo   	= $_POST['colorfondo']; //Fondo Imagen
       	$user   = strtoupper($user);
		    	     
		$conn   = sql_conectar();//Apertura de Conexion       
       	    	        
    	if(trim($user)==''){
    		$err=1; //sin usuario
    		$tmpl->setVariable('error','Wrong User'); 
    	}
		if(trim($pass)==''){
			$err=1; //sin pass
			$tmpl->setVariable('error','Wrong Password'); 
		}	
	
		
    	if($err==0){ //Si no hay error
    		$clvMD5 = trim(md5($pass.$user.'BVDSIS'));
    		//admin = f44d26db6e69623c41b727bcfa944ff0
    		
			$query = "SELECT ERRCOD,ERRMSG,USUCODIGO,USUGRPCOD,EMPCODDEF,EMPDESCRI,
							 IDICODIGO,IDICODINT,EMPCOLORFND
					  FROM VAL_LOGIN ('$user' , '$clvMD5') ";
			            
			$Table = sql_query($query,$conn);
			
			if ($Table->Rows_Count<0)
				$tmpl->setVariable('error','Error en Login'); 
			else{ //Todo OK
				$row = $Table->Rows[0];
				$ERRCOD = $row['ERRCOD']; //Codigo de Error
				$ERRMSG = $row['ERRMSG']; //Texto del Error
				
				if($ERRCOD<>0){ //Se Produjo Error
					$tmpl->setVariable('error',$ERRMSG);
				}else{
								
					$_SESSION[GLBAPPPORT.'USRCODBVDSIS'] 	= $row['USUCODIGO']; //Codigo de Usuario
					$_SESSION[GLBAPPPORT.'USRNOMBVDSIS'] 	= $user;             //Nombre de Usuario
					$_SESSION[GLBAPPPORT.'USRGRPBVDSIS'] 	= $row['USUGRPCOD']; //Grupo de Usuario
					$_SESSION[GLBAPPPORT.'EMPCODBVDSIS'] 	= $row['EMPCODDEF']; //Codigo de Empresa
					$_SESSION[GLBAPPPORT.'EMPNOMBVDSIS'] 	= $row['EMPDESCRI']; //Nombre de Empresa					
					$_SESSION[GLBAPPPORT.'IDIINTBVDSIS'] 	= $row['IDICODINT']; //Codigo de Idioma (interno)
					$_SESSION[GLBAPPPORT.'IDICODBVDSIS'] 	= strtolower($row['IDICODIGO']); //Codigo de Idioma
					$_SESSION[GLBAPPPORT.'FONDOBVDSIS'] 	= $imgfondo; //Fondo de pantalla
					$_SESSION[GLBAPPPORT.'EMPCOLORSN']		= 'N';
						
					if($colorfondo != ''){
						$_SESSION[GLBAPPPORT.'COLORFONDOBVDSIS'] 	= $colorfondo; //Color Fondo de pantalla						
					}else{
						$_SESSION[GLBAPPPORT.'COLORFONDOBVDSIS'] 	= '#'.$row['EMPCOLORFND']; //Color de Fondo
						$_SESSION[GLBAPPPORT.'EMPCOLORSN']			= 'S'; //Color de Empresa
					}
					
					$_SESSION[GLBAPPPORT.'COBCAJNRO'] = ''; //Nro de Caja, se inicializa la Variable de Session
					$_SESSION[GLBAPPPORT.'COBCAJDES'] = ''; //Descripcion de Caja, se inicializa la Variable de Session
					$_SESSION[GLBAPPPORT.'COBCIENRO'] = ''; //Nro de Cierre/Apertura
					$_SESSION[GLBAPPPORT.'COBCIEAPD'] = ''; //Fecha de Apertura de Caja
					$_SESSION[GLBAPPPORT.'COBCAJSUC'] = ''; //Sucursal de Caja 
					$_SESSION[GLBAPPPORT.'COBCAJCTA'] = ''; //Cuentas por Defecto
					
					$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];
					$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];
					$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];
					$idicodigo = strtoupper($_SESSION[GLBAPPPORT.'IDICODBVDSIS']);
					$idicodint = $_SESSION[GLBAPPPORT.'IDIINTBVDSIS'];
										
					//----------------------------------------------------------------------------------------------------------------------------
					//Accesso a Permisos de Sistema					
					$query="SELECT PRMCODIGO FROM ZZZ_USER_PERM WHERE USUCODIGO=$usucodigo ";
			  	   
			  	    $Table= sql_query($query,$conn);  
				  	for($i=0; $i < $Table->Rows_Count; $i++){
		    			$row	= $Table->Rows[$i];
		    			$parm	= trim($row['PRMCODIGO']);
				  		$_SESSION[GLBAPPPORT.'XXXPrm'][$parm] = 'S'; 
				  	}		
					//----------------------------------------------------------------------------------------------------------------------------					
					//Configuracion de Sistema por Ventanas
					$query="SELECT WINID,EMPCODIGO,WINCNFCOD,WINCNFEST FROM ZZZ_WIND_CONF";
			  	   
			  	    $Table= sql_query($query,$conn);  
				  	for($i=0; $i < $Table->Rows_Count; $i++){
		    			$row	= $Table->Rows[$i];
		    			
		    			$winid		= trim($row['WINID']);
		    			$wincnfcod	= trim($row['WINCNFCOD']);
		    			$wincnfest	= trim($row['WINCNFEST']);
						$empres	= trim($row['EMPCODIGO']);
		    			
				  		$_SESSION[GLBAPPPORT.'XXXWinConf'][$winid][$empres.$wincnfcod] = $wincnfest; 
				  	}		
					//----------------------------------------------------------------------------------------------------------------------------
					//----------------------------------------------------------------------------------------------------------------------------					
					//Configuracion de Sistema General
					$query="SELECT CFGPARAM,CFGVALUE FROM ZZZ_CONF";
			  	   
			  	    $Table= sql_query($query,$conn);  
				  	for($i=0; $i < $Table->Rows_Count; $i++){
		    			$row	= $Table->Rows[$i];
		    			
		    			$cfgparam	= trim($row['CFGPARAM']);
		    			$cfgvalue	= trim($row['CFGVALUE']);		    		
		    			
				  		$_SESSION[GLBAPPPORT.'XXXConf'][$cfgparam] = $cfgvalue; 
				  	}
					//----------------------------------------------------------------------------------------------------------------------------
					//----------------------------------------------------------------------------------------------------------------------------
				  	//Configuracion de Sistema General
					$query="SELECT WINID,USUWINDAT FROM USU_WIND WHERE EMPCODIGO=$empcodigo AND USUCODIGO=$usucodigo ";
			  	   
			  	    $Table= sql_query($query,$conn);  
				  	for($i=0; $i < $Table->Rows_Count; $i++){
		    			$row	= $Table->Rows[$i];
		    			
		    			$usuwinid	= trim($row['WINID']);
		    			$usuwindat	= trim($row['USUWINDAT']);		    		
		    			
				  		$_SESSION[GLBAPPPORT.'XXXWinUsu'][$usuwinid] = $usuwindat; 
				  	}
					//----------------------------------------------------------------------------------------------------------------------------					
					//Configuracion Diccionario de Datos
					$query="SELECT WINID,WINFLD,WINFLDNEW FROM ZZZ_WIND_DD WHERE IDICODINT=$idicodint";
			  	   
			  	    $Table= sql_query($query,$conn);  
				  	for($i=0; $i < $Table->Rows_Count; $i++){
		    			$row	= $Table->Rows[$i];
		    			
		    			$winid		= trim($row['WINID']);
		    			$winfld		= trim($row['WINFLD']);
		    			$winfldnew	= trim($row['WINFLDNEW']);
		    			
				  		$_SESSION[GLBAPPPORT.'XXXWinDD'][$winid][$winfld] = $winfldnew; 
				  	}		
				  	//----------------------------------------------------------------------------------------------------------------------------					
					//Busco la Caja por defecto que tenga el usuario
					$query="SELECT CU.COBCAJNRO,C.COBCAJDES 
							FROM COB_CAJA_USU CU
							LEFT OUTER JOIN COB_CAJA C ON C.EMPCODIGO=CU.EMPCODIGO AND C.COBCAJNRO=CU.COBCAJNRO
							WHERE CU.EMPCODIGO=$empcodigo AND CU.USUCODIGO=$usucodigo AND CU.CAJUSUDEF=1";
			  	    
			  	    $Table= sql_query($query,$conn);  
				  	if($Table->Rows_Count>0){
		    			$row	= $Table->Rows[0];		    			
		    			
		    			$_SESSION[GLBAPPPORT.'COBCAJNRO'] = trim($row['COBCAJNRO']);
						$_SESSION[GLBAPPPORT.'COBCAJDES'] = trim($row['COBCAJDES']);
				  	}							
					//----------------------------------------------------------------------------------------------------------------------------
					//----------------------------------------------------------------------------------------------------------------------------					
					MenuLoad($empcodigo,$usucodigo,$usugrpcod,$idicodint,$conn);
					//----------------------------------------------------------------------------------------------------------------------------
					//----------------------------------------------------------------------------------------------------------------------------
				  	//Si el Sistema no utiliza Caja, busco el unico cierre realizado
					if(!isset($_SESSION[GLBAPPPORT.'XXXConf'][$empcodigo.'CajaActiva'])){
						echo 'Falta Parametro: CajaActiva (Conf)';
						exit;
					}	
				  	if(intval($_SESSION[GLBAPPPORT.'XXXConf'][$empcodigo.'CajaActiva']) == 0 ){
				  		$cobcajnro = ($_SESSION[GLBAPPPORT.'COBCAJNRO']!='')? $_SESSION[GLBAPPPORT.'COBCAJNRO'] : 0;
						$query="SELECT FIRST 1 CI.COBCIENRO,CI.COBCIEAPD,CI.COBCAJNRO,C.COBCAJDES,C.COBCAJSUC,C.COBCAJCTA
								FROM COB_CIER CI
								LEFT OUTER JOIN COB_CAJA C ON CI.EMPCODIGO=C.EMPCODIGO AND CI.COBCAJNRO=C.COBCAJNRO 
								WHERE CI.EMPCODIGO=$empcodigo AND CI.COBCIECID IS NULL AND CI.COBCAJNRO=$cobcajnro
								ORDER BY CI.COBCIENRO DESC";
				  	   	
				  	    $Table= sql_query($query,$conn);  
					  	for($i=0; $i < $Table->Rows_Count; $i++){
			    			$row	= $Table->Rows[$i];
			    			
					  		$_SESSION[GLBAPPPORT.'COBCAJNRO'] = trim($row['COBCAJNRO']); //Nro de Caja, se inicializa la Variable de Session
							$_SESSION[GLBAPPPORT.'COBCAJDES'] = trim($row['COBCAJDES']); //Descripcion de Caja, se inicializa la Variable de Session
							$_SESSION[GLBAPPPORT.'COBCIENRO'] = trim($row['COBCIENRO']); //Nro de Cierre/Apertura
							$_SESSION[GLBAPPPORT.'COBCIEAPD'] = trim($row['COBCIEAPD']); //Fecha de Apertura de Caja
							$_SESSION[GLBAPPPORT.'COBCAJSUC'] = trim($row['COBCAJSUC']); //Sucursal de Caja
							$_SESSION[GLBAPPPORT.'COBCAJCTA'] = trim($row['COBCAJCTA']); //Cuentas por Defecto de Caja 
					  	}	
				  	}		
					//----------------------------------------------------------------------------------------------------------------------------
				  	IniSession();
				  	
				  	//Redirecciono a la pagina principal
					//header('Location: src/principal.php');						
					echo "<script>  window.location='view/esp/sistema.html'; </script>";
					exit;
				} // FIN - ELSE - if($ERRCOD<>0){ //Se Produjo Error           
			} // FIN - ELSE - if ($Table->Rows_Count<0)
		} //fin - if($err==0)
		sql_close($conn); //Cierro Conexion		
	   } 
	    	
	   unset($_POST);
	   //unset($_SESSION);
	   //session_unset();
   
	   $tmpl->show();

//******************************************************************************************************************************
function MenuLoad($empcodigo,$usucodigo,$usugrpcod,$idicodint,$conn){
	
    //Busco los Menu Principales, Nivel 1
    $query="SELECT DISTINCT MI.MNUPATH
			FROM ZZZ_USER_MENU MU
			LEFT OUTER JOIN ZZZ_MENU_IDIOM MI ON MI.MNUNRO=MU.MNUNRO
			LEFT OUTER JOIN ZZZ_MENU M ON M.MNUNRO=MU.MNUNRO
			WHERE MU.USUCODIGO=$usucodigo AND MI.IDICODINT=$idicodint AND M.WINID=0 AND M.MNUEST=1";
   
    $TMenu1= sql_query($query,$conn);   
    for($i_1=0; $i_1 < $TMenu1->Rows_Count; $i_1++){
    	$row1	= $TMenu1->Rows[$i_1];
		$menu1 	= trim($row1['MNUPATH']); 
		
    	
    	$_SESSION[GLBAPPPORT.'MENU-NIV1'][$menu1] = $menu1;
			
    	//Busco Segundo Nivel de Menu
    	$query="SELECT M.MNUNRO, MI.MNUTITLE,
						W.WINID, WI.WINTITLE, W.WINWIDTH, W.WINHEIGHT, W.WINMAX, W.WINMIN, W.WINMAXIM, W.WINFILE
				FROM ZZZ_USER_MENU MU
				LEFT OUTER JOIN ZZZ_MENU_IDIOM MI ON MI.MNUNRO=MU.MNUNRO
				LEFT OUTER JOIN ZZZ_MENU M ON M.MNUNRO=MU.MNUNRO 
				LEFT OUTER JOIN ZZZ_WIND W ON W.WINID=M.WINID
				LEFT OUTER JOIN ZZZ_WIND_IDIOM WI ON WI.WINID=W.WINID
				WHERE MU.USUCODIGO=$usucodigo AND MI.MNUPATH='$menu1' AND MI.IDICODINT=$idicodint AND  MI.MNUTITLE<>'$menu1' AND M.MNUEST=1
				ORDER BY M.MNUNRO, W.WINID";	 //AND WI.IDICODINT=$idicodint  AND M.WINID<>0
    	
    	$TMenu2= sql_query($query,$conn);   
    	for($i_2=0; $i_2 < $TMenu2->Rows_Count; $i_2++){
    		$row2		= $TMenu2->Rows[$i_2];
    		
    		$menu2 		= trim($row2['MNUTITLE']);    		
    		$path2 		= trim($row2['WINFILE']);    		
    		$id2 		= trim($row2['WINID']); 
    		$title2		= trim($row2['WINTITLE']);
    		$width2		= trim($row2['WINWIDTH']);
    		$height2	= trim($row2['WINHEIGHT']);
    		$winmax2	= trim($row2['WINMAX']);
    		$winmin2	= trim($row2['WINMIN']);
    		$winmaxim2	= trim($row2['WINMAXIM']);
    		$winfile2	= trim($row2['WINFILE']);
			
			/*if($winfile2 != ''){
				$ctabenvido = trim($_SESSION[GLBAPPPORT.'XXXConf']['CtaBenvidoCodigo']);
				$fileexiste	= 0;
				if(file_exists(GBLNameFldSistema.'/ctas/'.$ctabenvido.'/src/'.$winfile2)){
					$fileexiste	= 1;
				}
			}*/
			

    		if($width2	=='') $width2	= 100;
    		if($height2	=='') $height2	= 100;
    		
    		$_SESSION[GLBAPPPORT.'MENU-NIV2'] [$menu1][$menu2]['MENU'] = $menu2;
    		$_SESSION[GLBAPPPORT.'MENU-NIV2'] [$menu1][$menu2]['PATH'] = $path2;    		 		
    		$_SESSION[GLBAPPPORT.'MENU-NIV2'] [$menu1][$menu2]['ID'] = $id2;
			$_SESSION[GLBAPPPORT.'TITLES']	  [$id2]['TITLE'] 	= $title2." ($id2)";
			$_SESSION[GLBAPPPORT.'TITLES']	  [$id2]['WINID'] 	= $id2;
			$_SESSION[GLBAPPPORT.'TITLES']	  [$id2]['WIDTH'] 	= $width2;
			$_SESSION[GLBAPPPORT.'TITLES']	  [$id2]['HEIGHT']	= $height2;
			$_SESSION[GLBAPPPORT.'TITLES']	  [$id2]['MAX']	= $winmax2;
			$_SESSION[GLBAPPPORT.'TITLES']	  [$id2]['MIN']	= $winmin2;
			$_SESSION[GLBAPPPORT.'TITLES']	  [$id2]['MAXIM']	= $winmaxim2;
			$_SESSION[GLBAPPPORT.'TITLES']	  [$id2]['WINFILE']= $winfile2;
    		
    		if($path2 == ''){ //NO tiene ruta, es un submenu 
    			
    			$query="SELECT M.MNUNRO, MI.MNUTITLE,
								W.WINID, WI.WINTITLE, W.WINWIDTH, W.WINHEIGHT, W.WINMAX, W.WINMIN, W.WINMAXIM, W.WINFILE
						FROM ZZZ_USER_MENU MU
						LEFT OUTER JOIN ZZZ_MENU_IDIOM MI ON MI.MNUNRO=MU.MNUNRO
						LEFT OUTER JOIN ZZZ_MENU M ON M.MNUNRO=MU.MNUNRO 
						LEFT OUTER JOIN ZZZ_WIND W ON W.WINID=M.WINID
						LEFT OUTER JOIN ZZZ_WIND_IDIOM WI ON WI.WINID=W.WINID
						WHERE MU.USUCODIGO=$usucodigo AND MI.MNUPATH='$menu2' AND MI.IDICODINT=$idicodint  AND MI.MNUTITLE<>'$menu2' AND M.MNUEST=1
						ORDER BY M.MNUNRO, W.WINID";
					
		    	$TMenu3= sql_query($query,$conn);   
		    	for($i_3=0; $i_3 < $TMenu3->Rows_Count; $i_3++){
					$row3		= $TMenu3->Rows[$i_3];
					
		    		$menu3 		= trim($row3['MNUTITLE']);					
					$path3 		= trim($row3['WINFILE']);    		
					$id3 		= trim($row3['WINID']); 
					$title3		= trim($row3['WINTITLE']);
					$width3		= trim($row3['WINWIDTH']);
					$height3	= trim($row3['WINHEIGHT']);
					$winmax3	= trim($row3['WINMAX']);
					$winmin3	= trim($row3['WINMIN']);
					$winmaxim3	= trim($row3['WINMAXIM']);
					$winfile3	= trim($row3['WINFILE']);

		    		if($width3	=='') 	$width3		= 100;
    				if($height3	=='') 	$height3	= 100;
		    		
		    		$_SESSION[GLBAPPPORT.'MENU-NIV3'][$menu1][$menu2][$menu3]['MENU'] 	= $menu3;
					$_SESSION[GLBAPPPORT.'MENU-NIV3'][$menu1][$menu2][$menu3]['PATH'] 	= $path3;    		 		
					$_SESSION[GLBAPPPORT.'MENU-NIV3'][$menu1][$menu2][$menu3]['ID'] 	= $id3;
		    		$_SESSION[GLBAPPPORT.'TITLES'][$id3]['TITLE'] 	= $title3." ($id3)";
					$_SESSION[GLBAPPPORT.'TITLES'][$id3]['WINID'] 	= $id3;
					$_SESSION[GLBAPPPORT.'TITLES'][$id3]['WIDTH'] 	= $width3;
					$_SESSION[GLBAPPPORT.'TITLES'][$id3]['HEIGHT']	= $height3;
					$_SESSION[GLBAPPPORT.'TITLES'][$id3]['MAX']	= $winmax3;
					$_SESSION[GLBAPPPORT.'TITLES'][$id3]['MIN']	= $winmin3;
					$_SESSION[GLBAPPPORT.'TITLES'][$id3]['MAXIM']	= $winmaxim3;
					$_SESSION[GLBAPPPORT.'TITLES'][$id3]['WINFILE']= $winfile3;
				
				}						    		
		    		/*
		    		if($path3 == ''){ //NO tiene ruta, es un submenu		    			
		    			//Busco Cuarto Nivel de Menu	
				    	$query="SELECT M.MNUNRO, M.MNURUTA, I.MNUNOMBRE, I.MNUUBICA,M.MNUID,I.MNUTITLE,M.MNUWIDTH,M.MNUHEIGHT
							    FROM ZZZ_USER_MENU MU
			    				LEFT OUTER JOIN ZZZ_MENU_IDIOM I ON I.MNUNRO=MU.MNUNRO
							    LEFT OUTER JOIN ZZZ_MENU M ON M.MNUNRO=I.MNUNRO 
							    WHERE MU.USUCODIGO=$usucodigo AND I.MNUUBICA='$name3' AND I.IDICODINT=$idicodint
							    ORDER BY M.MNUNRO";
				    	$TMenu4= sql_query($query,$conn);   
				    	for($i_4=0; $i_4 < $TMenu4->Rows_Count; $i_4++){
				    		$row4	= $TMenu4->Rows[$i_4];
				    		$menu4 	= trim($row4['MNUUBICA']);
				    		$path4 	= trim($row4['MNURUTA']);
				    		$name4 	= trim($row4['MNUNOMBRE']);
				    		$title4	= trim($row4['MNUTITLE']);
				    		$width4	= trim($row4['MNUWIDTH']);
    						$height4= trim($row4['MNUHEIGHT']);
				    		$id4 	= trim($row4['MNUID']);  

				    		if($width4=='') 	$width4		=100;
    						if($height4=='') 	$height4	=100;
				    		
				    		$_SESSION[GLBAPPPORT.'MENU-NIV4'][$menu1][$menu2][$name3][$name4]['MENU'] 	= $menu4;
				    		$_SESSION[GLBAPPPORT.'MENU-NIV4'][$menu1][$menu2][$name3][$name4]['PATH'] 	= $path4;
				    		$_SESSION[GLBAPPPORT.'MENU-NIV4'][$menu1][$menu2][$name3][$name4]['NAME'] 	= $name4;
				    		$_SESSION[GLBAPPPORT.'MENU-NIV4'][$menu1][$menu2][$name3][$name4]['ID'] 	= $id4;
				    		$_SESSION[GLBAPPPORT.'TITLES'][$id4]['TITLE'] 	= $title4;
							$_SESSION[GLBAPPPORT.'TITLES'][$id4]['WINID'] 	= 'ID'.$id4;
							$_SESSION[GLBAPPPORT.'TITLES'][$id4]['WIDTH'] 	= $width4;
							$_SESSION[GLBAPPPORT.'TITLES'][$id4]['HEIGHT']	= $height4;		
				    		
		    				if($path4 == ''){ //NO tiene ruta, es un submenu		    					
				    			//Busco Quinto Nivel de Menu	
						    	$query="SELECT M.MNUNRO, M.MNURUTA, I.MNUNOMBRE, I.MNUUBICA,M.MNUID,I.MNUTITLE,M.MNUWIDTH,M.MNUHEIGHT
									    FROM ZZZ_USER_MENU MU
			    						LEFT OUTER JOIN ZZZ_MENU_IDIOM I ON I.MNUNRO=MU.MNUNRO
									    LEFT OUTER JOIN ZZZ_MENU M ON M.MNUNRO=I.MNUNRO 
									    WHERE MU.USUCODIGO=$usucodigo AND I.MNUUBICA='$name4' AND I.IDICODINT=$idicodint
									    ORDER BY M.MNUNRO";
						    	$TMenu5= sql_query($query,$conn);   
						    	for($i_5=0; $i_5 < $TMenu5->Rows_Count; $i_5++){
						    		$row5	= $TMenu4->Rows[$i_5];
						    		$menu5 	= trim($row5['MNUUBICA']);
						    		$path5 	= trim($row5['MNURUTA']);
						    		$name5 	= trim($row5['MNUNOMBRE']);
						    		$title5	= trim($row5['MNUTITLE']);
						    		$width5	= trim($row5['MNUWIDTH']);
    								$height5= trim($row5['MNUHEIGHT']);
						    		$id5 	= trim($row5['MNUID']);

						    		if($width5=='') 	$width5		=100;
    								if($height5=='') 	$height5	=100;
						    		
						    		$_SESSION[GLBAPPPORT.'MENU-NIV5'][$menu1][$name2][$name3][$name4][$name5]['MENU'] 	= $menu5;
						    		$_SESSION[GLBAPPPORT.'MENU-NIV5'][$menu1][$name2][$name3][$name4][$name5]['PATH'] 	= $path5;
						    		$_SESSION[GLBAPPPORT.'MENU-NIV5'][$menu1][$name2][$name3][$name4][$name5]['NAME'] 	= $name5;						    		
						    		$_SESSION[GLBAPPPORT.'MENU-NIV5'][$menu1][$name2][$name3][$name4][$name5]['ID'] 	= $id5;
						    		$_SESSION[GLBAPPPORT.'TITLES'][$id5]['TITLE'] 	= $title5;
									$_SESSION[GLBAPPPORT.'TITLES'][$id5]['WINID'] 	= 'ID'.$id5;
									$_SESSION[GLBAPPPORT.'TITLES'][$id5]['WIDTH'] 	= $width5;
									$_SESSION[GLBAPPPORT.'TITLES'][$id5]['HEIGHT']	= $height5;
						    	} //For 5 NIvel
		    				}//IF 4 Nivel
				    	}//For 4 NIvel
		    		}//IF 3 Nivel		    					    		
		    	}//For 3 NIvel
		    	*/
    		}//IF 2 Nivel
    	}//For 2 NIvel    	    	
    }//For 1 NIvel    
    
    
    //Tomo todo el resto de las ventanas que no tenegan menu
    $query =   "SELECT W.WINID, WI.WINTITLE, W.WINWIDTH, W.WINHEIGHT, W.WINMAX, W.WINMIN, W.WINMAXIM, W.WINFILE
				FROM ZZZ_WIND_IDIOM WI
				LEFT OUTER JOIN ZZZ_WIND W ON WI.WINID=W.WINID
				WHERE WI.IDICODINT=$idicodint AND NOT EXISTS(SELECT 1 FROM ZZZ_MENU M WHERE M.WINID=WI.WINID)
				ORDER BY W.WINID ";
	$TWin= sql_query($query,$conn);   
    for($i=0; $i < $TWin->Rows_Count; $i++){
    	$row	= $TWin->Rows[$i];
    	
    	$id 		= trim($row['WINID']); 
		$title		= trim($row['WINTITLE']);
		$width		= trim($row['WINWIDTH']);
		$height		= trim($row['WINHEIGHT']);
		$winmax		= trim($row['WINMAX']);
		$winmin		= trim($row['WINMIN']);
		$winmaxim	= trim($row['WINMAXIM']);
		$winfile	= trim($row['WINFILE']);
    	
		/*if($winfile != ''){
			$ctabenvido = trim($_SESSION[GLBAPPPORT.'XXXConf']['CtaBenvidoCodigo']);
			$fileexiste	= 0;
			if(file_exists('ctas/'.$ctabenvido.'/src/'.$winfile)){
				$winfile = '../ctas/'.$ctabenvido.'/src/'.$winfile;
				$fileexiste	= 1;
			}
			
		}*/		
		
    	$_SESSION[GLBAPPPORT.'TITLES'][$id]['TITLE'] 	= $title." ($id)";
		$_SESSION[GLBAPPPORT.'TITLES'][$id]['WINID'] 	= $id;
		$_SESSION[GLBAPPPORT.'TITLES'][$id]['WIDTH'] 	= $width;
		$_SESSION[GLBAPPPORT.'TITLES'][$id]['HEIGHT']	= $height;
		$_SESSION[GLBAPPPORT.'TITLES'][$id]['MAX']		= $winmax;
		$_SESSION[GLBAPPPORT.'TITLES'][$id]['MIN']		= $winmin;
		$_SESSION[GLBAPPPORT.'TITLES'][$id]['MAXIM']	= $winmaxim;
		$_SESSION[GLBAPPPORT.'TITLES'][$id]['WINFILE']	= $winfile;

    }			
	
	if(!isset($_SESSION[GLBAPPPORT.'XXXConf']['CtaBenvidoCodigo'])){
		echo 'Falta Parametro: CtaBenvidoCodigo (Conf)';
		exit;
	}
	
	$ctabenvido = trim($_SESSION[GLBAPPPORT.'XXXConf']['CtaBenvidoCodigo']);
	if($ctabenvido !='' && $ctabenvido != '0'){		
		$fp 	= fopen(GLBRutaFUNC.'/sistema.type',"r");
		$data 	= fread($fp,100);
		fclose($fp);
		
		if(trim($data) != $ctabenvido && trim($data) != ''){
			echo "Primero coloque la version Standar del Sistema";
			exit;
		}
		
		if(trim($data) == ''){
			$fp = fopen(GLBRutaFUNC.'/sistema.type',"w");
			fwrite($fp, $ctabenvido);
			fclose($fp);
			
			$path = 'ctas/'.$ctabenvido.'/';	
			runFiles($path,$path);
		}
	}
	
	
}

function runFiles($path,$fldctas){	
	$dir = opendir($path);   
    while ($elemento = readdir($dir)){		
        if( $elemento != "." && $elemento != ".."){
            // Si es una carpeta
            if(is_dir($path.$elemento)){                
                $pathdestino 	= str_replace($fldctas,'',$path);
                if(!file_exists($pathdestino.$elemento)){ //Si no existe la carpeta la creo					
					mkdir($pathdestino.$elemento);
				}

				runFiles($path.$elemento.'/',$fldctas);            
            } else {                
				$pathorigen 	= $path;
				$pathdestino 	= str_replace($fldctas,'',$path);
				
				if(!file_exists($pathdestino) and trim($pathdestino)!=''){ //Si no existe la carpeta la creo					
					mkdir($pathdestino);
				}
				
				if(file_exists($pathdestino.$elemento)){ //Si existe el archivo
					if(!file_exists($pathdestino.$elemento.'.STDBenvido')){ //Si existe el standar						
						rename($pathdestino.$elemento, $pathdestino.$elemento.'.STDBenvido'); //Genero un resguardo del original del standar benvido
						copy($pathorigen.$elemento, $pathdestino.$elemento); //Copio el del cliente
					}								
				}else{
					copy($pathorigen.$elemento, $pathdestino.$elemento); //Copio el del cliente
				}
							
				/*if(!file_exists(GLBRutaFUNC.'/actual.benvido')){
					$fp = fopen(GLBRutaFUNC.'/actual.benvido', 'w');
					fwrite($fp, date("F j, Y, g:i a"));					
					fclose($fp);
				}*/
            }
        }
    }
}
//******************************************************************************************************************************

?>
