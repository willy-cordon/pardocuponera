<?
	//--------------------------------------------------------------------------------------------------------------
	define('APPID','');

	require_once '../func/zglobals.php';
	require_once '../func/zdatabase.php';
	require_once '../func/zfvarias.php';
	require 	 '../func/Slim/Slim/Slim.php';
	
	\Slim\Slim::registerAutoloader();

	$app = new \Slim\Slim();
	
$app->get('/getpremio', function()  use ($app){		
    $response = array();	
    $response["ERROR"] 	 	= false;
	$response["MESSAGE"] 	= '';
	$response["EXCEPTION"] 	= '';
    $response["Premio"] = 0;	
    	
	/* PREMIOS -------------
	--1=Tablet LIHUE
	--2=Cajas de Herramientas ENAS
	--3=Almuerzo en el restaurant BUONO del Sheraton Hotel
	--4,5,6,7,8,9=Gracias por participar		
	*/	
		
	$horaPremio1 = '16:30:00'; // A partir de que hora puede salir
	$horaPremio2 = 900; // Frecuencia en tiempo que puede salir (segundos. Ej: cada 15 = 900)
	$horaPremio3 = '15:00:00'; // A partir de que hora puede salir
		
	try{
		$conn= sql_conectar();//Apertura de Conexion			
		
		$precodigo = rand(1,9); //Genero un numero al hazar de premio
		logerror("Premio: ".$precodigo);
		
		//------RESTRICCIONES DE HORARIOS ----------------------------------------
		//Si el premio es la tableta, el horario debe ser luego de las 16hs
		if($precodigo == 1){
			$query  = 'SELECT CASE WHEN CURRENT_TIME > CAST(\''.$horaPremio1.'\' AS TIME) THEN 1 ELSE 0 END AS HORAOK FROM RDB$DATABASE ';
			$Table 	= sql_query($query,$conn);
			$horaok	= $Table->Rows[0]['HORAOK'];
			logerror("Hora OK Premio 1:".$horaok);
			
			if($horaok == 0) $precodigo = rand(4,9); //NO PUEDE SALIR, genero un Gracias por participar
		}		
		
		//Si el premio es la Caja, no pueden salir dentro de los 20 minutos
		if($precodigo == 2){			
			$query  = 'SELECT 0 AS HORAOK FROM JUG_MAEST WHERE PRECODIGO=2 AND JUGHRA > CURRENT_TIME-'.$horaPremio2;
			$Table 	= sql_query($query,$conn);
			if($Table->Rows_Count > 0){
				$horaok	= 0;
			}else{
				$horaok	= 1;
			}		
			logerror("Hora OK Premio 2:".$horaok);
			
			if($horaok == 0) $precodigo = rand(4,9); //NO PUEDE SALIR, genero un Gracias por participar
		}
		
		//Si el premio es el Almuerzo, el horario debe ser luego de las 15hs
		if($precodigo == 3){
			$query  = 'SELECT CASE WHEN CURRENT_TIME > CAST(\''.$horaPremio3.'\' AS TIME) THEN 1 ELSE 0 END AS HORAOK FROM RDB$DATABASE';
			$Table 	= sql_query($query,$conn);
			$horaok	= $Table->Rows[0]['HORAOK'];
			logerror("Hora OK Premio 3:".$horaok);
			
			if($horaok == 0) $precodigo = rand(4,9); //NO PUEDE SALIR, genero un Gracias por participar
		}
		//------------------------------------------------------------------------
		
		//Controlo si el premio tiene stock
		$preok = 0;
		$query  = "SELECT 1 AS PREOK FROM PRE_MAEST WHERE PRECODIGO=$precodigo AND PRESTOCK>0 ";
		$Table 	= sql_query($query,$conn);
		if($Table->Rows_Count > 0){
			$preok 	= $Table->Rows[0]['PREOK'];
		}
		logerror("Premio STOCK OK: ".$preok);
		if($preok == 0) $precodigo = rand(4,9);  //NO PUEDE SALIR, genero un Gracias por participar
		
		
		//Si el premio 4,5,6,7,8,9 ==> 4 (SEGUI PARTICIPANDO)
		if($precodigo >= 4) $precodigo = 4;
		
		//Inserto el resultado del premio
		$query   = "INSERT INTO JUG_MAEST (JUGNRO,JUGFCH,JUGHRA,PRECODIGO)
						VALUES(GEN_ID(G_JUGNUMERO,1), CURRENT_DATE, CURRENT_TIME, $precodigo )";
		$err	= sql_execute($query,$conn);
		
		logerror("Premio FINAL: ".$precodigo);		
		$response["Premio"] = $precodigo;	
		logerror('------------------------------------------');
		
		sql_close($conn);
		echoRespnse(200, $response);
	}catch(Exception $e){
		$response["ERROR"] 	 	= true;
		$response["MESSAGE"] 	= "Error al obtener los premios ";
		$response["EXCEPTION"] 	=  $e->getMessage();
		
		echoRespnse(400, $response);
	}
});

$app->run();

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**
 * Devuelve la respuesta del API en forma de Json.
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
	//obtengo una instancia de la aplicacion
	$app = \Slim\Slim::getInstance();
	// Seteo el codigo HTTP
	$app->status($status_code);
	//Seteo la respuesta como Json
	$app->contentType('application/json');
	$responsestring = iconv("UTF-8","ISO-8859-1",json_encode($response));
	//codifico el array como json.
	echo json_encode($response);
}
		
function authenticate(\Slim\Route $route) {	
	try{
		// tomo los datos de la cabecera
		$headers = apache_request_headers();
		$response = array();
		$app = \Slim\Slim::getInstance();
				
		// Verifico Authorization del Header
		if (isset($headers['APPID'])) {
			// obtengo la Authorization 
			$appkeycod = $headers['APPID'];
			$appkeyref = APPID;
			
			if($appkeycod != $appkeyref){
				$response["error"] 	 = true;
				$response["mensaje"] = "Appkey incorrecta";
				echoRespnse(400, $response);
				$app->stop();
			}
			
		} else {
			// API KEY no viene en la cabecera
			$response["error"] 	 = true;
			$response["message"] = "No se encuentra autorizado para realizar la consulta.";
			echoRespnse(400, $response);
			$app->stop();
		}
	}catch(Exception $ex){
		$response["error"] 	 = true;
		$response["message"] = $ex->getMessage();
		$echoCode			 = 500;
		//log de errores al archivo.
		LogException($ex);
	}
}

function convertJsonObject($data) {
	$arrgral = explode("&",$data);
	$arr = null;
	foreach ($arrgral as $valgral){		
	   $arrint = explode("=",$valgral);
	   $valor = $arrint[1];
	   //$valor = str_replace('%40', '@', $arrint[1]);
	   $valor = urldecode($valor);
	   $arr[$arrint[0]] = $valor;
	}

	$json = json_encode($arr, JSON_FORCE_OBJECT);
	$json = json_decode($json);
	return $json;
}


function LogException(Exception $ex){
	$logerror = "ERROR DE SINCRONIZACION : ".$ex->getMessage()." \n Detalle de la excepcion: Codigo ==>".$ex->getCode()." \nLinea ==>".$ex->getLine()." \nPila de Llamadas ==>".$ex->getTraceAsString();
	logerror($logerror);
}

class BenvidoException extends Exception{}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
?>





