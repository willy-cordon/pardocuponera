<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";		
	require_once GLBRutaFUNC.'/c_estados.php';

	$pathview= GLBRutaVIEW;
	eval("\$pathview = \"$pathview\";");	
			
	$tmpl= new HTML_Template_Sigma();	
	$tmpl->loadTemplateFile($pathview.'/maestros/premios/mst.html');
	$tmpl->setVariable('gblpathjs' , GLBRutaJS 	);  //Path JS	
	$tmpl->setVariable('gblpathsty', GLBRutaSTY ); 	//Estilo	
	$tmpl->setVariable('gblpathimg', GLBRutaIMG );  //Path Imagenes
	//--------------------------------------------------------------------------------------------------------------
	//Datos de Ventana
	$winid		= 9402; //Ventana de Maestro
	$winidbrw	= 9401; //Ventana de Browser
	$wintitle	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['TITLE'];
	$winwidth	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WIDTH'];
	$winheight	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['HEIGHT'];
	$winmax		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAX'];
	$winmin		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MIN'];
	$winmaxim	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAXIM'];	
	$winfile	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WINFILE'];
	//--------------------------------------------------------------------------------------------------------------		
	$precod		= '';
	$predescri	= '';
	$preprecio	= '';
	$prenota	= '';
	$pretipo	= '';
	$preorden	= 1;
	$estcodigo	= 1;
	//--------------------------------------------------------------------------------------------------------------
	$precod	= isset($_GET['CI'])? $_GET['CI']: 'NEW';
	$winid		= $winid.$precod;
	$tmpl->setVariable('winid'		, $winid 		);
	$tmpl->setVariable('winidbrw'	, $winidbrw 	);
	//--------------------------------------------------------------------------------------------------------------
	$conn= sql_conectar();//Apertura de Conexion
	
	if($precod != 'NEW'){		
		$query	= "	SELECT PRECOD,PREDESCRI,PREPRECIO,PRENOTA,PREORDEN,PRETIPO,ESTCODIGO
					FROM PRE_MAEST 
					WHERE PRECOD = $precod ";
								   
		$Table	= sql_query($query,$conn);
		$row	= $Table->Rows[0];
			
		$precod 	= trim($row['PRECOD']);
		$predescri 	= trim($row['PREDESCRI']);
		$preprecio 	= trim($row['PREPRECIO']);
		$prenota 	= trim($row['PRENOTA']);
		$preorden 	= trim($row['PREORDEN']);
		$estcodigo 	= trim($row['ESTCODIGO']);
		$pretipo 	= trim($row['PRETIPO']);
		
		$preprecio	= number_format(floatval($preprecio) ,2 , ',', '.');
		
		$tmpl->setVariable('precod'		, $precod 		);
		$tmpl->setVariable('predescri'	, $predescri 	);
		$tmpl->setVariable('preprecio'	, $preprecio 	);
		$tmpl->setVariable('prenota'	, $prenota 		);
		$tmpl->setVariable('preorden'	, $preorden		);
		
		$tmpl->setVariable('pretipo_'.$pretipo	, 'selected'		);
	}
	
	CEstados('estado' ,$tmpl,$conn,$estcodigo);		//Estados
	
	//--------------------------------------------------------------------------------------------------------------
	sql_close($conn);
		
	$tmpl->show();
	
?>	
