<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";
	//--------------------------------------------------------------------------------------------------------------	
	$winid 		= 9402; //Id de Ventana Maestro
	$winidbrw 	= 9401; //Id de Ventana Browser
	//--------------------------------------------------------------------------------------------------------------		
	//Inicializo Variables
	$precod		= '';
	$predescri	= '';
	$preprecio	= '';
	$prenota	= '';
	$pretipo	= '';
	$preorden	= 1;
	$estcodigo	= 1;
	//--------------------------------------------------------------------------------------------------------------
	$errcod = 0;
	$err 	= 'SQLACCEPT';	

	$precod		= trim($_POST['precod']);
	$predescri	= trim($_POST['predescri']);
	$preprecio	= trim($_POST['preprecio']);
	$prenota	= trim($_POST['prenota']);
	$pretipo	= trim($_POST['pretipo']);
	$preorden	= trim($_POST['preorden']);
	$estcodigo	= trim($_POST['estcodigo']);
	//--------------------------------------------------------------------------------------------------------------	
	$conn	= sql_conectar();//Apertura de Conexion
	$trans	= sql_begin_trans($conn);
	
	$newreg = false;
	$precod = VarNullBD($precod ,'N');
	if($precod == 0){ //Nuevo			
		//- - - -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 		
		//Genero un ID 
		$query 		= 'SELECT GEN_ID(G_PREMIOS,1) AS ID FROM RDB$DATABASE';
		$TblId		= sql_query($query,$conn);
		$RowId		= $TblId->Rows[0];			
		$precod 	= trim($RowId['ID']);
		//- - - -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
		$newreg		= true;
	}
	
	$predescri	= VarNullBD($predescri	,'S');
	$preprecio	= VarNullBD($preprecio	,'N');
	$prenota	= VarNullBD($prenota	,'S');
	$pretipo	= VarNullBD($pretipo	,'N');
	$preorden	= VarNullBD($preorden	,'N');
	$estcodigo	= VarNullBD($estcodigo	,'N');
	
	if($preorden==0 || $preorden==''){
		$query 		= "SELECT MAX(PREORDEN)+1 AS ORDEN FROM PRE_MAEST WHERE PRETIPO=$pretipo";
		$TblOrd		= sql_query($query,$conn);
		$RowOrd		= $TblOrd->Rows[0];			
		$preorden 	= trim($RowOrd['ORDEN']);
		if($preorden=='') $preorden=1;
	}
	
	//--------------------------------------------------------------------------------------------------------------
	if($newreg){ //Nuevo
		$winid.='NEW';
				
		$query = "	INSERT INTO PRE_MAEST (PRECOD,PREDESCRI,PREPRECIO,PRENOTA,PREORDEN,PRETIPO,ESTCODIGO)
					VALUES($precod,$predescri,$preprecio,$prenota,$preorden,$pretipo,$estcodigo) ";
		$err   = sql_execute($query,$conn,$trans);	
	}else{ //Modificar
		$winid.=$precod;
		
		$query = "	UPDATE PRE_MAEST SET
					PREDESCRI=$predescri, PREPRECIO=$preprecio, PRENOTA=$prenota,
					PREORDEN=$preorden, PRETIPO=$pretipo, ESTCODIGO=$estcodigo
					WHERE PRECOD = $precod ";
		$err   = sql_execute($query,$conn,$trans);			
	}
	//--------------------------------------------------------------------------------------------------------------	
	if($err == 'SQLACCEPT'){
		sql_commit_trans($trans);		
		$errcod = 0;
		$errmsg = 'Guardado Correcto!';        
	}else{ 
		sql_rollback_trans($trans);		
		$errcod = 2;
		$errmsg = 'No se Guardo';
	}	

	//--------------------------------------------------------------------------------------------------------------	
	echo '{"errcod":"'.$errcod.'", "errmsg":"'.$errmsg.'"}';
	//--------------------------------------------------------------------------------------------------------------	
	    
	sql_close($conn);
	//--------------------------------------------------------------------------------------------------------------
	    
?>
