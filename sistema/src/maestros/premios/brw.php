<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";
		
	$pathview= GLBRutaVIEW;
	eval("\$pathview = \"$pathview\";");	
			
	$tmpl= new HTML_Template_Sigma();	
	$tmpl->loadTemplateFile($pathview.'/maestros/premios/brw.html');
	$tmpl->setVariable('gblpathjs' , GLBRutaJS 	);  //Path JS	
	$tmpl->setVariable('gblpathsty', GLBRutaSTY ); 	//Estilo	
	$tmpl->setVariable('gblpathimg', GLBRutaIMG );  //Path Imagenes
	//--------------------------------------------------------------------------------------------------------------
	//Venta Browser
	$winidbrw 	= 9401; 
	$tmpl->setVariable('winidbrw' ,	$winidbrw );
	
	//Datos de Ventana	
	$winid		= 9402; //Ventana de Maestro
	$wintitle	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['TITLE'];
	$winwidth	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WIDTH'];
	$winheight	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['HEIGHT'];
	$winmax		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAX'];
	$winmin		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MIN'];
	$winmaxim	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAXIM'];	
	$winfile	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WINFILE'];
	//--------------------------------------------------------------------------------------------------------------
	//Filtros de Busqueda
	$fltwhere 		= '';
	if(isset($_POST['fltbuscar'])){		
		$fltbuscar	= trim($_POST['fltbuscar']);
		if($fltbuscar != ''){
			$tmpl->setVariable('fltbuscar', $fltbuscar);
			$fltwhere 	= " AND (PREDESCRI CONTAINING '$fltbuscar' OR PRENOTA CONTAINING '$fltbuscar') ";
		}
	}
	
	//Estado
	if(isset($_POST['fltestcodigo'])){		
		$fltestcodigo	= trim($_POST['fltestcodigo']);		
		$tmpl->setVariable('fltestcodigo', $fltestcodigo);
	}
	
	//Tipo
	if(isset($_POST['fltpretipo'])){		
		$fltpretipo	= trim($_POST['fltpretipo']);		
		$tmpl->setVariable('fltpretipo', $fltpretipo);
	}
	
	//--------------------------------------------------------------------------------------------------------------
	$conn= sql_conectar();//Apertura de Conexion
		
	//PAGINACION -----------------------------------------------	
    $sorton = isset($_POST['sorton'])? $_POST["sorton"]: 'PREORDEN';
	$sortby = isset($_POST['sortby'])? $_POST["sortby"]: 'DESC';
	
	$inicio = 0;  
	if (isset($_POST['incre'])) $inicio = $_POST['incre'];
	if($inicio <= 0) $inicio = 0;
	$fin 	= $inicio + GLBFilas;
	$sort 	= "ORDER BY $sorton $sortby";	
	//----------------------------------------------------------		
	
	$conn= sql_conectar();//Apertura de Conexion
	
	$query   = "SELECT PRECOD,PREDESCRI,PREPRECIO,PRENOTA,ESTCODIGO,PREORDEN,PRETIPO
				FROM PRE_MAEST 
				WHERE ESTCODIGO=$fltestcodigo AND PRETIPO=$fltpretipo $fltwhere
				$sort";
	
	$query = PaginarSql($inicio,$query);	
	$Table = sql_query($query,$conn);
	
	//PAGINACION -----------------------------------------------
    $tmpl->setVariable('gblpathimgPag'	, GLBRutaIMG );  //Path Imagenes
    $tmpl->setVariable('pagfilas'		, GLBFilas); 
	$tmpl->setVariable('incre'			, $inicio);
    $tmpl->setVariable('sorton'			, $sorton);
	$tmpl->setVariable('sortby'			, $sortby);
	$tmpl->setVariable('cntrow'			, $Table->Rows_Count ); //Cantidad de Registros de Consulta
	//----------------------------------------------------------
		
	for($i=0; $i < $Table->Rows_Count; $i++){
		$row= $Table->Rows[$i];
				
		$precod 	= trim($row['PRECOD']);
		$predescri 	= trim($row['PREDESCRI']);
		$preprecio 	= trim($row['PREPRECIO']);
		$prenota 	= trim($row['PRENOTA']);
		$preorden 	= trim($row['PREORDEN']);
		$estcodigo 	= trim($row['ESTCODIGO']);
		$pretipo 	= trim($row['PRETIPO']);
		
		$preprecio	= number_format(floatval($preprecio) ,2 , ',', '.');
		
		$tmpl->setCurrentBlock('browser');				
		$tmpl->setVariable('precod'		, $precod 		);
		$tmpl->setVariable('predescri'	, $predescri 	);
		$tmpl->setVariable('preprecio'	, $preprecio 	);
		$tmpl->setVariable('prenota'	, $prenota 		);	
		$tmpl->setVariable('preorden'	, $preorden		);	
		
		if($pretipo==1){
			$tmpl->setVariable('pretipo'	, 'Terminal'	);
		}else{
			$tmpl->setVariable('pretipo'	, 'Abono'	);
		}
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		//Datos de Ventana		
		$tmpl->setVariable('winid'			,	$winid.$precod 				);
		$tmpl->setVariable('winfile'		,	$winfile.'?CI='.$precod		); 
		$tmpl->setVariable('wintitle'		,	$wintitle 						);
		$tmpl->setVariable('winwidth'		,	$winwidth 						);
		$tmpl->setVariable('winheight'		,	$winheight 						);
		$tmpl->setVariable('winmax'			,	$winmax	 						);
		$tmpl->setVariable('winmin'			,	$winmin	 						);
		$tmpl->setVariable('winmaxim'		,	$winmaxim 						);
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 		
		$tmpl->parseCurrentBlock();	
	}
	
	sql_close($conn);
	//--------------------------------------------------------------------------
	
	
	$tmpl->show();
?>
