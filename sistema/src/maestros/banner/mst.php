<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";		
	require_once GLBRutaFUNC.'/c_estados.php';

	$pathview= GLBRutaVIEW;
	eval("\$pathview = \"$pathview\";");	
			
	$tmpl= new HTML_Template_Sigma();	
	$tmpl->loadTemplateFile($pathview.'/maestros/banner/mst.html');
	$tmpl->setVariable('gblpathjs' , GLBRutaJS 	);  //Path JS	
	$tmpl->setVariable('gblpathsty', GLBRutaSTY ); 	//Estilo	
	$tmpl->setVariable('gblpathimg', GLBRutaIMG );  //Path Imagenes
	//--------------------------------------------------------------------------------------------------------------
	//Datos de Ventana
	$winid		= 9902; //Ventana de Maestro
	$winidbrw	= 9901; //Ventana de Browser
	$wintitle	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['TITLE'];
	$winwidth	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WIDTH'];
	$winheight	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['HEIGHT'];
	$winmax		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAX'];
	$winmin		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MIN'];
	$winmaxim	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAXIM'];	
	$winfile	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WINFILE'];
	//--------------------------------------------------------------------------------------------------------------		
	$banreg 	= '';
	$bandescri 	= '';
	$banimagen 	= '';
	$estcodigo 	= 1;
	$banorden 	= '';
	$banlink	= '';
	
	$pathimagenes 	= '../../../imges/data/banners/';
	//--------------------------------------------------------------------------------------------------------------
	$banreg	= isset($_GET['CI'])? $_GET['CI']: 'NEW';
	$winid		= $winid.$banreg;
	$tmpl->setVariable('winid'		, $winid 		);
	$tmpl->setVariable('winidbrw'	, $winidbrw 	);
	//--------------------------------------------------------------------------------------------------------------
	$conn= sql_conectar();//Apertura de Conexion
	
	if($banreg != 'NEW'){		
		$query	= "SELECT BANREG,BANDESCRI,BANIMAGEN,BANORDEN,ESTCODIGO,BANLINK
					FROM BAN_MAEST 
					WHERE BANREG = $banreg ";
								   
		$Table	= sql_query($query,$conn);
		$row	= $Table->Rows[0];
			
		$banreg 	= trim($row['BANREG']);
		$bandescri 	= trim($row['BANDESCRI']);
		$banimagen 	= trim($row['BANIMAGEN']);
		$estcodigo 	= trim($row['ESTCODIGO']);
		$banorden 	= trim($row['BANORDEN']);
		$banlink 	= trim($row['BANLINK']);
		
		$tmpl->setVariable('banreg'		, $banreg 		);
		$tmpl->setVariable('bandescri'	, $bandescri 	);
		$tmpl->setVariable('banimagen'	, $banimagen 	);
		$tmpl->setVariable('estcodigo'	, $estcodigo 	);
		$tmpl->setVariable('banorden'	, $banorden 	);
		$tmpl->setVariable('banlink'	, $banlink 		);
	}
	
	if($banreg != 'NEW'){	
		$tmpl->setVariable('banimagenurl', $pathimagenes.$banreg.'/'.$banimagen	);
	}

	CEstados('estado' ,$tmpl,$conn,$estcodigo);		//Estados
	
	//--------------------------------------------------------------------------------------------------------------
	sql_close($conn);
		
	$tmpl->show();
	
?>	
