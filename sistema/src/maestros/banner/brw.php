<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";
		
	$pathview= GLBRutaVIEW;
	eval("\$pathview = \"$pathview\";");	
			
	$tmpl= new HTML_Template_Sigma();	
	$tmpl->loadTemplateFile($pathview.'/maestros/banner/brw.html');
	$tmpl->setVariable('gblpathjs' , GLBRutaJS 	);  //Path JS	
	$tmpl->setVariable('gblpathsty', GLBRutaSTY ); 	//Estilo	
	$tmpl->setVariable('gblpathimg', GLBRutaIMG );  //Path Imagenes
	//--------------------------------------------------------------------------------------------------------------
	//Venta Browser
	$winidbrw 	= 9901; 
	$tmpl->setVariable('winidbrw' ,	$winidbrw );
	
	//Datos de Ventana	
	$winid		= 9902; //Ventana de Maestro
	$wintitle	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['TITLE'];
	$winwidth	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WIDTH'];
	$winheight	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['HEIGHT'];
	$winmax		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAX'];
	$winmin		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MIN'];
	$winmaxim	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAXIM'];	
	$winfile	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WINFILE'];
	//--------------------------------------------------------------------------------------------------------------
	//Filtros de Busqueda
	$pathimagenes 	= '../../../imges/data/banner/';
	$imgwhite		= 'imges/cancelar.png';
	
	$fltwhere 		= '';
	if(isset($_POST['fltbuscar'])){		
		$fltbuscar	= trim($_POST['fltbuscar']);
		if($fltbuscar != ''){
			$tmpl->setVariable('fltbuscar', $fltbuscar);
			$fltwhere 	= " AND (BANDESCRI CONTAINING '$fltbuscar') ";
		}
	}
	
	//Estado
	if(isset($_POST['fltestcodigo'])){		
		$fltestcodigo	= trim($_POST['fltestcodigo']);		
		$tmpl->setVariable('fltestcodigo', $fltestcodigo);
	}
	//--------------------------------------------------------------------------------------------------------------
	$conn= sql_conectar();//Apertura de Conexion
		
	//PAGINACION -----------------------------------------------	
    $sorton = isset($_POST['sorton'])? $_POST["sorton"]: 'BANORDEN';
	$sortby = isset($_POST['sortby'])? $_POST["sortby"]: 'DESC';
	
	$inicio = 0;  
	if (isset($_POST['incre'])) $inicio = $_POST['incre'];
	if($inicio <= 0) $inicio = 0;
	$fin 	= $inicio + GLBFilas;
	$sort 	= "ORDER BY $sorton $sortby";	
	//----------------------------------------------------------		
	
	$conn= sql_conectar();//Apertura de Conexion
	
	$query   = "SELECT BANREG,BANDESCRI,BANIMAGEN,BANORDEN,ESTCODIGO
				FROM BAN_MAEST
				WHERE ESTCODIGO=$fltestcodigo $fltwhere
				$sort";
	
	$query = PaginarSql($inicio,$query);	
	$Table = sql_query($query,$conn);
	logerror($query);
	//PAGINACION -----------------------------------------------
    $tmpl->setVariable('gblpathimgPag'	, GLBRutaIMG );  //Path Imagenes
    $tmpl->setVariable('pagfilas'		, GLBFilas); 
	$tmpl->setVariable('incre'			, $inicio);
    $tmpl->setVariable('sorton'			, $sorton);
	$tmpl->setVariable('sortby'			, $sortby);
	$tmpl->setVariable('cntrow'			, $Table->Rows_Count ); //Cantidad de Registros de Consulta
	//----------------------------------------------------------
		
	for($i=0; $i < $Table->Rows_Count; $i++){
		$row= $Table->Rows[$i];
				
		$banreg 	= trim($row['BANREG']);
		$bandescri 	= trim($row['BANDESCRI']);
		$banimagen 	= trim($row['BANIMAGEN']);
		$estcodigo 	= trim($row['ESTCODIGO']);
		$banorden 	= trim($row['BANORDEN']);
		
		$tmpl->setCurrentBlock('browser');				
		$tmpl->setVariable('banreg'		, $banreg 		);
		$tmpl->setVariable('bandescri'	, $bandescri 	);
		$tmpl->setVariable('banimagen'	, $banimagen 	);
		$tmpl->setVariable('estcodigo'	, $estcodigo 	);
		$tmpl->setVariable('banorden'	, $banorden 	);
	
		$tmpl->setVariable('banimagen'		, $pathimagenes.$banreg.'/'.$banimagen	);
		
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		//Datos de Ventana		
		$tmpl->setVariable('winid'			,	$winid.$banreg 				);
		$tmpl->setVariable('winfile'		,	$winfile.'?CI='.$banreg		); 
		$tmpl->setVariable('wintitle'		,	$wintitle 						);
		$tmpl->setVariable('winwidth'		,	$winwidth 						);
		$tmpl->setVariable('winheight'		,	$winheight 						);
		$tmpl->setVariable('winmax'			,	$winmax	 						);
		$tmpl->setVariable('winmin'			,	$winmin	 						);
		$tmpl->setVariable('winmaxim'		,	$winmaxim 						);
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 		
		
		$tmpl->parseCurrentBlock();	
	}
	
	sql_close($conn);
	//--------------------------------------------------------------------------
	
	
	$tmpl->show();
?>
