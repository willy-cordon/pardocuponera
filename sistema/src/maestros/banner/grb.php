<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";
	//--------------------------------------------------------------------------------------------------------------	
	$winid 		= 9902; //Id de Ventana Maestro
	$winidbrw 	= 9901; //Id de Ventana Browser
	$pathimagenes 	= '/imges/data/banners/'; //Carpeta de iamgenes locales
	if (!file_exists("../../..".$pathimagenes)) {
		mkdir("../../..".$pathimagenes);	   				
	}
	
	//Copia el archivo a la carpeta del "clientes" para la web
	$pathcopyclientes = "C:/AppWeb/proyectolg/frontend/assets/images/ban/";	//PRD 
	//$pathcopyclientes = '../../../../proyectolg/assets/images/ban/'; //DEV
			
	if($pathcopyclientes!=''){		
		if (!file_exists($pathcopyclientes)) {
			mkdir($pathcopyclientes);	   				
		}		
	}
			
	//--------------------------------------------------------------------------------------------------------------		
	//Inicializo Variables
	$banreg 	= '';
	$bandescri 	= '';
	$banimagen 	= '';
	$estcodigo 	= 1;
	$banorden 	= '';
	$banlink 	= '';
	//--------------------------------------------------------------------------------------------------------------
	$errcod = 0;
	$err 	= 'SQLACCEPT';	

	$banreg			= trim($_POST['banreg']);
	$banregant		= trim($_POST['banregant']);
	$bandescri		= trim($_POST['bandescri']);
	$estcodigo		= trim($_POST['estcodigo']);
	$banimagen		= trim($_POST['banimagen']);
	$banimagenant	= trim($_POST['banimagenant']);
	$banorden		= trim($_POST['banorden']);
	$banlink		= trim($_POST['banlink']);
	
	//--------------------------------------------------------------------------------------------------------------	
	if(isset($_FILES['file'])){		
		$ext 		= pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
		$file		= $_FILES['file'];				
		$digit 		= rand(10,100);
		$banimagen	= "C_".time().$digit.".".$ext;
	}else{
		$banimagen = basename($banimagenant);
	}
	//--------------------------------------------------------------------------------------------------------------	
	$conn	= sql_conectar();//Apertura de Conexion
	$trans	= sql_begin_trans($conn);
	
	$newreg = false;
	$banreg = VarNullBD($banreg ,'N');
	if($banreg == 0){ //Nuevo			
		//- - - -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 		
		//Genero un ID 
		$query 		= 'SELECT GEN_ID(G_BANNERS,1) AS ID FROM RDB$DATABASE';
		$TblId		= sql_query($query,$conn);
		$RowId		= $TblId->Rows[0];			
		$banreg 	= trim($RowId['ID']);
		//- - - -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
		$newreg		= true;
	}
	
	$bandescri = str_replace("'","''",$bandescri);
	
	$banregant		= VarNullBD($banregant	 	,'N');
	$bandescri		= VarNullBD($bandescri		,'S');
	$estcodigo		= VarNullBD($estcodigo		,'N');
	$banorden		= VarNullBD($banorden		,'N');
	$banlink		= VarNullBD($banlink		,'S');
	
	if($banorden==0 || $banorden==''){
		$query 		= "SELECT MAX(BANORDEN)+1 AS ORDEN FROM BAN_MAEST ";
		$TblOrd		= sql_query($query,$conn);
		$RowOrd		= $TblOrd->Rows[0];			
		$banorden 	= trim($RowOrd['ORDEN']);
		if($banorden=='') $banorden=1;
	}
		 	
	//--------------------------------------------------------------------------------------------------------------
	if($newreg){ //Nuevo
		$winid.='NEW';
				
		$query = "	INSERT INTO BAN_MAEST(BANREG,BANDESCRI,BANIMAGEN,BANORDEN,ESTCODIGO,BANLINK)
					VALUES($banreg,$bandescri,'$banimagen',$banorden,$estcodigo,$banlink)";
		$err   = sql_execute($query,$conn,$trans);	
	}else{ //Modificar
		$winid.=$banreg;
		
		$query = "	UPDATE BAN_MAEST SET
						BANIMAGEN 	= '$banimagen',BANDESCRI 	= $bandescri,
						ESTCODIGO 	= $estcodigo, BANORDEN = $banorden, BANLINK = $banlink
					WHERE BANREG = $banreg ";
		$err   = sql_execute($query,$conn,$trans);			
	}
	
	//--------------------------------------------------------------------------------------------------------------		
	if($banimagen != $banimagenant){
		if($errcod == 0 && $err == 'SQLACCEPT'){
			//Creo el directorio si no existe
			if (!file_exists("../../..".$pathimagenes.$banreg)) {
				mkdir("../../..".$pathimagenes.$banreg);	   				
			}
			
			//Elimino la foto actual si existe
			if(file_exists("../../..".$pathimagenes.$banreg.'/'.$banimagen)){
				unlink("../../..".$pathimagenes.$banreg.'/'.$banimagen);
			}
			
			move_uploaded_file( $file['tmp_name'], "../../..".$pathimagenes.$banreg.'/'.$banimagen);
			
			if($pathcopyclientes!=''){
				if (!file_exists($pathcopyclientes.$banreg.'/')) {
					mkdir($pathcopyclientes.$banreg.'/');	   				
				}
				copy( "../../..".$pathimagenes.$banreg.'/'.$banimagen, $pathcopyclientes.$banreg.'/'.$banimagen);			
			}
		}
	}
	//--------------------------------------------------------------------------------------------------------------	
	
	if($err == 'SQLACCEPT'){
		sql_commit_trans($trans);		
		$errcod = 0;
		$errmsg = 'Guardado Correcto!';        
	}else{ 
		sql_rollback_trans($trans);		
		$errcod = 2;
		$errmsg = 'No se Guardo';
	}	

	//--------------------------------------------------------------------------------------------------------------	
	echo '{"errcod":"'.$errcod.'", "errmsg":"'.$errmsg.'"}';
	//--------------------------------------------------------------------------------------------------------------	
	    
	sql_close($conn);
	//--------------------------------------------------------------------------------------------------------------
	    
?>
