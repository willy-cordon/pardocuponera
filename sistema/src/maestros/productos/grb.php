<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";
	//--------------------------------------------------------------------------------------------------------------	
	$winid 		= 9702; //Id de Ventana Maestro
	$winidbrw 	= 9701; //Id de Ventana Browser
	$pathimagenes 	= '/imges/data/productos/'; //Carpeta de iamgenes locales
	if (!file_exists("../../..".$pathimagenes)) {
		mkdir("../../..".$pathimagenes);	   				
	}
	
	//Copia el archivo a la carpeta del "clientes" para la web
	$pathcopyclientes = "C:/AppWeb/proyectolg/frontend/assets/images/pro/";	//PRD 
	//$pathcopyclientes = '../../../../proyectolg/assets/images/pro/'; //DEV
			
	if($pathcopyclientes!=''){		
		if (!file_exists($pathcopyclientes)) {
			mkdir($pathcopyclientes);	   				
		}		
	}
			
	//--------------------------------------------------------------------------------------------------------------		
	//Inicializo Variables
	$proreg 	= '';
	$protitulo	= '';
	$profchpub 	= '';
	$prodescri 	= '';
	$proimagen 	= '';
	$proimgico 	= '';
	$provideo 	= '';
	$estcodigo 	= '';
	$proorden 	= '';
	$prolink	= '';
	//--------------------------------------------------------------------------------------------------------------
	$errcod = 0;
	$err 	= 'SQLACCEPT';	

	$proreg 		= trim($_POST['proreg']);
	$protitulo 		= trim($_POST['protitulo']);
	$proregant 		= trim($_POST['proregant']);
	$prodescri 		= trim($_POST['prodescri']);
	$profchpub 		= trim($_POST['profchpub']);
	$estcodigo 		= trim($_POST['estcodigo']);
	$proimagen 		= trim($_POST['proimagen']);
	$proimagenant 	= trim($_POST['proimagenant']);
	$proimgico 		= trim($_POST['proimgico']);
	$proimgicoant 	= trim($_POST['proimgicoant']);
	$provideo 		= trim($_POST['provideo']);
	$provideoant 	= trim($_POST['provideoant']);
	$proorden 		= trim($_POST['proorden']);
	$prolink 		= trim($_POST['prolink']);
	
	
	//--------------------------------------------------------------------------------------------------------------	
	if(isset($_FILES['file'])){		
		$ext 		= pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
		$file		= $_FILES['file'];				
		$digit 		= rand(10,100);
		$proimagen	= "P_".time().$digit.".".$ext;
	}else{
		$proimagen = basename($proimagenant);
	}
	
	if(isset($_FILES['fileico'])){		
		$ext 		= pathinfo($_FILES['fileico']['name'], PATHINFO_EXTENSION);
		$fileico	= $_FILES['fileico'];				
		$digit 		= rand(10,100);
		$proimgico	= "PI_".time().$digit.".".$ext;
	}else{
		$proimgico = basename($proimgicoant);
	}
	
	if(isset($_FILES['filevideo'])){		
		$ext 		= pathinfo($_FILES['filevideo']['name'], PATHINFO_EXTENSION);
		$filevideo	= $_FILES['filevideo'];				
		$digit 		= rand(10,100);
		//$blovideo	= "V_".time().$digit.".".$ext;
		$provideo	= trim($_FILES['filevideo']['name']);
		
	}else{
		$provideo = basename($provideoant);
	}
	//--------------------------------------------------------------------------------------------------------------	
	$conn	= sql_conectar();//Apertura de Conexion
	$trans	= sql_begin_trans($conn);
	
	$newreg = false;
	$proreg = VarNullBD($proreg ,'N');
	if($proreg == 0){ //Nuevo			
		//- - - -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 		
		//Genero un ID 
		$query 		= 'SELECT GEN_ID(G_PRODUCTOS,1) AS ID FROM RDB$DATABASE';
		$TblId		= sql_query($query,$conn);
		$RowId		= $TblId->Rows[0];			
		$proreg 	= trim($RowId['ID']);
		//- - - -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
		$newreg		= true;
	}
	
	$prodescri = str_replace("'","''",$prodescri);
	
	$proregant		= VarNullBD($proregant	 	,'N');
	$protitulo		= VarNullBD($protitulo		,'S');
	$prodescri		= VarNullBD($prodescri		,'S');
	$profchpub		= VarNullBD($profchpub		,'S');
	$prolink		= VarNullBD($prolink		,'S');
	$estcodigo		= VarNullBD($estcodigo		,'N');
	$proorden		= VarNullBD($proorden		,'N');
	
	if($proorden==0 || $proorden==''){
		$query 		= "SELECT MAX(PROORDEN)+1 AS ORDEN FROM PRO_MAEST ";
		$TblOrd		= sql_query($query,$conn);
		$RowOrd		= $TblOrd->Rows[0];			
		$proorden 	= trim($RowOrd['ORDEN']);
		if($proorden=='') $proorden=1;
	}
		 	
	//--------------------------------------------------------------------------------------------------------------
	if($newreg){ //Nuevo
		$winid.='NEW';
				
		$query = "	INSERT INTO PRO_MAEST(PROREG,PROTITULO,PRODESCRI,PROFCHPUB,PROIMAGEN,PROIMGICO,PROVIDEO,PROORDEN,ESTCODIGO,PROLINK)
					VALUES($proreg,$protitulo,$prodescri,$profchpub,'$proimagen','$proimgico','$provideo',$proorden,$estcodigo,$prolink)";
		$err   = sql_execute($query,$conn,$trans);	
	}else{ //Modificar
		$winid.=$proreg;
		
		$query = "	UPDATE PRO_MAEST SET
						PROIMAGEN 	= '$proimagen', PROIMGICO='$proimgico', PROVIDEO='$provideo',
						PRODESCRI 	= $prodescri,PROFCHPUB 	= $profchpub, PROTITULO = $protitulo,
						ESTCODIGO 	= $estcodigo, PROORDEN = $proorden, PROLINK = $prolink
					WHERE PROREG = $proreg ";
		$err   = sql_execute($query,$conn,$trans);			
	}
	
	//--------------------------------------------------------------------------------------------------------------		
	if($proimagen != $proimagenant){
		if($errcod == 0 && $err == 'SQLACCEPT'){
			//Creo el directorio si no existe
			if (!file_exists("../../..".$pathimagenes.$proreg)) {
				mkdir("../../..".$pathimagenes.$proreg);	   				
			}
			
			//Elimino la foto actual si existe
			if(file_exists("../../..".$pathimagenes.$proreg.'/'.$proimagen)){
				unlink("../../..".$pathimagenes.$proreg.'/'.$proimagen);
			}
			
			move_uploaded_file( $file['tmp_name'], "../../..".$pathimagenes.$proreg.'/'.$proimagen);
			
			if($pathcopyclientes!=''){
				if (!file_exists($pathcopyclientes.$proreg.'/')) {
					mkdir($pathcopyclientes.$proreg.'/');	   				
				}
				copy( "../../..".$pathimagenes.$proreg.'/'.$proimagen, $pathcopyclientes.$proreg.'/'.$proimagen);			
			}
		}
	}
	//--------------------------------------------------------------------------------------------------------------		
	if($proimgico != $proimgicoant){
		if($errcod == 0 && $err == 'SQLACCEPT'){
			//Creo el directorio si no existe
			if (!file_exists("../../..".$pathimagenes.$proreg)) {
				mkdir("../../..".$pathimagenes.$proreg);	   				
			}
			
			//Elimino la foto actual si existe
			if(file_exists("../../..".$pathimagenes.$proreg.'/'.$proimgico)){
				unlink("../../..".$pathimagenes.$proreg.'/'.$proimgico);
			}
			
			move_uploaded_file( $fileico['tmp_name'], "../../..".$pathimagenes.$proreg.'/'.$proimgico);
			
			if($pathcopyclientes!=''){
				if (!file_exists($pathcopyclientes.$proreg.'/')) {
					mkdir($pathcopyclientes.$proreg.'/');	   				
				}
				copy( "../../..".$pathimagenes.$proreg.'/'.$proimgico, $pathcopyclientes.$proreg.'/'.$proimgico);			
			}
		}
	}
	//--------------------------------------------------------------------------------------------------------------	
	if($provideo != $provideoant){
		if($errcod == 0 && $err == 'SQLACCEPT'){
			//Creo el directorio si no existe
			if (!file_exists("../../..".$pathimagenes.$proreg)) {
				mkdir("../../..".$pathimagenes.$proreg);	   				
			}
			
			//Elimino la foto actual si existe
			if(file_exists("../../..".$pathimagenes.$proreg.'/'.$provideo)){
				unlink("../../..".$pathimagenes.$proreg.'/'.$provideo);
			}
			
			move_uploaded_file( $filevideo['tmp_name'], "../../..".$pathimagenes.$proreg.'/'.$provideo);
			
			if($pathcopyclientes!=''){
				if (!file_exists($pathcopyclientes.$proreg.'/')) {
					mkdir($pathcopyclientes.$proreg.'/');	   				
				}
				copy( "../../..".$pathimagenes.$proreg.'/'.$provideo, $pathcopyclientes.$proreg.'/'.$provideo);			
			}
		}
	}
	//--------------------------------------------------------------------------------------------------------------	
	
	if($err == 'SQLACCEPT'){
		sql_commit_trans($trans);		
		$errcod = 0;
		$errmsg = 'Guardado Correcto!';        
	}else{ 
		sql_rollback_trans($trans);		
		$errcod = 2;
		$errmsg = 'No se Guardo';
	}	

	//--------------------------------------------------------------------------------------------------------------	
	echo '{"errcod":"'.$errcod.'", "errmsg":"'.$errmsg.'"}';
	//--------------------------------------------------------------------------------------------------------------	
	    
	sql_close($conn);
	//--------------------------------------------------------------------------------------------------------------
	    
?>
