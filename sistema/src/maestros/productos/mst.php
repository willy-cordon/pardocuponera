<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";		
	require_once GLBRutaFUNC.'/c_estados.php';

	$pathview= GLBRutaVIEW;
	eval("\$pathview = \"$pathview\";");	
			
	$tmpl= new HTML_Template_Sigma();	
	$tmpl->loadTemplateFile($pathview.'/maestros/productos/mst.html');
	$tmpl->setVariable('gblpathjs' , GLBRutaJS 	);  //Path JS	
	$tmpl->setVariable('gblpathsty', GLBRutaSTY ); 	//Estilo	
	$tmpl->setVariable('gblpathimg', GLBRutaIMG );  //Path Imagenes
	//--------------------------------------------------------------------------------------------------------------
	//Datos de Ventana
	$winid		= 9702; //Ventana de Maestro
	$winidbrw	= 9701; //Ventana de Browser
	$wintitle	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['TITLE'];
	$winwidth	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WIDTH'];
	$winheight	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['HEIGHT'];
	$winmax		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAX'];
	$winmin		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MIN'];
	$winmaxim	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAXIM'];	
	$winfile	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WINFILE'];
	//--------------------------------------------------------------------------------------------------------------		
	$proreg 	= '';
	$protitulo 	= '';
	$profchpub 	= '';
	$prodescri 	= '';
	$proimagen 	= '';
	$proimgico 	= '';
	$provideo 	= '';
	$estcodigo 	= '';
	$proorden 	= '';
	
	$pathimagenes 	= '../../../imges/data/productos/';
	//--------------------------------------------------------------------------------------------------------------
	$proreg	= isset($_GET['CI'])? $_GET['CI']: 'NEW';
	$winid		= $winid.$proreg;
	$tmpl->setVariable('winid'		, $winid 		);
	$tmpl->setVariable('winidbrw'	, $winidbrw 	);
	//--------------------------------------------------------------------------------------------------------------
	$conn= sql_conectar();//Apertura de Conexion
	
	if($proreg != 'NEW'){		
		$query	= "SELECT PROREG,PROTITULO,PRODESCRI,PROFCHPUB,PROIMAGEN,PROIMGICO,
						PROVIDEO,PROORDEN,ESTCODIGO,PROLINK
					FROM PRO_MAEST 
					WHERE PROREG = $proreg ";
								   
		$Table	= sql_query($query,$conn);
		$row	= $Table->Rows[0];
			
		$proreg 	= trim($row['PROREG']);
		$protitulo 	= trim($row['PROTITULO']);
		$profchpub 	= trim($row['PROFCHPUB']);
		$prodescri 	= trim($row['PRODESCRI']);
		$proimagen 	= trim($row['PROIMAGEN']);
		$proimgico 	= trim($row['PROIMGICO']);
		$provideo 	= trim($row['PROVIDEO']);
		$estcodigo 	= trim($row['ESTCODIGO']);
		$proorden 	= trim($row['PROORDEN']);
		$prolink 	= trim($row['PROLINK']);
		
		$tmpl->setVariable('proreg'		, $proreg 		);
		$tmpl->setVariable('protitulo'	, $protitulo 	);
		$tmpl->setVariable('profchpub'	, $profchpub 	);
		$tmpl->setVariable('prodescri'	, $prodescri 	);
		$tmpl->setVariable('proimagen'	, $proimagen 	);
		$tmpl->setVariable('proimgico'	, $proimgico 	);
		$tmpl->setVariable('provideo'	, $provideo 	);
		$tmpl->setVariable('estcodigo'	, $estcodigo 	);
		$tmpl->setVariable('proorden'	, $proorden 	);
		$tmpl->setVariable('prolink'	, $prolink 		);
	}
	
	if($proreg != 'NEW'){	
		$tmpl->setVariable('proimgicourl', $pathimagenes.$proreg.'/'.$proimgico	);
		$tmpl->setVariable('proimagenurl', $pathimagenes.$proreg.'/'.$proimagen	);
	}

	CEstados('estado' ,$tmpl,$conn,$estcodigo);		//Estados
	
	//--------------------------------------------------------------------------------------------------------------
	sql_close($conn);
		
	$tmpl->show();
	
?>	
