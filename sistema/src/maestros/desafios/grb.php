<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";
	//--------------------------------------------------------------------------------------------------------------	
	$winid 		= 9502; //Id de Ventana Maestro
	$winidbrw 	= 9501; //Id de Ventana Browser
	//--------------------------------------------------------------------------------------------------------------		
	//Inicializo Variables
	$desreg		= '';
	$desfchpub	= '';
	$desdescri	= '';
	$destipo	= '';
	$deslink	= '';
	$desorden	= 1;
	$estcodigo	= 1;
	//--------------------------------------------------------------------------------------------------------------
	$errcod = 0;
	$err 	= 'SQLACCEPT';	

	$desreg		= trim($_POST['desreg']);
	$desdescri	= trim($_POST['desdescri']);
	$desfchpub	= trim($_POST['desfchpub']);
	$destipo	= trim($_POST['destipo']);
	$desorden	= trim($_POST['desorden']);
	$estcodigo	= trim($_POST['estcodigo']);
	$deslink	= trim($_POST['deslink']);
	//--------------------------------------------------------------------------------------------------------------	
	$conn	= sql_conectar();//Apertura de Conexion
	$trans	= sql_begin_trans($conn);
	
	$newreg = false;
	$desreg = VarNullBD($desreg ,'N');
	if($desreg == 0){ //Nuevo			
		//- - - -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 		
		//Genero un ID 
		$query 		= 'SELECT GEN_ID(G_DESAFIOS,1) AS ID FROM RDB$DATABASE';
		$TblId		= sql_query($query,$conn);
		$RowId		= $TblId->Rows[0];			
		$desreg 	= trim($RowId['ID']);
		//- - - -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
		$newreg		= true;
	}
	
	$desfchpub	= VarNullBD($desfchpub	,'S');
	$desdescri	= VarNullBD($desdescri	,'S');
	$deslink	= VarNullBD($deslink	,'S');
	$destipo	= VarNullBD($destipo	,'N');
	$desorden	= VarNullBD($desorden	,'N');
	$estcodigo	= VarNullBD($estcodigo	,'N');
	
	if($desorden==0 || $desorden==''){
		$query 		= "SELECT MAX(DESORDEN)+1 AS ORDEN FROM DES_MAEST WHERE DESTIPO=$destipo";
		$TblOrd		= sql_query($query,$conn);
		$RowOrd		= $TblOrd->Rows[0];			
		$desorden 	= trim($RowOrd['ORDEN']);
		if($desorden=='') $desorden=1;
	}
	
	//--------------------------------------------------------------------------------------------------------------
	if($newreg){ //Nuevo
		$winid.='NEW';
				
		$query = "	INSERT INTO DES_MAEST (DESREG,DESFCHPUB,DESDESCRI,DESTIPO,DESORDEN,ESTCODIGO,DESLINK)
					VALUES($desreg,$desfchpub,$desdescri,$destipo,$desorden,$estcodigo,$deslink) ";
		$err   = sql_execute($query,$conn,$trans);	
	}else{ //Modificar
		$winid.=$desreg;
		
		$query = "	UPDATE DES_MAEST SET
					DESDESCRI=$desdescri, DESFCHPUB=$desfchpub, DESLINK=$deslink,
					DESORDEN=$desorden, DESTIPO=$destipo, ESTCODIGO=$estcodigo
					WHERE DESREG = $desreg ";
		$err   = sql_execute($query,$conn,$trans);			
	}
	//--------------------------------------------------------------------------------------------------------------	
	if($err == 'SQLACCEPT'){
		sql_commit_trans($trans);		
		$errcod = 0;
		$errmsg = 'Guardado Correcto!';        
	}else{ 
		sql_rollback_trans($trans);		
		$errcod = 2;
		$errmsg = 'No se Guardo';
	}	

	//--------------------------------------------------------------------------------------------------------------	
	echo '{"errcod":"'.$errcod.'", "errmsg":"'.$errmsg.'"}';
	//--------------------------------------------------------------------------------------------------------------	
	    
	sql_close($conn);
	//--------------------------------------------------------------------------------------------------------------
	    
?>
