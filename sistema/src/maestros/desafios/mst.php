<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";		
	require_once GLBRutaFUNC.'/c_estados.php';

	$pathview= GLBRutaVIEW;
	eval("\$pathview = \"$pathview\";");	
			
	$tmpl= new HTML_Template_Sigma();	
	$tmpl->loadTemplateFile($pathview.'/maestros/desafios/mst.html');
	$tmpl->setVariable('gblpathjs' , GLBRutaJS 	);  //Path JS	
	$tmpl->setVariable('gblpathsty', GLBRutaSTY ); 	//Estilo	
	$tmpl->setVariable('gblpathimg', GLBRutaIMG );  //Path Imagenes
	//--------------------------------------------------------------------------------------------------------------
	//Datos de Ventana
	$winid		= 9502; //Ventana de Maestro
	$winidbrw	= 9501; //Ventana de Browser
	$wintitle	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['TITLE'];
	$winwidth	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WIDTH'];
	$winheight	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['HEIGHT'];
	$winmax		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAX'];
	$winmin		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MIN'];
	$winmaxim	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAXIM'];	
	$winfile	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WINFILE'];
	//--------------------------------------------------------------------------------------------------------------		
	$desreg		= '';
	$desfchpub	= '';
	$desdescri	= '';
	$destipo	= '';
	$desorden	= 1;
	$estcodigo	= 1;
	//--------------------------------------------------------------------------------------------------------------
	$desreg	= isset($_GET['CI'])? $_GET['CI']: 'NEW';
	$winid		= $winid.$desreg;
	$tmpl->setVariable('winid'		, $winid 		);
	$tmpl->setVariable('winidbrw'	, $winidbrw 	);
	//--------------------------------------------------------------------------------------------------------------
	$conn= sql_conectar();//Apertura de Conexion
	
	if($desreg != 'NEW'){		
		$query	= "	SELECT DESREG,DESFCHPUB,DESDESCRI,DESTIPO,DESORDEN,DESLINK,ESTCODIGO
					FROM DES_MAEST 
					WHERE DESREG = $desreg ";
								   
		$Table	= sql_query($query,$conn);
		$row	= $Table->Rows[0];
			
		$desreg 	= trim($row['DESREG']);
		$desfchpub 	= trim($row['DESFCHPUB']);
		$desdescri 	= trim($row['DESDESCRI']);
		$desorden 	= trim($row['DESORDEN']);
		$estcodigo 	= trim($row['ESTCODIGO']);
		$destipo 	= trim($row['DESTIPO']);
		$deslink 	= trim($row['DESLINK']);
		
		$tmpl->setVariable('desreg'		, $desreg 		);
		$tmpl->setVariable('desfchpub'	, $desfchpub 	);
		$tmpl->setVariable('desdescri'	, $desdescri 	);
		$tmpl->setVariable('desorden'	, $desorden 	);	
		$tmpl->setVariable('deslink'	, $deslink 		);
		
		$tmpl->setVariable('destipo_'.$destipo	, 'selected'		);
	}
	
	CEstados('estado' ,$tmpl,$conn,$estcodigo);		//Estados
	
	//--------------------------------------------------------------------------------------------------------------
	sql_close($conn);
		
	$tmpl->show();
	
?>	
