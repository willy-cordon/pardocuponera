<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
			
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	//--------------------------------------------------------------------------------------------------------------
	//--------------------------------------------------------------------------------------------------------------
	$errcod 	= 0;
	$errmsg		= '';
	$chareg 	= (isset($_POST['chareg']))? 	trim($_POST['chareg']) : '';
	$chatexto 	= (isset($_POST['chatexto']))? 	trim($_POST['chatexto']) : '';
	$chatipo	= (isset($_POST['chatipo']))? 	trim($_POST['chatipo']) : '';
	$chaultitm	= 0;
	if($chareg!='' && $chatexto!='' && $chatipo!=''){
		$conn= sql_conectar();//Apertura de Conexion
		
		//Marco al chat como "en proceso"
		$query = " 	UPDATE CHA_CABE SET CHAESTCOD=2 WHERE CHAREG=$chareg ";
		$err   = sql_execute($query,$conn);
		
		$queryDet = " SELECT MAX(CHAITM) AS CHAITM FROM CHA_DETA WHERE CHAREG=$chareg ";
		$TableDet = sql_query($queryDet,$conn);
		$chaitm=0;
		if($TableDet->Rows_Count>0){
			$rowDet= $TableDet->Rows[0];
			$chaitm = intval(trim($rowDet['CHAITM']));
		}
		$chaitm++;
		$chaultitm = $chaitm;
		
		//Creo la consulta para ser atendida
		$query = " 	INSERT INTO CHA_DETA(CHAREG,CHAITM,CHATEXTO,CHATIPO,CHAFCHREG,USUCODIGO)
					VALUES($chareg,$chaitm,'$chatexto',$chatipo,CURRENT_TIMESTAMP,$usucodigo) ";
		$err   = sql_execute($query,$conn);
		
		sql_close($conn);
	}
	echo '{"errcod":"'.$errcod.'","errmsg":"'.$errmsg.'","ultitm":"'.$chaultitm.'"}';	
?>	
