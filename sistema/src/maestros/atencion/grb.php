<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  	//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";
	//--------------------------------------------------------------------------------------------------------------	
	$winid 		= 9141; //Id de Ventana Maestro
	$winidbrw 	= 9140; //Id de Ventana Browser
	//--------------------------------------------------------------------------------------------------------------		
	//Inicializo Variables
	$acccod		= '';
	$accusunom	= '';
	$accusupas	= '';
	$estcodigo	= '';
	//--------------------------------------------------------------------------------------------------------------
	$errcod = 0;
	$err 	= 'SQLACCEPT';	

	$acccod			= trim($_POST['acccod']);
	$accusunom		= trim($_POST['accusunom']);
	$accusupas		= trim($_POST['accusupas']);
	$estcodigo		= trim($_POST['estcodigo']);
	//--------------------------------------------------------------------------------------------------------------	
	$conn	= sql_conectar();//Apertura de Conexion
	$trans	= sql_begin_trans($conn);
	
	$newreg = false;
	$acccod = VarNullBD($acccod ,'N');
	if($acccod == 0){ //Nuevo			
		//- - - -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 		
		//Genero un ID 
		$query 		= 'SELECT GEN_ID(G_ACCESOS,1) AS ID FROM RDB$DATABASE';
		$TblId		= sql_query($query,$conn);
		$RowId		= $TblId->Rows[0];			
		$acccod 	= trim($RowId['ID']);
		//- - - -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
		$newreg		= true;
	}
	
	$accusunom		= VarNullBD($accusunom	,'S');
	$accusupas		= VarNullBD($accusupas	,'S');
	$estcodigo		= VarNullBD($estcodigo	,'N');
		 	
	//--------------------------------------------------------------------------------------------------------------
	if($newreg){ //Nuevo
		$winid.='NEW';
				
		$query = "	INSERT INTO ACC_MAEST (ACCCOD,ACCUSUNOM,ACCUSUPAS,ESTCODIGO)
					VALUES($acccod,$accusunom,$accusupas,$estcodigo) ";
		$err   = sql_execute($query,$conn,$trans);	
	}else{ //Modificar
		$winid.=$acccod;
		
		$query = "	UPDATE ACC_MAEST SET
					ACCUSUNOM=$accusunom, ACCUSUPAS=$accusupas, ESTCODIGO=$estcodigo
					WHERE ACCCOD = $acccod ";
		$err   = sql_execute($query,$conn,$trans);			
	}
	//--------------------------------------------------------------------------------------------------------------	
	if($err == 'SQLACCEPT'){
		sql_commit_trans($trans);		
		$errcod = 0;
		$errmsg = 'Guardado Correcto!';        
	}else{ 
		sql_rollback_trans($trans);		
		$errcod = 2;
		$errmsg = 'No se Guardo';
	}	

	//--------------------------------------------------------------------------------------------------------------	
	echo '{"errcod":"'.$errcod.'", "errmsg":"'.$errmsg.'"}';
	//--------------------------------------------------------------------------------------------------------------	
	    
	sql_close($conn);
	//--------------------------------------------------------------------------------------------------------------
	    
?>
