<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
			
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
			
	//--------------------------------------------------------------------------------------------------------------
	//--------------------------------------------------------------------------------------------------------------
	$chareg 	= (isset($_POST['chareg']))? 	trim($_POST['chareg']) : '';
	$chaultitm 	= (isset($_POST['chaultitm']))? trim($_POST['chaultitm']) : '';
	$data = '[';
	if($chareg!='' && $chaultitm!=''){
		$conn= sql_conectar();//Apertura de Conexion
		
		$queryDet = " SELECT CHAITM,CHATEXTO,CHATIPO FROM CHA_DETA WHERE CHAREG=$chareg AND CHAITM>$chaultitm ORDER BY CHAITM ";
		$TableDet = sql_query($queryDet,$conn);
		for($i=0; $i < $TableDet->Rows_Count; $i++){
			$rowDet		= $TableDet->Rows[$i];
			$chaitm 	= trim($rowDet['CHAITM']);
			$chatexto 	= trim($rowDet['CHATEXTO']);
			$chatipo 	= trim($rowDet['CHATIPO']);
			$data .= '{"texto":"'.$chatexto.'","tipo":"'.$chatipo.'"},';
			$chaultitm = $chaitm;
		}
		
		if(trim(substr($data,strlen($data)-1,strlen($data)))==','){
			$data = substr($data,0,strlen($data)-1);
		}
		
		sql_close($conn);
	}
	$data .= ']';
	echo '{"data":'.$data.',"ultitm":"'.$chaultitm.'"}';		
	
?>	
