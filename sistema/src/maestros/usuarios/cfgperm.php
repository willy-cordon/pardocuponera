<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma
	$idicodint = $_SESSION[GLBAPPPORT.'IDIINTBVDSIS'];     	//Codigo de Idioma (Interno)
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";
		
	$pathview= GLBRutaVIEW;
	eval("\$pathview = \"$pathview\";");	
			
	$tmpl= new HTML_Template_Sigma();	
	$tmpl->loadTemplateFile($pathview.'/maestros/usuarios/cfgperm.html');
	$tmpl->setVariable('gblpathjs' , GLBRutaJS 	);  //Path JS	
	$tmpl->setVariable('gblpathsty', GLBRutaSTY ); 	//Estilo	
	$tmpl->setVariable('gblpathimg', GLBRutaIMG );  //Path Imagenes
	//--------------------------------------------------------------------------------------------------------------
	//Datos de Ventana
	$winid		= 9104; //Ventana de Maestro - Usuarios
	$wintitle	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['TITLE'];
	$winwidth	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WIDTH'];
	$winheight	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['HEIGHT'];
	$winmax		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAX'];
	$winmin		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MIN'];
	$winmaxim	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAXIM'];	
	$winfile	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WINFILE'];
	//--------------------------------------------------------------------------------------------------------------
	$usucod = trim($_GET['CI']);
	$winid		= $winid.$usucod;
	$tmpl->setVariable('winid'	, $winid 	);			
	//--------------------------------------------------------------------------------------------------------------		
	$conn= sql_conectar();//Apertura de Conexion	
		
	$query   = "SELECT P.PRMCODIGO, P.PRMDESCRI, 
					   CASE WHEN PU.USUCODIGO IS NOT NULL THEN '1' ELSE '0' END AS ESTADO 
				FROM ZZZ_PERM P				
				LEFT OUTER JOIN ZZZ_USER_PERM PU ON PU.PRMCODIGO=P.PRMCODIGO AND PU.USUCODIGO=$usucod				
				ORDER BY P.PRMDESCRI ";		
		
	$tmpl->setVariable('usucodigo'	, $usucod);
	
	$col = 1;
	$Table= sql_query($query,$conn);
	for($i=0; $i < $Table->Rows_Count; $i++){
		$row= $Table->Rows[$i];
		
		$prmcodigo	= trim($row['PRMCODIGO']); 	//Codigo Interno		
		$prmdescri	= trim($row['PRMDESCRI']);	//Nombre del Permiso
		$prmestado	= trim($row['ESTADO']);		//Estado del Permiso
		
		$prmestado_chk = '0';
				
		$tmpl->setCurrentBlock('permisos');

		$tmpl->setVariable('prmcodigo_'.$col	, $prmcodigo);
		$tmpl->setVariable('dt_permiso_'.$col	, $prmdescri );
		
		//Menu Habilitado
		if($prmestado == '1'){
			$prmestado_chk = 'checked';
		}
		$tmpl->setVariable('prmestado_'.$col		, $prmestado);
		$tmpl->setVariable('prmestado_chk_'.$col	, $prmestado_chk);
		
		if($col == 2)
			$tmpl->parseCurrentBlock();	
		
		if($col == 1) $col=2;
		else $col=1;	
	}
	//--------------------------------------------------------------------------------------------------------------			
	sql_close($conn);	
	//--------------------------------------------------------------------------------------------------------------		
	$tmpl->show();
	
?>	
