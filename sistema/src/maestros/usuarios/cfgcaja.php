<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma
	$idicodint = $_SESSION[GLBAPPPORT.'IDIINTBVDSIS'];     	//Codigo de Idioma (Interno)
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";
		
	$pathview= GLBRutaVIEW;
	eval("\$pathview = \"$pathview\";");	
			
	$tmpl= new HTML_Template_Sigma();	
	$tmpl->loadTemplateFile($pathview.'/maestros/usuarios/cfgcaja.html');
	$tmpl->setVariable('gblpathjs' , GLBRutaJS 	);  //Path JS	
	$tmpl->setVariable('gblpathsty', GLBRutaSTY ); 	//Estilo	
	$tmpl->setVariable('gblpathimg', GLBRutaIMG );  //Path Imagenes
	//--------------------------------------------------------------------------------------------------------------
	//Datos de Ventana
	$winid		= 9106; //Ventana de Maestro - Usuarios
	$wintitle	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['TITLE'];
	$winwidth	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WIDTH'];
	$winheight	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['HEIGHT'];
	$winmax		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAX'];
	$winmin		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MIN'];
	$winmaxim	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAXIM'];	
	$winfile	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WINFILE'];
	//--------------------------------------------------------------------------------------------------------------
	$usucod = trim($_GET['CI']);
	$winid		= $winid.$usucod;
	$tmpl->setVariable('winid'	, $winid 	);		
	//--------------------------------------------------------------------------------------------------------------	
	$conn= sql_conectar();//Apertura de Conexion	
	
	$query   = "SELECT C.COBCAJNRO,C.COBCAJDES,
					    COALESCE(CU.CAJUSUVER,0) AS CAJUSUVER,
						COALESCE(CU.CAJUSUUSA,0) AS CAJUSUUSA,
						COALESCE(CU.CAJUSUANU,0) AS CAJUSUANU,
						COALESCE(CU.CAJUSUDEF,0) AS CAJUSUDEF
				FROM COB_CAJA C
				LEFT OUTER JOIN COB_CAJA_USU CU ON CU.EMPCODIGO=C.EMPCODIGO AND CU.COBCAJNRO=C.COBCAJNRO  AND CU.USUCODIGO=$usucod
				WHERE C.EMPCODIGO=$empcodigo
				ORDER BY COBCAJNRO";		
	
	$tmpl->setVariable('usucodigo'	, $usucod);

	$Table= sql_query($query,$conn);
	for($i=0; $i < $Table->Rows_Count; $i++){
		$row= $Table->Rows[$i];
		
		$cobcajnro	= trim($row['COBCAJNRO']); 	//Codigo Interno
		$cobcajdes	= trim($row['COBCAJDES']); 	//Descripcion
		$cajusuver	= trim($row['CAJUSUVER']);
		$cajusuusa	= trim($row['CAJUSUUSA']);
		$cajusuanu	= trim($row['CAJUSUANU']);
		$cajusudef	= trim($row['CAJUSUDEF']);				
				
		$cajusuVer_chk = ($cajusuver == 1)? 'checked' : '';
		$cajusuUsa_chk = ($cajusuusa == 1)? 'checked' : '';
		$cajusuAnu_chk = ($cajusuanu == 1)? 'checked' : '';
		$cajusuDef_rad = ($cajusudef == 1)? 'checked' : '';

		if($cajusudef==1){

			$tmpl->setVariable('caja'		, $cobcajnro);
		}
												
		$tmpl->setCurrentBlock('cajas');

		$tmpl->setVariable('cobcajnro'		, $cobcajnro);
		$tmpl->setVariable('dt_cobcajdes'	, $cobcajdes);
		
		$tmpl->setVariable('cajusuver'	, $cajusuver);
		$tmpl->setVariable('cajusuusa'	, $cajusuusa);
		$tmpl->setVariable('cajusuanu'	, $cajusuanu);
		$tmpl->setVariable('cajusuver_chk'	, $cajusuVer_chk);
		$tmpl->setVariable('cajusuusa_chk'	, $cajusuUsa_chk);
		$tmpl->setVariable('cajusuanu_chk'	, $cajusuAnu_chk);
		$tmpl->setVariable('cajusudef_rad'	, $cajusuDef_rad);

				
		$tmpl->parseCurrentBlock();	
	}
	//--------------------------------------------------------------------------------------------------------------		
	sql_close($conn);	
	//--------------------------------------------------------------------------------------------------------------	
	$tmpl->show();
	
?>	
