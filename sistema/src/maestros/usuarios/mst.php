<?php include('../../../val/valuser.php'); ?>
<?	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	//$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";
		
	require_once GLBRutaFUNC.'/c_estados.php';
	require_once GLBRutaFUNC.'/c_cligrupos.php';
		
	require_once GLBRutaFUNC.'/c_estados.php';	
		
	$pathview= GLBRutaVIEW;
	eval("\$pathview = \"$pathview\";");	
			
	$tmpl= new HTML_Template_Sigma();	
	$tmpl->loadTemplateFile($pathview.'/maestros/usuarios/mst.html');
	$tmpl->setVariable('gblpathjs' , GLBRutaJS 	);  //Path JS	
	$tmpl->setVariable('gblpathsty', GLBRutaSTY ); 	//Estilo	
	$tmpl->setVariable('gblpathimg', GLBRutaIMG );  //Path Imagenes
	//--------------------------------------------------------------------------------------------------------------
	//Datos de Ventana
	$winid		= 9101; //Ventana de Maestro
	$wintitle	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['TITLE'];
	$winwidth	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WIDTH'];
	$winheight	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['HEIGHT'];
	$winmax		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAX'];
	$winmin		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MIN'];
	$winmaxim	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAXIM'];	
	$winfile	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WINFILE'];
	//--------------------------------------------------------------------------------------------------------------
	$regtable	= 100000;
	$usucodigo 	= 0; 	//Codigo Interno
	$usunomlog	= '';	//Nombre de Login	
	$usunombre	= '';	//Nombre
	$usuapelli	= '';	//Apellido
	$usuclvlog	= '';	//Clave de Logeo		
	$estcodigo	= 1;	//Estado
	//--------------------------------------------------------------------------------------------------------------
	$usucodigo	= isset($_GET['CI'])? $_GET['CI']: 'NEW';
	$winid		= $winid.$usucodigo;
	$tmpl->setVariable('winid'	, $winid 	);
	//--------------------------------------------------------------------------------------------------------------		
	$conn= sql_conectar();//Apertura de Conexion
	
	if($usucodigo != 'NEW'){			
		//Busco Datos
		$query	= " SELECT U.USUCODIGO,U.USUNOMLOG,U.USUCLVLOG,
							U.USUNOMBRE,U.USUAPELLI,U.USUESTADO,U.PSWMODPRE,
							V.VENCODIGO, V.VENDESCRI
					FROM USU_MAEST U
					LEFT OUTER JOIN VEN_MAEST V ON U.EMPCODDEF=V.EMPCODIGO AND U.USUVENCOD=V.VENCODIGO 					
					WHERE U.USUCODIGO=$usucodigo";
		                        
		$Table	= sql_query($query,$conn);
		$row	= $Table->Rows[0];
		
		$usucodigo 	= trim($row['USUCODIGO']);
		$usunomlog 	= trim($row['USUNOMLOG']);
		$usuclvlog 	= trim($row['USUCLVLOG']);
		$usunombre 	= trim($row['USUNOMBRE']);
		$usuapelli 	= trim($row['USUAPELLI']);			
		$estcodigo 	= trim($row['USUESTADO']);
		$pswmodpre 	= trim($row['PSWMODPRE']);
		$vencodigo 	= trim($row['VENCODIGO']);		
		$vendescri 	= trim($row['VENDESCRI']);
				
		//--------------------------------------------------------------------------	
		//Asigno los Valores a la Pantalla
		$tmpl->setVariable('usucodigo'		, $usucodigo 	);
		$tmpl->setVariable('usunomlog'		, $usunomlog	);
		$tmpl->setVariable('usuclvlog'		, $usuclvlog	);
		$tmpl->setVariable('usunombre'		, $usunombre	);
		$tmpl->setVariable('usuapelli'		, $usuapelli	);		
		$tmpl->setVariable('pswmodpre'		, $pswmodpre	);
		$tmpl->setVariable('vencodigo'		, $vencodigo	);
		$tmpl->setVariable('vendescri'		, $vendescri	);
	}
	
	CEstados('estado' ,$tmpl,$conn,$estcodigo);	//Estados
	//--------------------------------------------------------------------------
	
	sql_close($conn);
	
	//--------------------------------------------------------------------------
	$tmpl->show();
	
?>	
