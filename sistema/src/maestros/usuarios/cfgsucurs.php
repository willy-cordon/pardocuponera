<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma
	$idicodint = $_SESSION[GLBAPPPORT.'IDIINTBVDSIS'];     	//Codigo de Idioma (Interno)
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";
		
	$pathview= GLBRutaVIEW;
	eval("\$pathview = \"$pathview\";");	
			
	$tmpl= new HTML_Template_Sigma();	
	$tmpl->loadTemplateFile($pathview.'/maestros/usuarios/cfgsucurs.html');
	$tmpl->setVariable('gblpathjs' , GLBRutaJS 	);  //Path JS	
	$tmpl->setVariable('gblpathsty', GLBRutaSTY ); 	//Estilo	
	$tmpl->setVariable('gblpathimg', GLBRutaIMG );  //Path Imagenes
	//--------------------------------------------------------------------------------------------------------------
	//Datos de Ventana
	$winid		= 9103; //Ventana de Maestro - Usuarios
	$wintitle	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['TITLE'];
	$winwidth	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WIDTH'];
	$winheight	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['HEIGHT'];
	$winmax		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAX'];
	$winmin		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MIN'];
	$winmaxim	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAXIM'];	
	$winfile	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WINFILE'];
	//--------------------------------------------------------------------------------------------------------------
	$usucod = trim($_GET['CI']);
	$winid		= $winid.$usucod;
	$tmpl->setVariable('winid'	, $winid 	);		
	//--------------------------------------------------------------------------------------------------------------	
	$conn= sql_conectar();//Apertura de Conexion	
	
	$query   = "SELECT S.SUCCODIGO,S.SUCDESCRI,
					    COALESCE(SU.SUCUSUVER,0) AS SUCUSUVER,
						COALESCE(SU.SUCUSUUSA,0) AS SUCUSUUSA,
						COALESCE(SU.SUCUSUANU,0) AS SUCUSUANU
				FROM TBL_SUCU S
				LEFT OUTER JOIN TBL_SUCU_USU SU ON SU.EMPCODIGO=S.EMPCODIGO AND SU.SUCCODIGO=S.SUCCODIGO  AND SU.USUCODIGO=$usucod
				WHERE S.EMPCODIGO=$empcodigo
				ORDER BY SUCCODIGO";		
	
	$tmpl->setVariable('usucodigo'	, $usucod);

	$Table= sql_query($query,$conn);
	for($i=0; $i < $Table->Rows_Count; $i++){
		$row= $Table->Rows[$i];
		
		$succodigo	= trim($row['SUCCODIGO']); 	//Codigo Interno
		$sucdescri	= trim($row['SUCDESCRI']); 	//Descripcion
		$sucusuver	= trim($row['SUCUSUVER']);
		$sucusuusa	= trim($row['SUCUSUUSA']);
		$sucusuanu	= trim($row['SUCUSUANU']);				
				
		$sucusuVer_chk = ($sucusuver == 1)? 'checked' : '';
		$sucusuUsa_chk = ($sucusuusa == 1)? 'checked' : '';
		$sucusuAnu_chk = ($sucusuanu == 1)? 'checked' : '';
												
		$tmpl->setCurrentBlock('sucursales');

		$tmpl->setVariable('succodigo'		, $succodigo);
		$tmpl->setVariable('dt_sucursal'	, $sucdescri);
		
		$tmpl->setVariable('sucusuver'	, $sucusuver);
		$tmpl->setVariable('sucusuusa'	, $sucusuusa);
		$tmpl->setVariable('sucusuanu'	, $sucusuanu);
		$tmpl->setVariable('sucusuver_chk'	, $sucusuVer_chk);
		$tmpl->setVariable('sucusuusa_chk'	, $sucusuUsa_chk);
		$tmpl->setVariable('sucusuanu_chk'	, $sucusuAnu_chk);

				
		$tmpl->parseCurrentBlock();	
	}
	//--------------------------------------------------------------------------------------------------------------		
	sql_close($conn);	
	//--------------------------------------------------------------------------------------------------------------	
	$tmpl->show();
	
?>	
