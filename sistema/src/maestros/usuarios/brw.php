<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";
		
	$pathview= GLBRutaVIEW;
	eval("\$pathview = \"$pathview\";");	
			
	$tmpl= new HTML_Template_Sigma();	
	$tmpl->loadTemplateFile($pathview.'/maestros/usuarios/brw.html');
	$tmpl->setVariable('gblpathjs' , GLBRutaJS 	);  //Path JS	
	$tmpl->setVariable('gblpathsty', GLBRutaSTY ); 	//Estilo	
	$tmpl->setVariable('gblpathimg', GLBRutaIMG );  //Path Imagenes
	//--------------------------------------------------------------------------------------------------------------
	//Datos de Ventana
	$winid				= 9101; //Ventana de Maestro - Usuarios
	$wintitle			= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['TITLE'];
	$winwidth			= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WIDTH'];
	$winheight			= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['HEIGHT'];
	$winmax				= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAX'];
	$winmin				= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MIN'];
	$winmaxim			= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAXIM'];	
	$winfile			= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WINFILE'];
	
	$winid_menu			= 9102; //Ventana de Maestro - Menu
	$wintitle_menu		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_menu]['TITLE'];
	$winwidth_menu		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_menu]['WIDTH'];
	$winheight_menu		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_menu]['HEIGHT'];
	$winmax_menu		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_menu]['MAX'];
	$winmin_menu		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_menu]['MIN'];
	$winmaxim_menu		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_menu]['MAXIM'];	
	$winfile_menu		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_menu]['WINFILE'];
	
	$winid_sucurs		= 9103; //Ventana de Maestro - Sucursales
	$wintitle_sucurs	= $_SESSION[GLBAPPPORT.'TITLES'][$winid_sucurs]['TITLE'];
	$winwidth_sucurs	= $_SESSION[GLBAPPPORT.'TITLES'][$winid_sucurs]['WIDTH'];
	$winheight_sucurs	= $_SESSION[GLBAPPPORT.'TITLES'][$winid_sucurs]['HEIGHT'];
	$winmax_sucurs		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_sucurs]['MAX'];
	$winmin_sucurs		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_sucurs]['MIN'];
	$winmaxim_sucurs	= $_SESSION[GLBAPPPORT.'TITLES'][$winid_sucurs]['MAXIM'];	
	$winfile_sucurs		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_sucurs]['WINFILE'];
	
	$winid_perm			= 9104; //Ventana de Maestro - Permisos
	$wintitle_perm		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_perm]['TITLE'];
	$winwidth_perm		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_perm]['WIDTH'];
	$winheight_perm		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_perm]['HEIGHT'];
	$winmax_perm		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_perm]['MAX'];
	$winmin_perm		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_perm]['MIN'];
	$winmaxim_perm		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_perm]['MAXIM'];	
	$winfile_perm		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_perm]['WINFILE'];

	$winid_stock		= 9105; //Ventana de Maestro - Stock
	$wintitle_stock		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_stock]['TITLE'];
	$winwidth_stock		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_stock]['WIDTH'];
	$winheight_stock	= $_SESSION[GLBAPPPORT.'TITLES'][$winid_stock]['HEIGHT'];
	$winmax_stock		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_stock]['MAX'];
	$winmin_stock		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_stock]['MIN'];
	$winmaxim_stock		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_stock]['MAXIM'];	
	$winfile_stock		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_stock]['WINFILE'];	

	$winid_caja			= 9106; //Ventana de Maestro - Stock
	$wintitle_caja		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_caja]['TITLE'];
	$winwidth_caja		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_caja]['WIDTH'];
	$winheight_caja		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_caja]['HEIGHT'];
	$winmax_caja		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_caja]['MAX'];
	$winmin_caja		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_caja]['MIN'];
	$winmaxim_caja		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_caja]['MAXIM'];	
	$winfile_caja		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_caja]['WINFILE'];	

	$winid_empresa		= 9107; //Ventana de Maestro - Stock
	$wintitle_empresa	= $_SESSION[GLBAPPPORT.'TITLES'][$winid_empresa]['TITLE'];
	$winwidth_empresa	= $_SESSION[GLBAPPPORT.'TITLES'][$winid_empresa]['WIDTH'];
	$winheight_empresa	= $_SESSION[GLBAPPPORT.'TITLES'][$winid_empresa]['HEIGHT'];
	$winmax_empresa		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_empresa]['MAX'];
	$winmin_empresa		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_empresa]['MIN'];
	$winmaxim_empresa	= $_SESSION[GLBAPPPORT.'TITLES'][$winid_empresa]['MAXIM'];	
	$winfile_empresa	= $_SESSION[GLBAPPPORT.'TITLES'][$winid_empresa]['WINFILE'];

	$winid_deposito		= 9108; //Ventana de Maestro - Stock
	$wintitle_deposito	= $_SESSION[GLBAPPPORT.'TITLES'][$winid_deposito]['TITLE'];
	$winwidth_deposito	= $_SESSION[GLBAPPPORT.'TITLES'][$winid_deposito]['WIDTH'];
	$winheight_deposito	= $_SESSION[GLBAPPPORT.'TITLES'][$winid_deposito]['HEIGHT'];
	$winmax_deposito	= $_SESSION[GLBAPPPORT.'TITLES'][$winid_deposito]['MAX'];
	$winmin_deposito	= $_SESSION[GLBAPPPORT.'TITLES'][$winid_deposito]['MIN'];
	$winmaxim_deposito	= $_SESSION[GLBAPPPORT.'TITLES'][$winid_deposito]['MAXIM'];	
	$winfile_deposito	= $_SESSION[GLBAPPPORT.'TITLES'][$winid_deposito]['WINFILE'];


	$winid_cmpvta		= 9109; //Ventana de Maestro - Comprobantes de Ventas
	$wintitle_cmpvta	= $_SESSION[GLBAPPPORT.'TITLES'][$winid_cmpvta]['TITLE'];
	$winwidth_cmpvta	= $_SESSION[GLBAPPPORT.'TITLES'][$winid_cmpvta]['WIDTH'];
	$winheight_cmpvta	= $_SESSION[GLBAPPPORT.'TITLES'][$winid_cmpvta]['HEIGHT'];
	$winmax_cmpvta		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_cmpvta]['MAX'];
	$winmin_cmpvta		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_cmpvta]['MIN'];
	$winmaxim_cmpvta	= $_SESSION[GLBAPPPORT.'TITLES'][$winid_cmpvta]['MAXIM'];	
	$winfile_cmpvta		= $_SESSION[GLBAPPPORT.'TITLES'][$winid_cmpvta]['WINFILE'];	
	//--------------------------------------------------------------------------------------------------------------			
	//Filtros de Busqueda
	$fltbuscar = '';
	if(isset($_POST['fltbuscar'])){
		$fltbuscar	= trim($_POST['fltbuscar']);
		$tmpl->setVariable('fltbuscar', $fltbuscar);
		$fltbuscar 	= " WHERE (USUNOMLOG CONTAINING '$fltbuscar' OR USUNOMBRE CONTAINING '$fltbuscar' OR USUAPELLI CONTAINING '$fltbuscar') ";
	}
	//--------------------------------------------------------------------------------------------------------------	
	$conn= sql_conectar();//Apertura de Conexion
	
	//PAGINACION -----------------------------------------------	
    $sorton = isset($_POST['sorton'])? $_POST["sorton"]: 'USUNOMLOG';
	$sortby = isset($_POST['sortby'])? $_POST["sortby"]: 'DESC';
	
	$inicio = 0;  
	if (isset($_POST['incre'])) $inicio = $_POST['incre'];
	if($inicio <= 0) $inicio = 0;
	$fin 	= $inicio + GLBFilas;
	$sort 	= "ORDER BY $sorton $sortby";	
	//----------------------------------------------------------
	
	$query   = "SELECT USUCODIGO,USUNOMLOG, USUNOMBRE, USUAPELLI
				FROM USU_MAEST
				$fltbuscar
				$sort";
	
	$query = PaginarSql($inicio,$query);	
	$Table = sql_query($query,$conn);
	
	//PAGINACION -----------------------------------------------
    $tmpl->setVariable('gblpathimgPag'	, GLBRutaIMG );  //Path Imagenes
    $tmpl->setVariable('pagfilas'		, GLBFilas); 
	$tmpl->setVariable('incre'			, $inicio);
    $tmpl->setVariable('sorton'			, $sorton);
	$tmpl->setVariable('sortby'			, $sortby);
	$tmpl->setVariable('cntrow'			, $Table->Rows_Count ); //Cantidad de Registros de Consulta
	//----------------------------------------------------------
	
	for($i=0; $i < $Table->Rows_Count; $i++){
		$row= $Table->Rows[$i];
		
		$usucodigo	= trim($row['USUCODIGO']); 	//Codigo Interno
		$usunomlog	= trim($row['USUNOMLOG']); 	//Nombre de Login
		$usunombre	= trim($row['USUNOMBRE']);	//Nombre
		$usuapelli	= trim($row['USUAPELLI']);	//Apellido
						
				
		$tmpl->setCurrentBlock('browser');
		
		$tmpl->setVariable('usucodigo'		, $usucodigo);	
		$tmpl->setVariable('usunomlog'		, $usunomlog);
		$tmpl->setVariable('usunombre'		, $usunombre);
		$tmpl->setVariable('usuapelli'		, $usuapelli);
		
		$tmpl->setVariable('gblpathimgdet'	, GLBRutaIMG );  //Path Imagenes
		
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		//Datos de Ventana - Usuarios
		$tmpl->setVariable('winid'		,	$winid.$usucodigo 			);
		$tmpl->setVariable('winfile'	,	$winfile.'?CI='.$usucodigo 	); 
		$tmpl->setVariable('wintitle'	,	$wintitle 					);
		$tmpl->setVariable('winwidth'	,	$winwidth 					);
		$tmpl->setVariable('winheight'	,	$winheight 					);
		$tmpl->setVariable('winmax'		,	$winmax	 					);
		$tmpl->setVariable('winmin'		,	$winmin	 					);
		$tmpl->setVariable('winmaxim'	,	$winmaxim 					);
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		//Datos de Ventana - Menu
		$tmpl->setVariable('winid_menu'		,	$winid_menu.$usucodigo 				);
		$tmpl->setVariable('winfile_menu'	,	$winfile_menu.'?CI='.$usucodigo 	); 
		$tmpl->setVariable('wintitle_menu' 	,	$wintitle_menu 						);
		$tmpl->setVariable('winwidth_menu' 	,	$winwidth_menu 						);
		$tmpl->setVariable('winheight_menu'	,	$winheight_menu 					);
		$tmpl->setVariable('winmax_menu'	,	$winmax_menu	 					);
		$tmpl->setVariable('winmin_menu'	,	$winmin_menu	 					);
		$tmpl->setVariable('winmaxim_menu' 	,	$winmaxim_menu 						);
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		//Datos de Ventana - Permisos
		$tmpl->setVariable('winid_perm'		,	$winid_perm.$usucodigo 				);
		$tmpl->setVariable('winfile_perm'	,	$winfile_perm.'?CI='.$usucodigo 	); 
		$tmpl->setVariable('wintitle_perm' 	,	$wintitle_perm 						);
		$tmpl->setVariable('winwidth_perm' 	,	$winwidth_perm 						);
		$tmpl->setVariable('winheight_perm'	,	$winheight_perm 					);
		$tmpl->setVariable('winmax_perm'	,	$winmax_perm	 					);
		$tmpl->setVariable('winmin_perm'	,	$winmin_perm	 					);
		$tmpl->setVariable('winmaxim_perm' 	,	$winmaxim_perm 						);
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		//Datos de Ventana - Sucursales
		$tmpl->setVariable('winid_sucurs'		,	$winid_sucurs.$usucodigo 			);
		$tmpl->setVariable('winfile_sucurs'		,	$winfile_sucurs.'?CI='.$usucodigo 	); 
		$tmpl->setVariable('wintitle_sucurs' 	,	$wintitle_sucurs 					);
		$tmpl->setVariable('winwidth_sucurs' 	,	$winwidth_sucurs 					);
		$tmpl->setVariable('winheight_sucurs'	,	$winheight_sucurs 					);
		$tmpl->setVariable('winmax_sucurs'		,	$winmax_sucurs	 					);
		$tmpl->setVariable('winmin_sucurs'		,	$winmin_sucurs	 					);
		$tmpl->setVariable('winmaxim_sucurs' 	,	$winmaxim_sucurs 					);
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		//Datos de Ventana - Stock
		$tmpl->setVariable('winid_stock'		,	$winid_stock.$usucodigo 			);
		$tmpl->setVariable('winfile_stock'		,	$winfile_stock.'?CI='.$usucodigo 	); 
		$tmpl->setVariable('wintitle_stock' 	,	$wintitle_stock 					);
		$tmpl->setVariable('winwidth_stock' 	,	$winwidth_stock 					);
		$tmpl->setVariable('winheight_stock'	,	$winheight_stock 					);
		$tmpl->setVariable('winmax_stock'		,	$winmax_stock	 					);
		$tmpl->setVariable('winmin_stock'		,	$winmin_stock	 					);
		$tmpl->setVariable('winmaxim_stock' 	,	$winmaxim_stock 					);
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		//Datos de Ventana - Caja
		$tmpl->setVariable('winid_caja'		,	$winid_caja.$usucodigo 			);
		$tmpl->setVariable('winfile_caja'	,	$winfile_caja.'?CI='.$usucodigo ); 
		$tmpl->setVariable('wintitle_caja' 	,	$wintitle_caja 					);
		$tmpl->setVariable('winwidth_caja' 	,	$winwidth_caja 					);
		$tmpl->setVariable('winheight_caja'	,	$winheight_caja 				);
		$tmpl->setVariable('winmax_caja'	,	$winmax_caja					);
		$tmpl->setVariable('winmin_caja'	,	$winmin_caja					);
		$tmpl->setVariable('winmaxim_caja' 	,	$winmaxim_caja 					);
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		//Datos de Ventana - Empresa
		$tmpl->setVariable('winid_empresa'		,	$winid_empresa.$usucodigo 			);
		$tmpl->setVariable('winfile_empresa'	,	$winfile_empresa.'?CI='.$usucodigo 	); 
		$tmpl->setVariable('wintitle_empresa' 	,	$wintitle_empresa 					);
		$tmpl->setVariable('winwidth_empresa' 	,	$winwidth_empresa 					);
		$tmpl->setVariable('winheight_empresa'	,	$winheight_empresa 					);
		$tmpl->setVariable('winmax_empresa'		,	$winmax_empresa						);
		$tmpl->setVariable('winmin_empresa'		,	$winmin_empresa						);
		$tmpl->setVariable('winmaxim_empresa' 	,	$winmaxim_empresa 					);
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
			
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		//Datos de Ventana - Deposito
		$tmpl->setVariable('winid_deposito'		,	$winid_deposito.$usucodigo 			);
		$tmpl->setVariable('winfile_deposito'	,	$winfile_deposito.'?CI='.$usucodigo ); 
		$tmpl->setVariable('wintitle_deposito' 	,	$wintitle_deposito 					);
		$tmpl->setVariable('winwidth_deposito' 	,	$winwidth_deposito 					);
		$tmpl->setVariable('winheight_deposito'	,	$winheight_deposito 				);
		$tmpl->setVariable('winmax_deposito'	,	$winmax_deposito					);
		$tmpl->setVariable('winmin_deposito'	,	$winmin_deposito					);
		$tmpl->setVariable('winmaxim_deposito' 	,	$winmaxim_deposito 					);
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		//Datos de Ventana - Comprobantes de Venta
		$tmpl->setVariable('winid_cmpvta'		,	$winid_cmpvta.$usucodigo 			);
		$tmpl->setVariable('winfile_cmpvta'		,	$winfile_cmpvta.'?CI='.$usucodigo 	); 
		$tmpl->setVariable('wintitle_cmpvta' 	,	$wintitle_cmpvta 					);
		$tmpl->setVariable('winwidth_cmpvta' 	,	$winwidth_cmpvta 					);
		$tmpl->setVariable('winheight_cmpvta'	,	$winheight_cmpvta 					);
		$tmpl->setVariable('winmax_cmpvta'		,	$winmax_cmpvta	 					);
		$tmpl->setVariable('winmin_cmpvta'		,	$winmin_cmpvta	 					);
		$tmpl->setVariable('winmaxim_cmpvta' 	,	$winmaxim_cmpvta 					);
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		
		$tmpl->parseCurrentBlock();	
	}
	
	sql_close($conn);
	//--------------------------------------------------------------------------
	
	
	$tmpl->show();
?>
