<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma
	$idicodint = $_SESSION[GLBAPPPORT.'IDIINTBVDSIS'];     	//Codigo de Idioma (Interno)
	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";		
	//--------------------------------------------------------------------------------------------------------------	
	$errcod = 0;
	$errmsg = 'sin_msg';
	//--------------------------------------------------------------------------------------------------------------	
	$conn	= sql_conectar();//Apertura de Conexion
	$trans	= sql_begin_trans($conn);
	
	//Codigo de Usuario en cuestion
	$usucod = $_POST['usucodigo'];
	$cobcajnro = $_POST['cobcajnro'];
	$cajusudef = $_POST['cajusudef'];
	logerror($cajusudef);
	$tbldetalleJSON	= trim($_POST['tbldetalleJSON']);
	//--------------------------------------------------------------------------------------------------------------
	//Creo los Objetos JSON 
	$tbldetalleJSON	= str_replace('\\','',$tbldetalleJSON);
	$tbldetalle		= json_decode($tbldetalleJSON);
	//--------------------------------------------------------------------------------------------------------------
	//Elimino todo las Sucursales del usuario
	$query = "DELETE FROM COB_CAJA_USU WHERE EMPCODIGO=$empcodigo AND USUCODIGO=$usucod ";
	
	$err	= sql_execute($query,$conn,$trans);	
	
	foreach($tbldetalle->row as $index => $row){	
		//$row-index = 0 ; $row-content = Array[campo:valor]	
		$cobcajnro 	= trim($row->content->cobcajnro);
		$cajusuver 	= trim($row->content->cajusuver);
		$cajusuusa 	= trim($row->content->cajusuusa);
		$cajusuanu 	= trim($row->content->cajusuanu);
		
		if($errcod==0  && $err == 'SQLACCEPT'){
			$query 	= "INSERT INTO COB_CAJA_USU (EMPCODIGO,USUCODIGO,COBCAJNRO,CAJUSUVER,CAJUSUUSA,CAJUSUANU,CAJUSUDEF) 
						VALUES($empcodigo,$usucod,$cobcajnro,$cajusuver,$cajusuusa,$cajusuanu,0) ";	
			$err	= sql_execute($query,$conn,$trans);	
		}
		
	}

	$query = "	UPDATE COB_CAJA_USU SET
					CAJUSUDEF=1
					WHERE EMPCODIGO=$empcodigo AND USUCODIGO=$usucod AND COBCAJNRO='$cajusudef'	";
	$err   = sql_execute($query,$conn,$trans);
	
	//--------------------------------------------------------------------------------------------------------------
	if($err == 'SQLACCEPT' || $errcod==0){
		sql_commit_trans($trans);
		$errcod = 0;
		$errmsg = 'Cajas Configuradas';        
	}else{            
		sql_rollback_trans($trans);
		$errcod = 2;
		$errmsg = 'Cajas No Configuradas Correctamente';
	}	
	//--------------------------------------------------------------------------------------------------------------
	sql_close($conn);			
	//--------------------------------------------------------------------------------------------------------------
	echo "<respuesta>";                 
	   echo "<errcod> $errcod </errcod>";		               
	   echo "<msg> $errmsg </msg>";
	   echo "<screxe>"; 
	   echo "</screxe>"; 
	echo "</respuesta>";
?>
