<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma
	$idicodint = $_SESSION[GLBAPPPORT.'IDIINTBVDSIS'];     	//Codigo de Idioma (Interno)
	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";		
	//--------------------------------------------------------------------------------------------------------------	
	$errcod = 0;
	$errmsg = 'sin_msg';
	$err 	= '';
	//--------------------------------------------------------------------------------------------------------------	
	$conn	= sql_conectar();//Apertura de Conexion
	$trans	= sql_begin_trans($conn);
	
	//Codigo de Usuario en cuestion
	$usucod = $_POST['usucodigo'];	
	$stkdepcod = $_POST['stkdepcod'];
	$depusudef = $_POST['depusudef'];
	$tbldetalleJSON	= trim($_POST['tbldetalleJSON']);
	//--------------------------------------------------------------------------------------------------------------
	//Creo los Objetos JSON 
	$tbldetalleJSON	= str_replace('\\','',$tbldetalleJSON);
	$tbldetalle		= json_decode($tbldetalleJSON);
	//--------------------------------------------------------------------------------------------------------------
	if($depusudef == ''){
		$errcod = 2;
		$errmsg = 'Falta seleccionar el deposito por Defecto.';
	}
	//--------------------------------------------------------------------------------------------------------------

	if($errcod == 0){
		//Elimino todo las Sucursales del usuario
		$query = "DELETE FROM STK_DEPO_USU WHERE EMPCODIGO=$empcodigo AND USUCODIGO=$usucod ";
		
		$err	= sql_execute($query,$conn,$trans);	
		
		foreach($tbldetalle->row as $index => $row){	
			//$row-index = 0 ; $row-content = Array[campo:valor]	
			$stkdepcod 	= trim($row->content->stkdepcod);
			$depusuver 	= trim($row->content->depusuver);
			$depusuusa 	= trim($row->content->depusuusa);

			if($depusuver != 0 || $depusuusa != 0){
				if($errcod==0  && $err == 'SQLACCEPT'){
					$query 	= "INSERT INTO STK_DEPO_USU (EMPCODIGO,USUCODIGO,STKDEPCOD,DEPUSUVER,DEPUSUUSA,DEPUSUDEF) 
								VALUES($empcodigo, $usucod, '$stkdepcod',$depusuver,$depusuusa,0) ";
					$err	= sql_execute($query,$conn,$trans);
				}
			}
		}

		
		$query = "	UPDATE STK_DEPO_USU SET
					DEPUSUDEF=1
					WHERE EMPCODIGO=$empcodigo AND USUCODIGO=$usucod AND STKDEPCOD='$depusudef'	";
		$err   = sql_execute($query,$conn,$trans);
	}
	//--------------------------------------------------------------------------------------------------------------
	if($err == 'SQLACCEPT' || $errcod==0){
		sql_commit_trans($trans);
		$errcod = 0;
		$errmsg = 'Depositos Configurados';        
	}else{            
		sql_rollback_trans($trans);
		$errcod = 2;
		if($errmsg == 'sin_msg'){
			$errmsg = 'Depositos No Configurados Correctamente';
		}
	}	
	//--------------------------------------------------------------------------------------------------------------
	sql_close($conn);			
	//--------------------------------------------------------------------------------------------------------------
	echo "<respuesta>";                 
	   echo "<errcod> $errcod </errcod>";		               
	   echo "<msg> $errmsg </msg>";
	   echo "<screxe>"; 
	   echo "</screxe>"; 
	echo "</respuesta>";
?>
