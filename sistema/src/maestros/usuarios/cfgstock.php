<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma
	$idicodint = $_SESSION[GLBAPPPORT.'IDIINTBVDSIS'];     	//Codigo de Idioma (Interno)
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";
		
	$pathview= GLBRutaVIEW;
	eval("\$pathview = \"$pathview\";");	
			
	$tmpl= new HTML_Template_Sigma();	
	$tmpl->loadTemplateFile($pathview.'/maestros/usuarios/cfgstock.html');
	$tmpl->setVariable('gblpathjs' , GLBRutaJS 	);  //Path JS	
	$tmpl->setVariable('gblpathsty', GLBRutaSTY ); 	//Estilo	
	$tmpl->setVariable('gblpathimg', GLBRutaIMG );  //Path Imagenes
	//--------------------------------------------------------------------------------------------------------------
	//Datos de Ventana
	$winid		= 9105; //Ventana de Maestro - Usuarios
	$wintitle	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['TITLE'];
	$winwidth	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WIDTH'];
	$winheight	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['HEIGHT'];
	$winmax		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAX'];
	$winmin		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MIN'];
	$winmaxim	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAXIM'];	
	$winfile	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WINFILE'];
	//--------------------------------------------------------------------------------------------------------------
	$usucod = trim($_GET['CI']);
	$winid		= $winid.$usucod;
	$tmpl->setVariable('winid'	, $winid 	);			
	//--------------------------------------------------------------------------------------------------------------	
	$conn= sql_conectar();//Apertura de Conexion	
	
	$query   = "SELECT C.STKCMPCOD,C.STKCMPDES,
					    COALESCE(SU.STKCMPVER,0) AS STKCMPVER,
						COALESCE(SU.STKCMPUSA,0) AS STKCMPUSA,
						COALESCE(SU.STKCMPANU,0) AS STKCMPANU
				FROM STK_COMP C
				LEFT OUTER JOIN STK_COMP_USU SU ON SU.EMPCODIGO=C.EMPCODIGO AND SU.STKCMPCOD=C.STKCMPCOD  AND SU.USUCODIGO=$usucod
				WHERE C.EMPCODIGO=$empcodigo AND C.ESTCODIGO=1
				ORDER BY C.STKCMPCOD ";		
	
	$tmpl->setVariable('usucodigo'	, $usucod);
	
	$Table= sql_query($query,$conn);
	for($i=0; $i < $Table->Rows_Count; $i++){
		$row= $Table->Rows[$i];
		
		$stkcmpcod	= trim($row['STKCMPCOD']);
		$stkcmpdes	= trim($row['STKCMPDES']);
		$stkcmpver	= trim($row['STKCMPVER']);
		$stkcmpusa	= trim($row['STKCMPUSA']);
		$stkcmpanu	= trim($row['STKCMPANU']);
		
		$usuVer_chk = ($stkcmpver == 1)? 'checked' : '';
		$usuUsa_chk = ($stkcmpusa == 1)? 'checked' : '';
		$usuAnu_chk = ($stkcmpanu == 1)? 'checked' : '';
		
		$tmpl->setCurrentBlock('comprobantes');

		$tmpl->setVariable('stkcmpcod'	, $stkcmpcod);
		$tmpl->setVariable('stkcmpdes'	, $stkcmpdes);
		
		$tmpl->setVariable('stkcmpver'	, $stkcmpver);
		$tmpl->setVariable('stkcmpusa'	, $stkcmpusa);
		$tmpl->setVariable('stkcmpanu'	, $stkcmpanu);
		$tmpl->setVariable('stkcmpver_chk'	, $usuVer_chk);
		$tmpl->setVariable('stkcmpusa_chk'	, $usuUsa_chk);
		$tmpl->setVariable('stkcmpanu_chk'	, $usuAnu_chk);
				
		$tmpl->parseCurrentBlock();	
	}
	//--------------------------------------------------------------------------------------------------------------		
	sql_close($conn);	
	//--------------------------------------------------------------------------------------------------------------	
	$tmpl->show();
	
?>	
