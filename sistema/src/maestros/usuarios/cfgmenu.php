<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma
	$idicodint = $_SESSION[GLBAPPPORT.'IDIINTBVDSIS'];     	//Codigo de Idioma (Interno)
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";
		
	$pathview= GLBRutaVIEW;
	eval("\$pathview = \"$pathview\";");	
			
	$tmpl= new HTML_Template_Sigma();	
	$tmpl->loadTemplateFile($pathview.'/maestros/usuarios/cfgmenu.html');
	$tmpl->setVariable('gblpathjs' , GLBRutaJS 	);  //Path JS	
	$tmpl->setVariable('gblpathsty', GLBRutaSTY ); 	//Estilo	
	$tmpl->setVariable('gblpathimg', GLBRutaIMG );  //Path Imagenes
	//--------------------------------------------------------------------------------------------------------------
	//Datos de Ventana
	$winid		= 9102; //Ventana de Maestro - Usuarios
	$wintitle	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['TITLE'];
	$winwidth	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WIDTH'];
	$winheight	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['HEIGHT'];
	$winmax		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAX'];
	$winmin		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MIN'];
	$winmaxim	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAXIM'];	
	$winfile	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WINFILE'];
	//--------------------------------------------------------------------------------------------------------------
	$usucod = trim($_GET['CI']);
	$winid		= $winid.$usucod;
	$tmpl->setVariable('winid'	, $winid 	);			
	//--------------------------------------------------------------------------------------------------------------
	$conn= sql_conectar();//Apertura de Conexion	
		
	$query   = "SELECT M.MNUNRO, MI.MNUPATH, MI.MNUTITLE, 
					   CASE WHEN MU.USUCODIGO IS NOT NULL THEN '1' ELSE '0' END AS ESTADO 
				FROM ZZZ_MENU M
				LEFT OUTER JOIN ZZZ_MENU_IDIOM MI ON MI.MNUNRO=M.MNUNRO AND MI.IDICODINT=$idicodint
				LEFT OUTER JOIN ZZZ_USER_MENU MU ON MU.MNUNRO=M.MNUNRO AND MU.USUCODIGO=$usucod
				WHERE M.MNUEST=1							
				ORDER BY  M.MNUNRO,MI.MNUPATH, MI.MNUTITLE";		
	//--------------------------------------------------------------------------------------------------------------
	$tmpl->setVariable('usucodigo'	, $usucod);
	
	$col = 1;
	$Table= sql_query($query,$conn);
	for($i=0; $i < $Table->Rows_Count; $i++){
		$row= $Table->Rows[$i];
		
		$mnunro		= trim($row['MNUNRO']); 	//Codigo Interno
		$mnuubica	= trim($row['MNUPATH']); 	//Ubicacion
		$mnunombre	= trim($row['MNUTITLE']);	//Nombre de Menu
		$mnuestado	= trim($row['ESTADO']);		//Estado del Menu
		
		$mnuestado_chk = '0';
				
		$tmpl->setCurrentBlock('menu');

		$tmpl->setVariable('mnunro_'.$col	, $mnunro);
		$tmpl->setVariable('dt_menu_'.$col	, $mnuubica.' \ '.$mnunombre );
		
		//Menu Habilitado
		if($mnuestado == '1'){
			$mnuestado_chk = 'checked';
		}
		$tmpl->setVariable('mnuestado_'.$col		, $mnuestado);
		$tmpl->setVariable('mnuestado_chk_'.$col	, $mnuestado_chk);
		
		if($col == 3){
			$tmpl->parseCurrentBlock();
			$col=0;
		}	
		
		$col ++;	
	}	
	//--------------------------------------------------------------------------------------------------------------	
	sql_close($conn);	
	//--------------------------------------------------------------------------------------------------------------
	$tmpl->show();	
?>	
