<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma
	$idicodint = $_SESSION[GLBAPPPORT.'IDIINTBVDSIS'];     	//Codigo de Idioma (Interno)
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";
		
	$pathview= GLBRutaVIEW;
	eval("\$pathview = \"$pathview\";");	
			
	$tmpl= new HTML_Template_Sigma();	
	$tmpl->loadTemplateFile($pathview.'/maestros/usuarios/cfgcmpvta.html');
	$tmpl->setVariable('gblpathjs' , GLBRutaJS 	);  //Path JS	
	$tmpl->setVariable('gblpathsty', GLBRutaSTY ); 	//Estilo	
	$tmpl->setVariable('gblpathimg', GLBRutaIMG );  //Path Imagenes
	//--------------------------------------------------------------------------------------------------------------
	//Datos de Ventana
	$winid		= 9109; //Ventana de Maestro - Usuarios
	$wintitle	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['TITLE'];
	$winwidth	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WIDTH'];
	$winheight	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['HEIGHT'];
	$winmax		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAX'];
	$winmin		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MIN'];
	$winmaxim	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAXIM'];	
	$winfile	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WINFILE'];
	//--------------------------------------------------------------------------------------------------------------
	$usucod 	= trim($_GET['CI']);
	$winid		= $winid.$usucod;
	$tmpl->setVariable('winid'	, $winid 	);			
	//--------------------------------------------------------------------------------------------------------------	
	$conn= sql_conectar();//Apertura de Conexion	
	
	$query   = "SELECT C.TIPDOCCOD AS DOCTIPCOD, C.TIPDOCDES AS DOCTIPDES,
					    COALESCE(VU.DOCTIPVER,0) AS DOCTIPVER,
						COALESCE(VU.DOCTIPUSA,0) AS DOCTIPUSA,
						COALESCE(VU.DOCTIPANU,0) AS DOCTIPANU
				FROM TBL_COMP C
				LEFT OUTER JOIN VTA_COMP_USU VU ON VU.EMPCODIGO=C.EMPCODIGO AND VU.DOCTIPCOD=C.TIPDOCCOD  AND VU.USUCODIGO=$usucod
				WHERE C.EMPCODIGO=$empcodigo AND C.TIPDOCCLS = 'V'
				ORDER BY C.TIPDOCCOD ";		
	
	$tmpl->setVariable('usucodigo'	, $usucod);
	
	$Table= sql_query($query,$conn);
	for($i=0; $i < $Table->Rows_Count; $i++){
		$row= $Table->Rows[$i];
		
		$doctipcod	= trim($row['DOCTIPCOD']);
		$doctipdes	= trim($row['DOCTIPDES']);
		$doctipver	= trim($row['DOCTIPVER']);
		$doctipusa	= trim($row['DOCTIPUSA']);
		$doctipanu	= trim($row['DOCTIPANU']);
		$doctipverchk = ($doctipver == 1)? 'checked' : '';
		$doctipusachk = ($doctipusa == 1)? 'checked' : '';
		$doctipanuchk = ($doctipanu == 1)? 'checked' : '';
		
		$tmpl->setCurrentBlock('comprobantes');

		$tmpl->setVariable('doctipcod'		, $doctipcod);
		$tmpl->setVariable('doctipdes'		, $doctipdes);
		$tmpl->setVariable('doctipver'	    , $doctipver);
		$tmpl->setVariable('doctipusa'	    , $doctipusa);
		$tmpl->setVariable('doctipanu'	    , $doctipanu);
		$tmpl->setVariable('doctipverchk'	, $doctipverchk);
		$tmpl->setVariable('doctipusachk'	, $doctipusachk);
		$tmpl->setVariable('doctipanuchk'	, $doctipanuchk);
				
		$tmpl->parseCurrentBlock();	
	}
	//--------------------------------------------------------------------------------------------------------------		
	sql_close($conn);	
	//--------------------------------------------------------------------------------------------------------------	
	$tmpl->show();
	
?>	
