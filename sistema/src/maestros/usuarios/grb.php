<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";
	//--------------------------------------------------------------------------------------------------------------	
	$winid 		= 9101; //Id de Ventana Maestro
	$winidbrw 	= 9100; //Id de Ventana Browser
	//--------------------------------------------------------------------------------------------------------------	
	//Inicializo Variables
	$usucodigo	= 0;	//USUARIO - Codigo Interno
	$usunomlog	= '';	//USUARIO - Nombre de login
	$usuclvlog	= '';	//USUARIO - Clave
	$usunombre	= '';	//USUARIO - Nombre
	$usuapelli	= '';	//USUARIO - Apellido	
	$estcodigo 	= '';	//USUARIO - Estado	
	$pswmodpre	= '';	//USUARIO - Clave para Modificacion de Precios
	//--------------------------------------------------------------------------------------------------------------	
	$errcod 	= 0;
	$msgnro		= 0;
	$errmsg		= '';
	
	//Recorro Variables proveninetes de NEW/MST HTML
	foreach($_POST as $var => $dato){
		eval('$'.$var.' = "'.$dato.'"; '); // $itnrores - Varialbe de Ingreso				
	}	
	//--------------------------------------------------------------------------------------------------------------
	//USUARIO en mayuscula
	$usunomlog = strtoupper($usunomlog);
	
	//Genero el MD5 de Clave
	$clvMD5 		= trim(md5($usuclvlog.$usunomlog.'BVDSIS'));	
	//$pswmodpreMD5 	= trim(md5($pswmodpre.$usunomlog.'BVDSIS'));
	//--------------------------------------------------------------------------------------------------------------
	$usucodigo		= VarNullBD($usucodigo,'N');
	$usunomlog 		= VarNullBD($usunomlog,'S');
	$usunombre		= VarNullBD($usunombre,'S');
	$usuapelli 		= VarNullBD($usuapelli,'S');	
	$estcodigo 		= VarNullBD($estcodigo,'N');
	$pswmodpre 		= VarNullBD($pswmodpre,'S');
	$vencodigo 		= VarNullBD($vencodigo,'S');	
	//--------------------------------------------------------------------------------------------------------------	
	$conn= sql_conectar();//Apertura de Conexion
	if($usucodigo == 0){ //Nuevo
		$winid	.='NEW';
		$clvMD5	= VarNullBD($clvMD5,'S');
		
		$query = "INSERT INTO USU_MAEST (USUCODIGO,USUNOMLOG,USUCLVLOG,USUNOMBRE,USUAPELLI,USUESTADO,USUGRPCOD,EMPCODDEF,IDICODINT,PSWMODPRE, USUVENCOD) 
					VALUES(GEN_ID(G_USUARIOS,1),$usunomlog,$clvMD5,$usunombre,$usuapelli,$estcodigo,1,1,1,$pswmodpre,$vencodigo)";
				
	}else{ //Modificar
		$winid.=$usucodigo;
		
		$query = "SELECT USUCLVLOG FROM USU_MAEST WHERE USUCODIGO='$usucodigo'";
		
		$Table		= sql_query($query,$conn);
		$row		= $Table->Rows[0];
		$usuclvaux	= $row['USUCLVLOG'];
		$clvMD5		= ($usuclvaux == $usuclvlog)? $usuclvaux : $clvMD5;
		$clvMD5		= VarNullBD($clvMD5,'S');
		
		$query = "UPDATE USU_MAEST SET
					USUNOMLOG=$usunomlog, USUESTADO=$estcodigo,
					USUCLVLOG=$clvMD5, USUNOMBRE=$usunombre, USUAPELLI=$usuapelli, PSWMODPRE=$pswmodpre, USUVENCOD=$vencodigo
					WHERE USUCODIGO=$usucodigo";							  
	}
	
	$err   = sql_execute($query,$conn);
	
	if($err == 'SQLACCEPT'){		
		$msgnro = 10101; //Usuario Guardado
		eval('$errmsg = $msg_'.$msgnro.';'); //Mensajes de Procedimiento
		eval('$errmsg = "'.$errmsg.'";');
	}else{            
		$errcod = 2;
		$msgnro = 10102; //No se pudo Guardar el Usuario.
		eval('$errmsg = $msg_'.$msgnro.';'); //Mensajes de Procedimiento
		eval('$errmsg = "'.$errmsg.'";');		
	}
	sql_close($conn);

	//--------------------------------------------------------------------------------------------------------------	
	echo "<respuesta>";                 
	   echo "<errcod> $errcod </errcod>";		               
	   echo "<msg> $errmsg </msg>";
	   echo "<screxe> (RefreshBrw('$winid','$winidbrw')) </screxe>"; 
	echo "</respuesta>";	
	//--------------------------------------------------------------------------------------------------------------	
?>
