<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma
	$idicodint = $_SESSION[GLBAPPPORT.'IDIINTBVDSIS'];     	//Codigo de Idioma (Interno)
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";
		
	$pathview= GLBRutaVIEW;
	eval("\$pathview = \"$pathview\";");	
			
	$tmpl= new HTML_Template_Sigma();	
	$tmpl->loadTemplateFile($pathview.'/maestros/usuarios/cfgempresa.html');
	$tmpl->setVariable('gblpathjs' , GLBRutaJS 	);  //Path JS	
	$tmpl->setVariable('gblpathsty', GLBRutaSTY ); 	//Estilo	
	$tmpl->setVariable('gblpathimg', GLBRutaIMG );  //Path Imagenes
	//--------------------------------------------------------------------------------------------------------------
	//Datos de Ventana
	$winid		= 9107; //Ventana de Maestro - Usuarios
	$wintitle	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['TITLE'];
	$winwidth	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WIDTH'];
	$winheight	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['HEIGHT'];
	$winmax		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAX'];
	$winmin		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MIN'];
	$winmaxim	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAXIM'];	
	$winfile	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WINFILE'];
	//--------------------------------------------------------------------------------------------------------------
	$usucod = trim($_GET['CI']);
	$winid		= $winid.$usucod;
	$tmpl->setVariable('winid'	, $winid 	);			
	//--------------------------------------------------------------------------------------------------------------	
	$conn= sql_conectar();//Apertura de Conexion	
	
	$query   = "SELECT E.EMPCODIGO,E.EMPDESCRI,
					    COALESCE(EU.EMPUSUVER,0) AS EMPUSUVER,
						COALESCE(EU.EMPUSUUSA,0) AS EMPUSUUSA
				FROM EMP_MAEST E
				LEFT OUTER JOIN EMP_USU EU ON EU.EMPCODIGO=E.EMPCODIGO  AND EU.USUCODIGO=$usucod
				ORDER BY E.EMPCODIGO ";		
	
	$tmpl->setVariable('usucodigo'	, $usucod);
	
	$Table= sql_query($query,$conn);
	for($i=0; $i < $Table->Rows_Count; $i++){
		$row= $Table->Rows[$i];
		
		$empcodigo	= trim($row['EMPCODIGO']);
		$empdescri	= trim($row['EMPDESCRI']);
		$empusuver	= trim($row['EMPUSUVER']);
		$empusuusa	= trim($row['EMPUSUUSA']);
		
		$usuVer_chk = ($empusuver == 1)? 'checked' : '';
		$usuUsa_chk = ($empusuusa == 1)? 'checked' : '';
		
		$tmpl->setCurrentBlock('comprobantes');

		$tmpl->setVariable('empcodigo'	, $empcodigo);
		$tmpl->setVariable('empdescri'	, $empdescri);
		
		$tmpl->setVariable('empusuver'	, $empusuver);
		$tmpl->setVariable('empusuusa'	, $empusuusa);
		$tmpl->setVariable('empusuver_chk'	, $usuVer_chk);
		$tmpl->setVariable('empusuusa_chk'	, $usuUsa_chk);
				
		$tmpl->parseCurrentBlock();	
	}
	//--------------------------------------------------------------------------------------------------------------		
	sql_close($conn);	
	//--------------------------------------------------------------------------------------------------------------	
	$tmpl->show();
	
?>	
