<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";
			
	$pathview= GLBRutaVIEW;
	eval("\$pathview = \"$pathview\";");	
			
	$tmpl= new HTML_Template_Sigma();	
	$tmpl->loadTemplateFile($pathview.'/maestros/cupon/brw.html');
	$tmpl->setVariable('gblpathjs' , GLBRutaJS 	);  //Path JS	
	$tmpl->setVariable('gblpathsty', GLBRutaSTY ); 	//Estilo	
	$tmpl->setVariable('gblpathimg', GLBRutaIMG );  //Path Imagenes
	//--------------------------------------------------------------------------------------------------------------
	//Venta Browser
	$winidbrw 	= 9920;

	$tmpl->setVariable('winidbrw' ,	$winidbrw );
	
	//Datos de Ventana	
	$winid		= 9921; //Ventana de Maestro
	$wintitle	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['TITLE'];
	$winwidth	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WIDTH'];
	$winheight	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['HEIGHT'];
	$winmax		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAX'];
	$winmin		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MIN'];
	$winmaxim	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAXIM'];	
	$winfile	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WINFILE'];
	

	
	
	//--------------------------------------------------------------------------------------------------------------
	$errcod = 0;
	$err 	= 'SQLACCEPT';

	//Filtros de Busqueda
	$pathimagenes 	= '../../../imges/data/campania/';
	$imgwhite		= 'imges/cancelar.png';	
	
	$fltwhere 		= '';
	if(isset($_POST['fltbuscar'])){		
		$fltbuscar	= trim($_POST['fltbuscar']);
		if($fltbuscar != ''){
			$tmpl->setVariable('fltbuscar', $fltbuscar);
			$fltwhere 	= " AND (CAMDESCRI CONTAINING '$fltbuscar') ";
		}
	}
	//Estado
	if(isset($_POST['fltestcodigo'])){		
		$fltestcodigo	= trim($_POST['fltestcodigo']);		
		$tmpl->setVariable('fltestcodigo', $fltestcodigo);
	}
	
	//--------------------------------------------------------------------------------------------------------------
	//PAGINACION -----------------------------------------------	
    $sorton = isset($_POST['sorton'])? $_POST["sorton"]: 'CAMREG';
	$sortby = isset($_POST['sortby'])? $_POST["sortby"]: 'DESC';
	
	$inicio = 0;  
	if (isset($_POST['incre'])) $inicio = $_POST['incre'];
	if($inicio <= 0) $inicio = 0;
	$fin 	= $inicio + GLBFilas;
	$sort 	= "ORDER BY $sorton $sortby";	


	$conn= sql_conectar();//Apertura de Conexion
	//-----------------------------------Muestro todas las campañas activas------------------------------------------------------//
	$query   = "SELECT CD.CAMREG,CD.CAMITM,CD.CAMARTCOD,CD.CAMARTDES,CD.CAMARTDSC,CD.CAMARTIMP,CD.CAMIMGURL,CD.CAMIMAGEN, SUBSTRING(CC.CAMDESCRI FROM 1 FOR 40) AS CAMDESCRI
				FROM CAM_DETA CD
				LEFT OUTER JOIN CAM_CABE CC ON CC.CAMREG=CD.CAMREG
				WHERE CC.CAMREG = CD.CAMREG $fltwhere
				$sort ";
	
	$query = PaginarSql($inicio,$query);	
	$Table = sql_query($query,$conn);
	//PAGINACION -----------------------------------------------
    $tmpl->setVariable('gblpathimgPag'	,   GLBRutaIMG );  //Path Imagenes
    $tmpl->setVariable('pagfilas'		,   GLBFilas); 
	$tmpl->setVariable('incre'			,   $inicio);
    $tmpl->setVariable('sorton'			,   $sorton);
	$tmpl->setVariable('sortby'			,   $sortby);
	$tmpl->setVariable('cntrow'			,   $Table->Rows_Count ); //Cantidad de Registros de Consulta
	//----------------------------------------------------------
		
	for($i=0; $i < $Table->Rows_Count; $i++){
		$row= $Table->Rows[$i];
				
		$camreg 	= trim($row['CAMREG']);
		$camitm 	= trim($row['CAMITM']);
		
		$camartcod 	= trim($row['CAMARTCOD']);
		$camartdes 	= trim($row['CAMARTDES']);
		$camartdsc 	= trim($row['CAMARTDSC']);
		$camartimp 	= trim($row['CAMARTIMP']);
		$camimgurl 	= trim($row['CAMIMGURL']);
		$camimagen 	= trim($row['CAMIMAGEN']);
		$camdescri  = trim($row['CAMDESCRI']);


		$tmpl->setCurrentBlock('browser');				
		$tmpl->setVariable('camreg'		, $camreg );
		$tmpl->setVariable('camitm'		,  $camitm 	);
		$tmpl->setVariable('camartcod'	, $camartcod 	);
		$tmpl->setVariable('camartdes'	, $camartdes 	);
		$tmpl->setVariable('camartdsc'	, $camartdsc 	);
		$tmpl->setVariable('camartimp'	, $camartimp 	);
		$tmpl->setVariable('camimgurl'	, $camimgurl 	);
		$tmpl->setVariable('camdescri'	, $camdescri 	);

		$tmpl->setVariable('camimagen'		, $pathimagenes.$camreg.'/'.$camimagen	);
		


		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		//Datos de Ventana		
		$tmpl->setVariable('winid'			,	$winid.$camreg				);
		$tmpl->setVariable('winfile'		,	$winfile.'?CI='.$camitm	); 
		$tmpl->setVariable('wintitle'		,	$wintitle						);
		$tmpl->setVariable('winwidth'		,	$winwidth 						);
		$tmpl->setVariable('winheight'		,	$winheight 						);
		$tmpl->setVariable('winmax'			,	$winmax	 						);
		$tmpl->setVariable('winmin'			,	$winmin	 						);
		$tmpl->setVariable('winmaxim'		,	$winmaxim 						);
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 		
		
		$tmpl->parseCurrentBlock();	
	}
	
	sql_close($conn);
	//--------------------------------------------------------------------------
	
	
	$tmpl->show();
?>
