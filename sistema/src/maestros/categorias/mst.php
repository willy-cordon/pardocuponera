<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";		
	require_once GLBRutaFUNC.'/c_estados.php';

	$pathview= GLBRutaVIEW;
	eval("\$pathview = \"$pathview\";");	
			
	$tmpl= new HTML_Template_Sigma();	
	$tmpl->loadTemplateFile($pathview.'/maestros/categorias/mst.html');
	$tmpl->setVariable('gblpathjs' , GLBRutaJS 	);  //Path JS	
	$tmpl->setVariable('gblpathsty', GLBRutaSTY ); 	//Estilo	
	$tmpl->setVariable('gblpathimg', GLBRutaIMG );  //Path Imagenes
	//--------------------------------------------------------------------------------------------------------------
	//Datos de Ventana
	$winid		= 9602; //Ventana de Maestro
	$winidbrw	= 9601; //Ventana de Browser
	$wintitle	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['TITLE'];
	$winwidth	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WIDTH'];
	$winheight	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['HEIGHT'];
	$winmax		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAX'];
	$winmin		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MIN'];
	$winmaxim	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAXIM'];	
	$winfile	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WINFILE'];
	//--------------------------------------------------------------------------------------------------------------		
	$catreg 	= '';
	$catfchpub 	= '';
	$catdescri 	= '';
	$catimagen 	= '';
	$cattipo 	= '';
	$estcodigo 	= 1;
	$catorden 	= '';
	
	$pathimagenes 	= '../../../imges/data/categorias/';
	//--------------------------------------------------------------------------------------------------------------
	$catreg	= isset($_GET['CI'])? $_GET['CI']: 'NEW';
	$winid		= $winid.$catreg;
	$tmpl->setVariable('winid'		, $winid 		);
	$tmpl->setVariable('winidbrw'	, $winidbrw 	);
	//--------------------------------------------------------------------------------------------------------------
	$conn= sql_conectar();//Apertura de Conexion
	
	if($catreg != 'NEW'){		
		$query	= "SELECT CATREG,CATFCHPUB,CATDESCRI,CATIMAGEN,CATTIPO,ESTCODIGO,CATORDEN 
					FROM CAT_MAEST 
					WHERE CATREG = $catreg ";
								   
		$Table	= sql_query($query,$conn);
		$row	= $Table->Rows[0];
			
		$catreg 	= trim($row['CATREG']);
		$catfchpub 	= trim($row['CATFCHPUB']);
		$catdescri 	= trim($row['CATDESCRI']);
		$catimagen 	= trim($row['CATIMAGEN']);
		$cattipo 	= trim($row['CATTIPO']);
		$estcodigo 	= trim($row['ESTCODIGO']);
		$catorden 	= trim($row['CATORDEN']);
		
		$tmpl->setVariable('catreg'		, $catreg 		);
		$tmpl->setVariable('catfchpub'	, $catfchpub 	);
		$tmpl->setVariable('catdescri'	, $catdescri 	);
		$tmpl->setVariable('catimagen'	, $catimagen 	);
		$tmpl->setVariable('cattipo'	, $cattipo 		);
		$tmpl->setVariable('estcodigo'	, $estcodigo 	);
		$tmpl->setVariable('catorden'	, $catorden 	);
		
		$tmpl->setVariable('cattipo_'.$cattipo	, 'selected'		);
	}
	
	if($catreg != 'NEW'){	
		$tmpl->setVariable('catimagenurl', $pathimagenes.$catreg.'/'.$catimagen	);
	}

	CEstados('estado' ,$tmpl,$conn,$estcodigo);		//Estados
	
	//--------------------------------------------------------------------------------------------------------------
	sql_close($conn);
		
	$tmpl->show();
	
?>	
