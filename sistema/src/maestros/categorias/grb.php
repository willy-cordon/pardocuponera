<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";
	//--------------------------------------------------------------------------------------------------------------	
	$winid 		= 9602; //Id de Ventana Maestro
	$winidbrw 	= 9601; //Id de Ventana Browser
	$pathimagenes 	= '/imges/data/categorias/'; //Carpeta de iamgenes locales
	if (!file_exists("../../..".$pathimagenes)) {
		mkdir("../../..".$pathimagenes);	   				
	}
	
	//Copia el archivo a la carpeta del "clientes" para la web
	$pathcopyclientes = "C:/AppWeb/proyectolg/frontend/assets/images/cat/";	//PRD 
	//$pathcopyclientes = '../../../../proyectolg/assets/images/cat/'; //DEV
			
	if($pathcopyclientes!=''){		
		if (!file_exists($pathcopyclientes)) {
			mkdir($pathcopyclientes);	   				
		}		
	}
			
	//--------------------------------------------------------------------------------------------------------------		
	//Inicializo Variables
	$catreg 	= '';
	$catfchpub 	= '';
	$catdescri 	= '';
	$catimagen 	= '';
	$cattipo 	= '';
	$estcodigo 	= 1;
	$catorden 	= '';
	//--------------------------------------------------------------------------------------------------------------
	$errcod = 0;
	$err 	= 'SQLACCEPT';	

	$catreg			= trim($_POST['catreg']);
	$catregant		= trim($_POST['catregant']);
	$catdescri		= trim($_POST['catdescri']);
	$catfchpub		= trim($_POST['catfchpub']);
	$estcodigo		= trim($_POST['estcodigo']);
	$catimagen		= trim($_POST['catimagen']);
	$catimagenant	= trim($_POST['catimagenant']);
	$catorden		= trim($_POST['catorden']);
	$cattipo		= trim($_POST['cattipo']);
	
	//--------------------------------------------------------------------------------------------------------------	
	if(isset($_FILES['file'])){		
		$ext 		= pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
		$file		= $_FILES['file'];				
		$digit 		= rand(10,100);
		$catimagen	= "C_".time().$digit.".".$ext;
	}else{
		$catimagen = basename($catimagenant);
	}
	//--------------------------------------------------------------------------------------------------------------	
	$conn	= sql_conectar();//Apertura de Conexion
	$trans	= sql_begin_trans($conn);
	
	$newreg = false;
	$catreg = VarNullBD($catreg ,'N');
	if($catreg == 0){ //Nuevo			
		//- - - -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 		
		//Genero un ID 
		$query 		= 'SELECT GEN_ID(G_CATEGORIAS,1) AS ID FROM RDB$DATABASE';
		$TblId		= sql_query($query,$conn);
		$RowId		= $TblId->Rows[0];			
		$catreg 	= trim($RowId['ID']);
		//- - - -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
		$newreg		= true;
	}
	
	$catdescri = str_replace("'","''",$catdescri);
	
	$catregant		= VarNullBD($catregant	 	,'N');
	$catdescri		= VarNullBD($catdescri		,'S');
	$catfchpub		= VarNullBD($catfchpub		,'S');
	$estcodigo		= VarNullBD($estcodigo		,'N');
	$catorden		= VarNullBD($catorden		,'N');
	$cattipo		= VarNullBD($cattipo		,'N');
	
	if($catorden==0 || $catorden==''){
		$query 		= "SELECT MAX(CATORDEN)+1 AS ORDEN FROM CAT_MAEST WHERE CATTIPO=$cattipo";
		$TblOrd		= sql_query($query,$conn);
		$RowOrd		= $TblOrd->Rows[0];			
		$catorden 	= trim($RowOrd['ORDEN']);
		if($catorden=='') $catorden=1;
	}
		 	
	//--------------------------------------------------------------------------------------------------------------
	if($newreg){ //Nuevo
		$winid.='NEW';
				
		$query = "	INSERT INTO CAT_MAEST(CATREG,CATFCHPUB,CATDESCRI,CATIMAGEN,CATTIPO,ESTCODIGO,CATORDEN )
					VALUES($catreg,$catfchpub,$catdescri,'$catimagen',$cattipo,$estcodigo,$catorden)";
		$err   = sql_execute($query,$conn,$trans);	
	}else{ //Modificar
		$winid.=$catreg;
		
		$query = "	UPDATE CAT_MAEST SET
						CATIMAGEN 	= '$catimagen', CATTIPO=$cattipo,
						CATDESCRI 	= $catdescri,CATFCHPUB 	= $catfchpub,
						ESTCODIGO 	= $estcodigo, CATORDEN = $catorden
					WHERE CATREG = $catreg ";
		$err   = sql_execute($query,$conn,$trans);			
	}
	
	//--------------------------------------------------------------------------------------------------------------		
	if($catimagen != $catimagenant){
		if($errcod == 0 && $err == 'SQLACCEPT'){
			//Creo el directorio si no existe
			if (!file_exists("../../..".$pathimagenes.$catreg)) {
				mkdir("../../..".$pathimagenes.$catreg);	   				
			}
			
			//Elimino la foto actual si existe
			if(file_exists("../../..".$pathimagenes.$catreg.'/'.$catimagen)){
				unlink("../../..".$pathimagenes.$catreg.'/'.$catimagen);
			}
			
			move_uploaded_file( $file['tmp_name'], "../../..".$pathimagenes.$catreg.'/'.$catimagen);
			
			if($pathcopyclientes!=''){
				if (!file_exists($pathcopyclientes.$catreg.'/')) {
					mkdir($pathcopyclientes.$catreg.'/');	   				
				}
				copy( "../../..".$pathimagenes.$catreg.'/'.$catimagen, $pathcopyclientes.$catreg.'/'.$catimagen);			
			}
		}
	}
	//--------------------------------------------------------------------------------------------------------------	
	
	if($err == 'SQLACCEPT'){
		sql_commit_trans($trans);		
		$errcod = 0;
		$errmsg = 'Guardado Correcto!';        
	}else{ 
		sql_rollback_trans($trans);		
		$errcod = 2;
		$errmsg = 'No se Guardo';
	}	

	//--------------------------------------------------------------------------------------------------------------	
	echo '{"errcod":"'.$errcod.'", "errmsg":"'.$errmsg.'"}';
	//--------------------------------------------------------------------------------------------------------------	
	    
	sql_close($conn);
	//--------------------------------------------------------------------------------------------------------------
	    
?>
