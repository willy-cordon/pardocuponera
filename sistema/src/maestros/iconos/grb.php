<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";
	//--------------------------------------------------------------------------------------------------------------	
	$winid 		= 9; //Id de Ventana Maestro
		
	$errcod = 0;
	$msgnro = 0;
	$err	= '';
	//--------------------------------------------------------------------------------------------------------------		
	//Inicializo Variables
	$winidicon	= '';	//Id de Ventana Icono	
	$action 	= '';	//Accion Elegida, borrar o agregar
	$usuicoord	= 0;	//Orden del Icono
	//--------------------------------------------------------------------------------------------------------------	
	$winicon 	= $_POST['winicon'];
	$existe 	= $_POST['existe'];
	$winidicon 	= $_POST['winid'];
	$action 	= $_POST['action'];
	$usuicoord 	= $_POST['usuicoord'];
	$wintitle 	= $_POST['wintitle'];
	
		
	//--------------------------------------------------------------------------------------------------------------
	$conn	= sql_conectar();//Apertura de Conexion
	if($action == 'A' && $existe==0){ //AGREGAR		
		$query = "INSERT INTO ZZZ_USER_WIND_ICON(EMPCODIGO,WINID,USUCODIGO,USUICOORD,WINICON,WINICONTIT) 
						VALUES($empcodigo,$winidicon,$usucodigo,$usuicoord,'$winicon','$wintitle') ";
		$err   = sql_execute($query,$conn);	
	}else{
		if($action == 'A' && $existe==1){ //AGREGAR		
			$query = "UPDATE ZZZ_USER_WIND_ICON 
					  SET USUICOORD   = $usuicoord, 
					      WINICON     ='$winicon', 
						  WINICONTIT ='$wintitle'
					  WHERE EMPCODIGO=$empcodigo
					  AND WINID=$winidicon
					  AND USUCODIGO=$usucodigo";
			$err   = sql_execute($query,$conn);	
		}
	}
	if($action == 'D'){ //BORRAR
		$query = "DELETE FROM ZZZ_USER_WIND_ICON WHERE EMPCODIGO=$empcodigo AND WINID=$winidicon AND USUCODIGO=$usucodigo";
		$err   = sql_execute($query,$conn);	
	}
	
	
		
	if($errcod==0 && $err == 'SQLACCEPT'){
		if($action == 'A')
			$errmsg = 'Icono Agregado en Escritorio';
		else
			$errmsg = 'Icono Eliminado del Escritorio';
	}else{    
		$errcod = 2;
		$errmsg = 'Error al Guardar';
	}	
	//--------------------------------------------------------------------------------------------------------------	
	sql_close($conn);
	//--------------------------------------------------------------------------------------------------------------	
	echo "<respuesta>";                 
	   echo "<errcod> $errcod </errcod>";		               
	   echo "<msg> $errmsg </msg>";
	   echo "<screxe> (RefreshBrw('$winid','')) </screxe>"; 
	echo "</respuesta>";	
	//--------------------------------------------------------------------------------------------------------------	
?>
