<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";
	require_once GLBRutaFUNC.'/c_tipcomp.php';	
			
	$pathview= GLBRutaVIEW;
	eval("\$pathview = \"$pathview\";");	
			
	$tmpl= new HTML_Template_Sigma();	
	$tmpl->loadTemplateFile($pathview.'/maestros/iconos/vslicono.html');
	$tmpl->setVariable('gblpathjs' , GLBRutaJS 	);  //Path JS	
	$tmpl->setVariable('gblpathsty', GLBRutaSTY ); 	//Estilo	
	$tmpl->setVariable('gblpathimg', GLBRutaIMG );  //Path Imagenes
		
	//--------------------------------------------------------------------------------------------------------------
	
	$winid		= $_GET['W']; //Ventana de Maestro
	
	if(strpos($winid,'NEW')){
		$winid =  substr($winid, 0, strpos($winid,'NEW'));
	}
	
	$wintitle	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['TITLE'];
	$winwidth	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WIDTH'];
	$winheight	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['HEIGHT'];
	$winmax		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAX'];
	$winmin		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MIN'];
	$winmaxim	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAXIM'];	
	$winfile	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WINFILE'];
		
	$tmpl->setVariable('winid'		, $winid			);	
	$tmpl->setVariable('wintitle'	, $wintitle			);
	//--------------------------------------------------------------------------------------------------------------	
	$conn= sql_conectar();//Apertura de Conexion
	
	$usuicoord 	= 0;
	$winicontit = '';
	$winicon 	= '';
	
	
	$query	= " SELECT WI.USUICOORD,WI.WINICON,WI.WINICONTIT
				FROM ZZZ_USER_WIND_ICON WI
				WHERE WI.EMPCODIGO=$empcodigo AND WI.USUCODIGO=$usucodigo AND WI.WINID=$winid ";
							   
	$Table	= sql_query($query,$conn);		
	if($Table->Rows_Count>0){
		$row	= $Table->Rows[0];
	
		$usuicoord 	= trim($row['USUICOORD']);
		$winicon 	= trim($row['WINICON']);	
		$winicontit	= trim($row['WINICONTIT']);
		
		//Si exite, habilito el boton de borrado
		$tmpl->setVariable('delvisible'	, 'visibile ');
		$tmpl->setVariable('existe'	, '1');
		
	}else{
		$tmpl->setVariable('delvisible'	, 'hidden');
		$tmpl->setVariable('existe'	, '0');
	}
		
	if($winicontit == ''){
		$winicontit = $wintitle;
	}
	
	//Asigno los Valores a la Pantalla	
	$tmpl->setVariable('usuicoord'	, $usuicoord 		);
	$tmpl->setVariable('winicon'	, $winicon			);
	$tmpl->setVariable('wintitle'	, $winicontit		);
	//--------------------------------------------------------------------------------------------------------------	
	sql_close($conn);	
	//--------------------------------------------------------------------------------------------------------------	
	$tmpl->show();	
?>