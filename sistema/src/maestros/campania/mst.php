<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";		
	require_once GLBRutaFUNC.'/c_estados.php';

	$pathview= GLBRutaVIEW;
	eval("\$pathview = \"$pathview\";");	
			
	$tmpl= new HTML_Template_Sigma();	
	$tmpl->loadTemplateFile($pathview.'/maestros/campania/mst.html');
	$tmpl->setVariable('gblpathjs' , GLBRutaJS 	);  //Path JS	
	$tmpl->setVariable('gblpathsty', GLBRutaSTY ); 	//Estilo	
	$tmpl->setVariable('gblpathimg', GLBRutaIMG );  //Path Imagenes
	
	//--------------------------------------------------------------------------------------------------------------
	//Datos de Ventana
	$winid		= 9911; //Ventana de Maestro
	$winidbrw	= 9910; //Ventana de Browser
	
	$wintitle	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['TITLE'];
	$winwidth	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WIDTH'];
	$winheight	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['HEIGHT'];
	$winmax		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAX'];
	$winmin		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MIN'];
	$winmaxim	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAXIM'];	
	$winfile	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WINFILE'];
	//--------------------------------------------------------------------------------------------------------------		
	$camreg 	= '';
	$camdescri 	= '';
	$camimagen 	= '';
	$camestcup 	= 1;
	$camcodcup 	= '';
	
	$pathimagenes 	= '../../../imges/data/campania/';
	//--------------------------------------------------------------------------------------------------------------
	$camreg	= isset($_GET['CI'])? $_GET['CI']: 'NEW';
	//logerror($camreg);
	$winid		= $winid.$camreg;
	$tmpl->setVariable('winid'		, $winid 		);
	$tmpl->setVariable('winidbrw'	, $winidbrw 	);
	//--------------------------------------------------------------------------------------------------------------
	$conn= sql_conectar();//Apertura de Conexion
	
	if($camreg != 'NEW'){		

		$query="SELECT * FROM CAM_CABE WHERE CAMREG=$camreg";
		//logerror($query);									   
		$Table	= sql_query($query,$conn);
		for($j=0; $j<$Table->Rows_Count; $j++){
		$row	= $Table->Rows[$j];
		
		$camreg 	= trim($row['CAMREG']);
		$camid 	= trim($row['CAMID']);
		$camcem 	= trim($row['CAMCEM']);
		$camcodcup 	= trim($row['CAMCODCUP']);
		$camdescri  = trim($row['CAMDESCRI']);
		$camvigdde  = trim($row['CAMVIGDDE']);
		$camvigddh  = trim($row['CAMVIGDDH']);
		$camvighta  = trim($row['CAMVIGHTA']);
		$camvighth  = trim($row['CAMVIGHTH']);
		$camdscrub  = trim($row['CAMDSCRUB']);
		$camestcup  = trim($row['CAMESTCUP']);
		$camimagen 	= trim($row['CAMIMAGEN']);
		$estcodigo 	= trim($row['ESTCODIGO']);
		

		
		
		$tmpl->setVariable('camreg'		, $camreg 		);
		$tmpl->setVariable('camid'		, $camid 		);
		$tmpl->setVariable('camcem'		, $camcem 		);
		$tmpl->setVariable('camcodcup'	, $camcodcup 	);
		$tmpl->setVariable('camdescri'	, $camdescri 	);
		$tmpl->setVariable('camvigdde'	, $camvigdde 	);
		$tmpl->setVariable('camvigddh'	, $camvigddh 	);
		$tmpl->setVariable('camvighta'	, $camvighta 	);
		$tmpl->setVariable('camvighth'	, $camvighth 	);
		$tmpl->setVariable('camdscrub'	, $camdscrub 	);
		$tmpl->setVariable('camimagen'	, $camimagen 	);
		$tmpl->setVariable('camestcup'	, $camestcup 	);
		$tmpl->setVariable('estcodigo'	, $estcodigo 	);
		
		}
	
	}
	if($camreg != 'NEW'){	
		$tmpl->setVariable('camimgurl', $pathimagenes.$camreg.'/'.$camimagen	);
	}
	CEstados('estado' ,$tmpl,$conn,$estcodigo);	

	
	//--------------------------------------------------------------------------------------------------------------
	sql_close($conn);
		
	$tmpl->show();
	
?>	
