<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";		
	require_once GLBRutaFUNC.'/c_estados.php';

	$pathview= GLBRutaVIEW;
	eval("\$pathview = \"$pathview\";");	
			
	$tmpl= new HTML_Template_Sigma();	
	$tmpl->loadTemplateFile($pathview.'/maestros/blog/mst.html');
	$tmpl->setVariable('gblpathjs' , GLBRutaJS 	);  //Path JS	
	$tmpl->setVariable('gblpathsty', GLBRutaSTY ); 	//Estilo	
	$tmpl->setVariable('gblpathimg', GLBRutaIMG );  //Path Imagenes
	//--------------------------------------------------------------------------------------------------------------
	//Datos de Ventana
	$winid		= 9302; //Ventana de Maestro
	$winidbrw	= 9301; //Ventana de Browser
	$wintitle	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['TITLE'];
	$winwidth	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WIDTH'];
	$winheight	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['HEIGHT'];
	$winmax		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAX'];
	$winmin		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MIN'];
	$winmaxim	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAXIM'];	
	$winfile	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WINFILE'];
	//--------------------------------------------------------------------------------------------------------------		
	$bloreg		= '';
	$blotitulo	= '';
	$blodescri	= '';
	$estcodigo	= '';
	$bloimagen	= '';
	$bloorden	= 1;
	$blovideo	= '';
	$blolink	= '';
	$chkenblog	= 'N';
	$chkempres	= 'N';
	$chkproduc	= 'N';
	$chkventas	= 'N';
	$chkexperi	= 'N';
	$chkgoodti	= 'N';
	$chkdesgru	= 'N';
	$chkdesgan	= 'N';
	
	$pathimagenes 	= '../../../imges/data/blog/';
	//--------------------------------------------------------------------------------------------------------------
	$bloreg	= isset($_GET['CI'])? $_GET['CI']: 'NEW';
	$winid		= $winid.$bloreg;
	$tmpl->setVariable('winid'		, $winid 		);
	$tmpl->setVariable('winidbrw'	, $winidbrw 	);
	//--------------------------------------------------------------------------------------------------------------
	$conn= sql_conectar();//Apertura de Conexion
	
	if($bloreg != 'NEW'){		
		$query	= "SELECT B.BLOREG,B.BLOTITULO,B.BLODESCRI,
							B.BLOFCHPUB,B.BLOIMAGEN,B.ESTCODIGO,B.BLOORDEN,B.BLOVIDEO,
							B.BLOLINK,CHKENBLOG,CHKEMPRES,CHKPRODUC,CHKVENTAS,CHKEXPERI,CHKGOODTI,CHKDESGRU,CHKDESGAN
					FROM BLO_CABE B
					WHERE B.BLOREG = $bloreg ";
								   
		$Table	= sql_query($query,$conn);
		$row	= $Table->Rows[0];
			
		$bloreg 	= trim($row['BLOREG']);
		$blotitulo 	= trim($row['BLOTITULO']);
		$blodescri 	= trim($row['BLODESCRI']);
		$blofchpub 	= trim($row['BLOFCHPUB']);
		$bloimagen 	= trim($row['BLOIMAGEN']);
		$estcodigo 	= trim($row['ESTCODIGO']);
		$bloorden 	= trim($row['BLOORDEN']);
		$blovideo 	= trim($row['BLOVIDEO']);
		$blolink 	= trim($row['BLOLINK']);
		$chkenblog	= trim($row['CHKENBLOG']);
		$chkempres	= trim($row['CHKEMPRES']);
		$chkproduc	= trim($row['CHKPRODUC']);
		$chkventas	= trim($row['CHKVENTAS']);
		$chkexperi	= trim($row['CHKEXPERI']);
		$chkgoodti	= trim($row['CHKGOODTI']);
		$chkdesgru	= trim($row['CHKDESGRU']);
		$chkdesgan	= trim($row['CHKDESGAN']);
		
		$chkenblog = ($chkenblog=='')? 'N' : $chkenblog;
		$chkempres = ($chkempres=='')? 'N' : $chkempres;
		$chkproduc = ($chkproduc=='')? 'N' : $chkproduc;
		$chkventas = ($chkventas=='')? 'N' : $chkventas;
		$chkexperi = ($chkexperi=='')? 'N' : $chkexperi;
		$chkgoodti = ($chkgoodti=='')? 'N' : $chkgoodti;
		$chkdesgru = ($chkdesgru=='')? 'N' : $chkdesgru;
		$chkdesgan = ($chkdesgan=='')? 'N' : $chkdesgan;
		
		$tmpl->setVariable('bloreg'		, $bloreg 		);
		$tmpl->setVariable('blotitulo'	, $blotitulo 	);
		$tmpl->setVariable('blodescri'	, $blodescri 	);
		$tmpl->setVariable('blofchpub'	, $blofchpub 	);
		$tmpl->setVariable('bloimagen'	, $bloimagen 	);
		$tmpl->setVariable('bloorden'	, $bloorden 	);
		$tmpl->setVariable('blovideo'	, $blovideo 	);
		$tmpl->setVariable('blolink'	, $blolink	 	);
		
		if($chkenblog=='S') $tmpl->setVariable('checkenblog', 'checked'	);
		if($chkempres=='S') $tmpl->setVariable('checkempres', 'checked'	);
		if($chkproduc=='S') $tmpl->setVariable('checkproduc', 'checked'	);
		if($chkventas=='S') $tmpl->setVariable('checkventas', 'checked'	);
		if($chkexperi=='S') $tmpl->setVariable('checkexperi', 'checked'	);
		if($chkgoodti=='S') $tmpl->setVariable('checkgoodti', 'checked'	);
		if($chkdesgru=='S') $tmpl->setVariable('checkdesgru', 'checked'	);
		if($chkdesgan=='S') $tmpl->setVariable('checkdesgan', 'checked'	);
	}
	
	$tmpl->setVariable('chkenblog'	, $chkenblog	);
	$tmpl->setVariable('chkempres'	, $chkempres	);
	$tmpl->setVariable('chkproduc'	, $chkproduc	);
	$tmpl->setVariable('chkventas'	, $chkventas	);
	$tmpl->setVariable('chkexperi'	, $chkexperi	);
	$tmpl->setVariable('chkgoodti'	, $chkgoodti	);
	$tmpl->setVariable('chkdesgru'	, $chkdesgru	);
	$tmpl->setVariable('chkdesgan'	, $chkdesgan	);
	
	if($bloreg != 'NEW'){	
		$tmpl->setVariable('bloimagenurl', $pathimagenes.$bloreg.'/'.$bloimagen	);
	}

	CEstados('estado' ,$tmpl,$conn,$estcodigo);		//Estados
	
	//--------------------------------------------------------------------------------------------------------------
	sql_close($conn);
		
	$tmpl->show();
	
?>	
