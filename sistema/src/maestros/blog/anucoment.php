<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";
	//--------------------------------------------------------------------------------------------------------------
	$bloreg 	= trim($_POST['bloreg']);
	$blocomitm 	= trim($_POST['blocomitm']);
	
	$conn= sql_conectar();//Apertura de Conexion
	$trans	= sql_begin_trans($conn);
	
	$query = " UPDATE BLO_COME SET ESTCODIGO=2 WHERE BLOREG=$bloreg AND BLOCOMITM=$blocomitm ";
	$err   = sql_execute($query,$conn,$trans);	
	//--------------------------------------------------------------------------------------------------------------	
	if($err == 'SQLACCEPT'){
		sql_commit_trans($trans);		
		$errcod = 0;
		$errmsg = 'Anulado Correctamente!';        
	}else{ 
		sql_rollback_trans($trans);		
		$errcod = 2;
		$errmsg = 'No se Anulo';
	}	

	//--------------------------------------------------------------------------------------------------------------	
	echo '{"errcod":"'.$errcod.'", "errmsg":"'.$errmsg.'", "bloreg":"'.$bloreg.'"}';
	//--------------------------------------------------------------------------------------------------------------	
	    
	sql_close($conn);
	//--------------------------------------------------------------------------------------------------------------
	    
?>
