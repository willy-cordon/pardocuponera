<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";
		
	$pathview= GLBRutaVIEW;
	eval("\$pathview = \"$pathview\";");	
			
	$tmpl= new HTML_Template_Sigma();	
	$tmpl->loadTemplateFile($pathview.'/maestros/blog/brw.html');
	$tmpl->setVariable('gblpathjs' , GLBRutaJS 	);  //Path JS	
	$tmpl->setVariable('gblpathsty', GLBRutaSTY ); 	//Estilo	
	$tmpl->setVariable('gblpathimg', GLBRutaIMG );  //Path Imagenes
	//--------------------------------------------------------------------------------------------------------------
	//Venta Browser
	$winidbrw 	= 9301; 
	$tmpl->setVariable('winidbrw' ,	$winidbrw );
	
	//Datos de Ventana	
	$winid		= 9302; //Ventana de Maestro
	$wintitle	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['TITLE'];
	$winwidth	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WIDTH'];
	$winheight	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['HEIGHT'];
	$winmax		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAX'];
	$winmin		= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MIN'];
	$winmaxim	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['MAXIM'];	
	$winfile	= $_SESSION[GLBAPPPORT.'TITLES'][$winid]['WINFILE'];
	
	//Ventana de Comentarios
	$winiddet		=	9303;	
	$wintitledet	= $_SESSION[GLBAPPPORT.'TITLES'][$winiddet]['TITLE'];
	$winwidthdet	= $_SESSION[GLBAPPPORT.'TITLES'][$winiddet]['WIDTH'];
	$winheightdet	= $_SESSION[GLBAPPPORT.'TITLES'][$winiddet]['HEIGHT'];
	$winmaxdet		= $_SESSION[GLBAPPPORT.'TITLES'][$winiddet]['MAX'];
	$winmindet		= $_SESSION[GLBAPPPORT.'TITLES'][$winiddet]['MIN'];
	$winmaximdet	= $_SESSION[GLBAPPPORT.'TITLES'][$winiddet]['MAXIM'];	
	$winfiledet		= $_SESSION[GLBAPPPORT.'TITLES'][$winiddet]['WINFILE'];
	
	//Ventana de Evidencias
	$winidevi		=	9304;	
	$wintitleevi	= $_SESSION[GLBAPPPORT.'TITLES'][$winidevi]['TITLE'];
	$winwidthevi	= $_SESSION[GLBAPPPORT.'TITLES'][$winidevi]['WIDTH'];
	$winheightevi	= $_SESSION[GLBAPPPORT.'TITLES'][$winidevi]['HEIGHT'];
	$winmaxevi		= $_SESSION[GLBAPPPORT.'TITLES'][$winidevi]['MAX'];
	$winminevi		= $_SESSION[GLBAPPPORT.'TITLES'][$winidevi]['MIN'];
	$winmaximevi	= $_SESSION[GLBAPPPORT.'TITLES'][$winidevi]['MAXIM'];	
	$winfileevi		= $_SESSION[GLBAPPPORT.'TITLES'][$winidevi]['WINFILE'];
	//--------------------------------------------------------------------------------------------------------------
	//Filtros de Busqueda
	$pathimagenes 	= '../../../imges/data/blog/';
	$imgwhite		= 'imges/cancelar.png';
	
	$fltwhere 		= '';
	if(isset($_POST['fltbuscar'])){		
		$fltbuscar	= trim($_POST['fltbuscar']);
		if($fltbuscar != ''){
			$tmpl->setVariable('fltbuscar', $fltbuscar);
			$fltwhere 	= " AND (B.BLOTITULO CONTAINING '$fltbuscar' OR B.BLODESCRI CONTAINING '$fltbuscar') ";
		}
	}
	
	//Estado
	if(isset($_POST['fltestcodigo'])){		
		$fltestcodigo	= trim($_POST['fltestcodigo']);		
		$tmpl->setVariable('fltestcodigo', $fltestcodigo);
	}
	
	//Tipo
	if(isset($_POST['fltblotipo'])){		
		$fltblotipo	= trim($_POST['fltblotipo']);		
		$tmpl->setVariable('fltblotipo', $fltblotipo);
		
		switch($fltblotipo){
			case 1:
				$fltwhere .= " AND CHKENBLOG='S' ";
				break;
			case 2:
				$fltwhere .= " AND CHKEMPRES='S' ";
				break;
			case 3:
				$fltwhere .= " AND CHKPRODUC='S' ";
				break;
			case 4:
				$fltwhere .= " AND CHKVENTAS='S' ";
				break;
			case 5:
				$fltwhere .= " AND CHKEXPERI='S' ";
				break;
			case 6:
				$fltwhere .= " AND CHKGOODTI='S' ";
				break;
			case 7:
				$fltwhere .= " AND CHKDESGRU='S' ";
				break;
			case 8:
				$fltwhere .= " AND CHKDESGAN='S' ";
				break;
		}
	}
	//--------------------------------------------------------------------------------------------------------------
	$conn= sql_conectar();//Apertura de Conexion
		
	//PAGINACION -----------------------------------------------	
    $sorton = isset($_POST['sorton'])? $_POST["sorton"]: 'B.BLOORDEN';
	$sortby = isset($_POST['sortby'])? $_POST["sortby"]: 'DESC';
	
	$inicio = 0;  
	if (isset($_POST['incre'])) $inicio = $_POST['incre'];
	if($inicio <= 0) $inicio = 0;
	$fin 	= $inicio + GLBFilas;
	$sort 	= "ORDER BY $sorton $sortby";	
	//----------------------------------------------------------		
	
	$conn= sql_conectar();//Apertura de Conexion
	
	$query   = "SELECT B.BLOREG,B.BLOTITULO,SUBSTRING(B.BLODESCRI FROM 1 FOR 50)||'...' AS BLODESCRI,
					B.BLOFCHPUB,B.BLOIMAGEN,B.ESTCODIGO,B.BLOORDEN,B.CHKDESGRU,B.CHKDESGAN
				FROM BLO_CABE B
				WHERE B.ESTCODIGO=$fltestcodigo $fltwhere
				$sort";
	
	$query = PaginarSql($inicio,$query);	
	$Table = sql_query($query,$conn);
	logerror($query);
	//PAGINACION -----------------------------------------------
    $tmpl->setVariable('gblpathimgPag'	, GLBRutaIMG );  //Path Imagenes
    $tmpl->setVariable('pagfilas'		, GLBFilas); 
	$tmpl->setVariable('incre'			, $inicio);
    $tmpl->setVariable('sorton'			, $sorton);
	$tmpl->setVariable('sortby'			, $sortby);
	$tmpl->setVariable('cntrow'			, $Table->Rows_Count ); //Cantidad de Registros de Consulta
	//----------------------------------------------------------
		
	for($i=0; $i < $Table->Rows_Count; $i++){
		$row= $Table->Rows[$i];
				
		$bloreg 	= trim($row['BLOREG']);
		$blotitulo 	= trim($row['BLOTITULO']);
		$blodescri 	= trim($row['BLODESCRI']);
		$blofchpub 	= trim($row['BLOFCHPUB']);
		$bloimagen 	= trim($row['BLOIMAGEN']);
		$estcodigo 	= trim($row['ESTCODIGO']);
		$bloorden 	= trim($row['BLOORDEN']);
		$chkdesgru 	= trim($row['CHKDESGRU']);
		$chkdesgan 	= trim($row['CHKDESGAN']);
		
		$tmpl->setCurrentBlock('browser');				
		$tmpl->setVariable('bloreg'		, $bloreg 	);
		$tmpl->setVariable('blotitulo'	, $blotitulo 	);
		$tmpl->setVariable('blodescri'	, $blodescri 	);
		$tmpl->setVariable('blofchpub'	, $blofchpub 	);
		$tmpl->setVariable('bloorden'	, $bloorden 	);
		
		if($bloimagen=='')	$bloimagen = $imgwhite;					
		$tmpl->setVariable('bloimagen'		, $pathimagenes.$bloreg.'/'.$bloimagen	);
		
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		//Datos de Ventana		
		$tmpl->setVariable('winid'			,	$winid.$bloreg 				);
		$tmpl->setVariable('winfile'		,	$winfile.'?CI='.$bloreg		); 
		$tmpl->setVariable('wintitle'		,	$wintitle 						);
		$tmpl->setVariable('winwidth'		,	$winwidth 						);
		$tmpl->setVariable('winheight'		,	$winheight 						);
		$tmpl->setVariable('winmax'			,	$winmax	 						);
		$tmpl->setVariable('winmin'			,	$winmin	 						);
		$tmpl->setVariable('winmaxim'		,	$winmaxim 						);

		$tmpl->setVariable('winiddet'		,	$winiddet.$bloreg 				);
		$tmpl->setVariable('winfiledet'		,	$winfiledet.'?CI='.$bloreg 		); 
		$tmpl->setVariable('wintitledet'	,	$wintitledet 					);
		$tmpl->setVariable('winwidthdet'	,	$winwidthdet 					);
		$tmpl->setVariable('winheightdet'	,	$winheightdet 					);
		$tmpl->setVariable('winmaxdet'		,	$winmaxdet	 					);
		$tmpl->setVariable('winmindet'		,	$winmindet	 					);
		$tmpl->setVariable('winmaximdet'	,	$winmaximdet 					);
		
		$tmpl->setVariable('winidevi'		,	$winidevi.$bloreg 				);
		$tmpl->setVariable('winfileevi'		,	$winfileevi.'?CI='.$bloreg 		); 
		$tmpl->setVariable('wintitleevi'	,	$wintitleevi 					);
		$tmpl->setVariable('winwidthevi'	,	$winwidthevi 					);
		$tmpl->setVariable('winheightevi'	,	$winheightevi 					);
		$tmpl->setVariable('winmaxevi'		,	$winmaxevi	 					);
		$tmpl->setVariable('winminevi'		,	$winminevi	 					);
		$tmpl->setVariable('winmaximevi'	,	$winmaximevi 					);
		if($chkdesgru=='N' && $chkdesgru=='N'){
			$tmpl->setVariable('viewevidencias'	,	'hidden' );
		}
		//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 		
		
		$tmpl->parseCurrentBlock();	
	}
	
	sql_close($conn);
	//--------------------------------------------------------------------------
	
	
	$tmpl->show();
?>
