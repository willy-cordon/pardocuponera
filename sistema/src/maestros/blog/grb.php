<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";
	//--------------------------------------------------------------------------------------------------------------	
	$winid 		= 9302; //Id de Ventana Maestro
	$winidbrw 	= 9301; //Id de Ventana Browser
	$pathimagenes 	= '/imges/data/blog/'; //Carpeta de iamgenes locales
	if (!file_exists("../../..".$pathimagenes)) {
		mkdir("../../..".$pathimagenes);	   				
	}
	
	//Copia el archivo a la carpeta del "clientes" para la web
	$pathcopyclientes = "C:/AppWeb/proyectolg/frontend/assets/images/";	//PRD 
	//$pathcopyclientes = '../../../../proyectolg/assets/images/'; //DEV
			
	//--------------------------------------------------------------------------------------------------------------		
	//Inicializo Variables
	$bloreg		= '';
	$bloregant	= '';
	$blotitulo	= '';
	$blodescri	= '';
	$bloimagen	= '';
	$blofchpub	= '';
	$estcodigo	= '';
	$bloorden	= '';
	$blovideo	= '';
	$blolink	= '';
	$chkenblog	= 'N';
	$chkempres	= 'N';
	$chkproduc	= 'N';
	$chkventas	= 'N';
	$chkexperi	= 'N';
	$chkgoodti	= 'N';
	$chkdesgru	= 'N';
	$chkdesgan	= 'N';
	//--------------------------------------------------------------------------------------------------------------
	$errcod = 0;
	$err 	= 'SQLACCEPT';	

	$bloreg			= trim($_POST['bloreg']);
	$bloregant		= trim($_POST['bloregant']);
	$blotitulo		= trim($_POST['blotitulo']);
	$blodescri		= trim($_POST['blodescri']);
	$blofchpub		= trim($_POST['blofchpub']);
	$estcodigo		= trim($_POST['estcodigo']);
	$bloimagen		= trim($_POST['bloimagen']);
	$bloimagenant	= trim($_POST['bloimagenant']);
	$bloorden		= trim($_POST['bloorden']);
	$blovideo		= trim($_POST['blovideo']);
	$blovideoant	= trim($_POST['blovideoant']);
	$blolink		= trim($_POST['blolink']);
	$chkenblog		= trim($_POST['chkenblog']);
	$chkempres		= trim($_POST['chkempres']);
	$chkproduc		= trim($_POST['chkproduc']);
	$chkventas		= trim($_POST['chkventas']);
	$chkexperi		= trim($_POST['chkexperi']);
	$chkgoodti		= trim($_POST['chkgoodti']);
	$chkdesgru		= trim($_POST['chkdesgru']);
	$chkdesgan		= trim($_POST['chkdesgan']);
	//--------------------------------------------------------------------------------------------------------------	
	if(isset($_FILES['file'])){		
		$ext 		= pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
		$file		= $_FILES['file'];				
		$digit 		= rand(10,100);
		$bloimagen	= "I_".time().$digit.".".$ext;
		
	}else{
		$bloimagen = basename($bloimagenant);
	}
	
	if(isset($_FILES['filevideo'])){		
		$ext 		= pathinfo($_FILES['filevideo']['name'], PATHINFO_EXTENSION);
		$filevideo	= $_FILES['filevideo'];				
		$digit 		= rand(10,100);
		//$blovideo	= "V_".time().$digit.".".$ext;
		$blovideo	= trim($_FILES['filevideo']['name']);
		
	}else{
		$blovideo = basename($blovideoant);
	}
	//--------------------------------------------------------------------------------------------------------------	
	$conn	= sql_conectar();//Apertura de Conexion
	$trans	= sql_begin_trans($conn);
	
	$newreg = false;
	$bloreg = VarNullBD($bloreg ,'N');
	if($bloreg == 0){ //Nuevo			
		//- - - -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 		
		//Genero un ID 
		$query 		= 'SELECT GEN_ID(G_BLOGREG,1) AS ID FROM RDB$DATABASE';
		$TblId		= sql_query($query,$conn);
		$RowId		= $TblId->Rows[0];			
		$bloreg 	= trim($RowId['ID']);
		//- - - -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
		$newreg		= true;
	}
	
	$bloregant	= VarNullBD($bloregant	 ,'N');
	$blotitulo	= VarNullBD($blotitulo	 ,'S');
	$blodescri	= VarNullBD($blodescri	 ,'S');
	$blofchpub	= VarNullBD($blofchpub	 ,'S');
	$estcodigo	= VarNullBD($estcodigo	 ,'N');
	$bloorden	= VarNullBD($bloorden	 ,'N');
	$blolink	= VarNullBD($blolink	 ,'S');
	$chkenblog	= VarNullBD($chkenblog	 ,'S');
	$chkempres	= VarNullBD($chkempres	 ,'S');
	$chkproduc	= VarNullBD($chkproduc	 ,'S');
	$chkventas	= VarNullBD($chkventas	 ,'S');
	$chkexperi	= VarNullBD($chkexperi	 ,'S');
	$chkgoodti	= VarNullBD($chkgoodti	 ,'S');
	$chkdesgru	= VarNullBD($chkdesgru	 ,'S');
	$chkdesgan	= VarNullBD($chkdesgan	 ,'S');
	
	if($bloorden==0 || $bloorden==''){
		$query 		= 'SELECT MAX(BLOORDEN)+1 AS ORDEN FROM BLO_CABE';
		$TblOrd		= sql_query($query,$conn);
		$RowOrd		= $TblOrd->Rows[0];			
		$bloorden 	= trim($RowOrd['ORDEN']);
		if($bloorden=='') $bloorden=1;
	}
		 	
	//--------------------------------------------------------------------------------------------------------------
	if($newreg){ //Nuevo
		$winid.='NEW';
				
		$query = "	INSERT INTO BLO_CABE(BLOREG,BLOTITULO,BLODESCRI,BLOFCHPUB,BLOIMAGEN,ESTCODIGO,BLOORDEN,BLOVIDEO,BLOLINK,
										CHKENBLOG,CHKEMPRES,CHKPRODUC,CHKVENTAS,CHKEXPERI,CHKGOODTI,CHKDESGRU,CHKDESGAN)
					VALUES($bloreg,$blotitulo,$blodescri,$blofchpub,'$bloimagen',1,$bloorden,'$blovideo',$blolink,
							$chkenblog,$chkempres,$chkproduc,$chkventas,$chkexperi,$chkgoodti,$chkdesgru,$chkdesgan)";
		$err   = sql_execute($query,$conn,$trans);	
	}else{ //Modificar
		$winid.=$bloreg;
		
		$query = "	UPDATE BLO_CABE SET
						BLOTITULO 	= $blotitulo,BLOIMAGEN 	= '$bloimagen',BLODESCRI 	= $blodescri,BLOFCHPUB 	= $blofchpub,
						ESTCODIGO 	= $estcodigo, BLOORDEN = $bloorden,BLOVIDEO	= '$blovideo', BLOLINK = $blolink,
						CHKENBLOG=$chkenblog,CHKEMPRES=$chkempres,CHKPRODUC=$chkproduc,CHKVENTAS=$chkventas,
						CHKEXPERI=$chkexperi,CHKGOODTI=$chkgoodti,CHKDESGRU=$chkdesgru,CHKDESGAN=$chkdesgan
					WHERE BLOREG = $bloreg ";
		$err   = sql_execute($query,$conn,$trans);			
	}
	
	//--------------------------------------------------------------------------------------------------------------		
	if($bloimagen != $bloimagenant){
		if($errcod == 0 && $err == 'SQLACCEPT'){
			//Creo el directorio si no existe
			if (!file_exists("../../..".$pathimagenes.$bloreg)) {
				mkdir("../../..".$pathimagenes.$bloreg);	   				
			}
			
			//Elimino la foto actual si existe
			if(file_exists("../../..".$pathimagenes.$bloreg.'/'.$bloimagen)){
				unlink("../../..".$pathimagenes.$bloreg.'/'.$bloimagen);
			}
			
			move_uploaded_file( $file['tmp_name'], "../../..".$pathimagenes.$bloreg.'/'.$bloimagen);
			
			if($pathcopyclientes!=''){
				if (!file_exists($pathcopyclientes.$bloreg.'/')) {
					mkdir($pathcopyclientes.$bloreg.'/');	   				
				}
				copy( "../../..".$pathimagenes.$bloreg.'/'.$bloimagen, $pathcopyclientes.$bloreg.'/'.$bloimagen);			
			}
		}
	}
	
	if($blovideo != $blovideoant){
		if($errcod == 0 && $err == 'SQLACCEPT'){
			//Creo el directorio si no existe
			if (!file_exists("../../..".$pathimagenes.$bloreg)) {
				mkdir("../../..".$pathimagenes.$bloreg);	   				
			}
			
			//Elimino la foto actual si existe
			if(file_exists("../../..".$pathimagenes.$bloreg.'/'.$blovideo)){
				unlink("../../..".$pathimagenes.$bloreg.'/'.$blovideo);
			}
			
			move_uploaded_file( $filevideo['tmp_name'], "../../..".$pathimagenes.$bloreg.'/'.$blovideo);
			
			if($pathcopyclientes!=''){
				if (!file_exists($pathcopyclientes.$bloreg.'/')) {
					mkdir($pathcopyclientes.$bloreg.'/');	   				
				}
				copy( "../../..".$pathimagenes.$bloreg.'/'.$blovideo, $pathcopyclientes.$bloreg.'/'.$blovideo);			
			}
		}
	}
	//--------------------------------------------------------------------------------------------------------------	
	
	if($err == 'SQLACCEPT'){
		sql_commit_trans($trans);		
		$errcod = 0;
		$errmsg = 'Guardado Correcto!';        
	}else{ 
		sql_rollback_trans($trans);		
		$errcod = 2;
		$errmsg = 'No se Guardo';
	}	

	//--------------------------------------------------------------------------------------------------------------	
	echo '{"errcod":"'.$errcod.'", "errmsg":"'.$errmsg.'"}';
	//--------------------------------------------------------------------------------------------------------------	
	    
	sql_close($conn);
	//--------------------------------------------------------------------------------------------------------------
	    
?>
