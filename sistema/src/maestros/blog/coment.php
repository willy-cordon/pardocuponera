<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";
		
	$pathview= GLBRutaVIEW;
	eval("\$pathview = \"$pathview\";");	
			
	$tmpl= new HTML_Template_Sigma();	
	$tmpl->loadTemplateFile($pathview.'/maestros/blog/coment.html');
	$tmpl->setVariable('gblpathjs' , GLBRutaJS 	);  //Path JS	
	$tmpl->setVariable('gblpathsty', GLBRutaSTY ); 	//Estilo	
	$tmpl->setVariable('gblpathimg', GLBRutaIMG );  //Path Imagenes
	//--------------------------------------------------------------------------------------------------------------
	$bloreg = trim($_GET['CI']);
	
	$conn= sql_conectar();//Apertura de Conexion
	
	$query   = "SELECT C.BLOREG,C.BLOCOMITM,C.BLOCOMENT,C.BLOFCHREG,A.ACCUSUNOM
				FROM BLO_COME C
				LEFT OUTER JOIN ACC_MAEST A ON A.ACCCOD=C.ACCCOD
				WHERE C.ESTCODIGO=1 AND C.BLOREG=$bloreg
				ORDER BY C.BLOFCHREG DESC ";
	
	$Table = sql_query($query,$conn);
	
	for($i=0; $i < $Table->Rows_Count; $i++){
		$row= $Table->Rows[$i];
				
		$bloreg 	= trim($row['BLOREG']);
		$blocomitm 	= trim($row['BLOCOMITM']);
		$blocoment 	= trim($row['BLOCOMENT']);
		$blofchreg 	= trim($row['BLOFCHREG']);
		$accusunom 	= trim($row['ACCUSUNOM']);
		
		$tmpl->setCurrentBlock('browser');				
		$tmpl->setVariable('bloreg'		, $bloreg 		);
		$tmpl->setVariable('blocomitm'	, $blocomitm 	);
		$tmpl->setVariable('blocoment'	, nl2br($blocoment) 	);
		$tmpl->setVariable('blofchreg'	, $blofchreg 	);
		$tmpl->setVariable('accusunom'	, $accusunom 	);
		$tmpl->parseCurrentBlock();	
	}
	
	sql_close($conn);
	//--------------------------------------------------------------------------
	
	
	$tmpl->show();
?>
