<?php include('../../../val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	//Funciones	
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];     	//Codigo de Usuario   
	$usugrpcod = $_SESSION[GLBAPPPORT.'USRGRPBVDSIS'];  		//Codigo de Grupo de Usuario   
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];     	//Codigo de Empresa
	$idicodigo = $_SESSION[GLBAPPPORT.'IDICODBVDSIS'];     	//Codigo de Idioma	
		
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
	require_once GLBRutaMSG."/msg$idicodigo.php";
		
	$pathview= GLBRutaVIEW;
	eval("\$pathview = \"$pathview\";");	
			
	$tmpl= new HTML_Template_Sigma();	
	$tmpl->loadTemplateFile($pathview.'/maestros/blog/evidencias.html');
	$tmpl->setVariable('gblpathjs' , GLBRutaJS 	);  //Path JS	
	$tmpl->setVariable('gblpathsty', GLBRutaSTY ); 	//Estilo	
	$tmpl->setVariable('gblpathimg', GLBRutaIMG );  //Path Imagenes
	//--------------------------------------------------------------------------------------------------------------
	$pathimagenes 	= '../../../imges/data/evidencias/';
	
	$bloreg = trim($_GET['CI']);
	
	$conn= sql_conectar();//Apertura de Conexion
	
	$query   = "SELECT C.BLOREG,C.BLOEVIREG,C.BLOEVIFIL,A.ACCUSUNOM,C.BLOEVIFCH
				FROM BLO_EVID C
				LEFT OUTER JOIN ACC_MAEST A ON A.ACCCOD=C.ACCCOD
				WHERE  C.BLOREG=$bloreg
				ORDER BY C.BLOEVIFCH DESC ";
	
	$Table = sql_query($query,$conn);
	
	for($i=0; $i < $Table->Rows_Count; $i++){
		$row= $Table->Rows[$i];
				
		$bloreg 	= trim($row['BLOREG']);
		$bloevireg 	= trim($row['BLOEVIREG']);
		$bloevifil 	= trim($row['BLOEVIFIL']);
		$bloevifch 	= trim($row['BLOEVIFCH']);
		$accusunom 	= trim($row['ACCUSUNOM']);
		
		$tmpl->setCurrentBlock('browser');				
		$tmpl->setVariable('bloreg'		, $bloreg 		);
		$tmpl->setVariable('bloevireg'	, $bloevireg 	);
		$tmpl->setVariable('bloevifil'	, $bloevifil 	);
		$tmpl->setVariable('bloevifch'	, $bloevifch 	);
		$tmpl->setVariable('accusunom'	, $accusunom 	);
		$tmpl->setVariable('bloevifil'	, $bloevifil 	);
		$tmpl->setVariable('urlbloevifil', $pathimagenes.$bloreg.'/'.$bloevifil	);
		$tmpl->parseCurrentBlock();	
	}
	
	sql_close($conn);
	//--------------------------------------------------------------------------
	
	
	$tmpl->show();
?>
