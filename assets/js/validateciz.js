/**
 * Verificamos que todos los campos obligatorios
 */

$(document).ready( () => {

    $('#btnSubmit').prop("disabled", true);

    $('#nombre, #apellido, #correo, #contraseña,#sucursal').keyup( ()=> {
        let buttonDisabled = $('#nombre').val().length == 0 || $('#apellido').val().length == 0 || $('#correo').val().length == 0 || $('#contraseña').val().length == 0 || $('#sucursal').val().length == 0;
        $('#btnSubmit').prop("disabled", buttonDisabled);
    });
    //----

});
       

/**
 * Validamos los datos ingresados por el usuario con ajax
 * 
 * Recibimnos los datos los pasamos por php para validarlos
 * Mostramos msj de error en caso de que lo ingresado conincida con la bd
 * Mostramos un check en caso que no se encuentren resultados.
 */
// $('#nombre').change(function(){
   
//     let nombre =  $('#nombre').val();
//     if (nombre == '') {
//         nombre.html('');
//     }
//     //console.log(nombre);
//     let datos = {
//         "nombre":nombre
//     };
    

//     $.ajax({
//         url:'validate/valnombre.php',
//         method:'POST',
//         data:datos,
//         success:function(rsp){
//             data = $.parseJSON(rsp);
//             //console.log(data.errmsg);
//             if(data.errcod == 0){
						
//                 $('#test').addClass('fa fa-check');
//                 $('#msgerror').hide(data.errmsg);
//                 $("#btnSubmit").prop("disabled",false);
				
// 			}else{
				
//                 $('#msgerror').html(data.errmsg);
//                 $("#btnSubmit").prop("disabled",true);
// 			}
//         }

//     });

// });

$('#correo').change(function(){

    let correo =  $('#correo').val();
    if (correo == '') {
        correo.html('');
    }
    //console.log(correo);
    let datos = {
        "correo":correo
    };
    

    $.ajax({
        url:'validate/valcorreo.php',
        method:'POST',
        data:datos,
        success:function(rsp){
            data = $.parseJSON(rsp);
          
             if(data.errcod == 0){
						
                $('#test1').addClass('fa fa-check');
                $('#msgerrorcorreo').hide(data.errmsg);
                $("#btnSubmit").prop("disabled",false);
				
			 }else{
				
                 $('#msgerrorcorreo').html(data.errmsg);
                 $("#btnSubmit").prop("disabled",true);
                 
			 }
        }

    });


});
