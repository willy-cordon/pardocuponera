<?php include('/val/valuser.php'); 
//--------------------------------------------------------------------------------------------------------------
    	
			
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
			
	$tmpl= new HTML_Template_Sigma();	
    $tmpl->loadTemplateFile('miscupones.html');
    //Recibo el cupon canjeado desde  cupones.html
    $camitm = (isset($_POST['camitm']))? trim($_POST['camitm']) : 0;
    $camreg = (isset($_POST['camreg']))? trim($_POST['camreg']) : 0;

    date_default_timezone_set('America/Argentina/Buenos_Aires');
    
 
    function seg_a_dhms($seg) {  
        $d = floor($seg / 86400); 
          
    
    return "$d Días";  
    } 
   
    //Inicializamos varibles
    $cancodcup=2;

    //Cambiamos el estado del cupon  activo
    // $conn= sql_conectar();//Apertura de Conexion
    // $query="UPDATE CAM_DETA SET CANCODCUP=1 WHERE CAMITM=$camitm AND CAMREG=$camreg";
    // $Table	= sql_execute($query,$conn);

 
	//--------------------------------------------------------------------------------------------------------------
	$pathimagenes='sistema/imges/data/campania/';
        $conn= sql_conectar();//Apertura de Conexion
        
        $queryd="SELECT CD.CAMREG,CD.CAMITM,CD.CAMARTCOD,CD.CAMARTDES,CD.CAMARTDSC,CD.CAMARTIMP,CD.CAMIMGURL,CD.CAMIMAGEN,CD.CANCODCUP,CC.CAMVIGDDE,CC.CAMVIGHTA
                 FROM CAM_DETA CD
                 LEFT OUTER JOIN CAM_CABE CC ON CD.CAMREG=CC.CAMREG
                 WHERE CD.CANCODCUP=1
                 ";
		
            $Tabled	= sql_query($queryd,$conn);
            for($i=0; $i<$Tabled->Rows_Count; $i++){
            $row	= $Tabled->Rows[$i];
            //CAM_DETA
            $camreg         = trim($row['CAMREG']);
            $camitm 	    = trim($row['CAMITM']);
            $camartcod 	    = trim($row['CAMARTCOD']);
            $camartdes 	    = trim($row['CAMARTDES']);
            $camartdsc 	    = trim($row['CAMARTDSC']);
            $camartimp      = trim($row['CAMARTIMP']);
            $camimgurl      = trim($row['CAMIMGURL']);
            $camimagen      = trim($row['CAMIMAGEN']);
            $cancodcup      = trim($row['CANCODCUP']);
            $camvigdde  	= trim($row['CAMVIGDDE']);
            $camvighta  	= trim($row['CAMVIGHTA']);


                $tmpl->setCurrentBlock('activos');	
                    $tmpl->setVariable('camreg'			    , $camreg 		);
                    $tmpl->setVariable('camitm'			    , $camitm 		);
                    $tmpl->setVariable('camartcod'			, $camartcod 		);
                    $tmpl->setVariable('camartdes'			, $camartdes 		);
                    $tmpl->setVariable('camartdsc'			, $camartdsc 	);
                    $tmpl->setVariable('camartimp'			, $camartimp 	);
                    $tmpl->setVariable('camimgurl'			, $camimgurl 	);
                    $tmpl->setVariable('camimagen'	, $pathimagenes.$camreg.'/'.$camimagen 	);
                    $tmpl->setVariable('camvigdde'			, $camvigdde 	);
                $tmpl->setVariable('camvighta'			, $camvighta 	);

                    //
                    $fechaInicial = $camvigdde; 
                    $fechaFinal =$camvighta; 
                    $segundos = strtotime($fechaFinal) - strtotime($fechaInicial); 
                    
                    $tiempo_transcurrido = seg_a_dhms( $segundos ); 
                    if($tiempo_transcurrido > '24 days'){
                        logerror($tiempo_transcurrido); 
                    }
                    $tmpl->setVariable('tiempo'	, $tiempo_transcurrido 	);
                
                $tmpl->parseCurrentBlock('activos');	 
 
        }

        sql_close($conn);	
      
      $tmpl->show();
	

