<?php include('/val/valuser.php'); ?>
<?
	//--------------------------------------------------------------------------------------------------------------
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
    require_once GLBRutaFUNC.'/zfvarias.php';

    if (!isset($_SESSION[GLBAPPPORT.'PERCODIGO'])) {
      header('Location: login.php');
      exit; 
    }
			
	$tmpl= new HTML_Template_Sigma();	
    $tmpl->loadTemplateFile('cupones.html');
    //Recibimos los datos desde campañas
    $camreg = (isset($_GET['id']))? trim($_GET['id']) : 0;
    //Establecemos la zona horaria

    $pernombre = (isset($_SESSION[GLBAPPPORT.'PERNOMBRE']))? trim($_SESSION[GLBAPPPORT.'PERNOMBRE']) : '';

    $tmpl->setVariable('nombre',$pernombre);
   
	//--------------------------------------------------------------------------------------------------------------
	$pathimagenes='sistema/imges/data/campania/';
        $conn= sql_conectar();//Apertura de Conexion
        
        $queryd="SELECT CD.CAMREG,CD.CAMITM,CD.CAMARTCOD,CD.CAMARTDES,CD.CAMARTDSC,CD.CAMARTIMP,CD.CAMIMGURL,CD.CAMIMAGEN,CC.CAMVIGDDE,CC.CAMVIGHTA
                 FROM CAM_DETA CD 
                 LEFT OUTER JOIN CAM_CABE CC ON CD.CAMREG=CC.CAMREG
                 WHERE CD.CAMREG='$camreg' AND CC.CAMREG='$camreg' ";
		
            $Tabled	= sql_query($queryd,$conn);
            for($i=0; $i<$Tabled->Rows_Count; $i++){
            $row	= $Tabled->Rows[$i];
            //CAM_DETA
            $camreg         = trim($row['CAMREG']);
            $camitm 		= trim($row['CAMITM']);
            $camartcod 	    = trim($row['CAMARTCOD']);
            $camartdes 	    = trim($row['CAMARTDES']);
            $camartdsc 	    = trim($row['CAMARTDSC']);
            $camartimp  	= trim($row['CAMARTIMP']);
            $camimgurl  	= trim($row['CAMIMGURL']);
            $camimagen  	= trim($row['CAMIMAGEN']);
            $camvigdde  	= trim($row['CAMVIGDDE']);
            $camvighta  	= trim($row['CAMVIGHTA']);
            
            //Cambiamos la fecha en el pormato dd/mm/aa
			$camvigdde = substr($camvigdde,5,2).'/'.substr($camvigdde,8,2).'/'.substr($camvigdde,0,4); //Fecha
			$camvighta = substr($camvighta,5,2).'/'.substr($camvighta,8,2).'/'.substr($camvighta,0,4); //Fecha
            

            //CAM_DETA
            $tmpl->setCurrentBlock('cupones');	
            
                $tmpl->setVariable('camreg'			    , $camreg 		);
                $tmpl->setVariable('camitm'			    , $camitm 		);
                $tmpl->setVariable('camartcod'			, $camartcod 		);
                $tmpl->setVariable('camartdes'			, $camartdes 		);
                $tmpl->setVariable('camartdsc'			, $camartdsc 	);
                $tmpl->setVariable('camartimp'			, $camartimp 	);
                $tmpl->setVariable('camimgurl'			, $camimgurl 	);
                $tmpl->setVariable('camvigdde'			, $camvigdde 	);
                $tmpl->setVariable('camvighta'			, $camvighta 	);
                $tmpl->setVariable('camimagen'	        , $pathimagenes.$camreg.'/'.$camimagen 	);
                
            $tmpl->parseCurrentBlock('cupones');	 
            }

          

        sql_close($conn);	
      
      $tmpl->show();
	
?>	
