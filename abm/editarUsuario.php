<?php include('../val/valuser.php'); ?>
<?

		
require_once GLBRutaFUNC.'/sigma.php';	
require_once GLBRutaFUNC.'/zdatabase.php';
require_once GLBRutaFUNC.'/zfvarias.php';
        
$tmpl= new HTML_Template_Sigma();	
$tmpl->loadTemplateFile('editarUsuario.html');


$percodigo = (isset($_SESSION[GLBAPPPORT.'PERCODIGO']))? trim($_SESSION[GLBAPPPORT.'PERCODIGO']) : '';


$conn= sql_conectar();//Apertura de Conexion

$query="SELECT * FROM PER_MAEST WHERE PERCODIGO=$percodigo";
$execute=sql_query($query,$conn);
// logerror($execute);

for($i=0; $i<$execute->Rows_Count; $i++){
    $row	= $execute->Rows[$i];
    
    $percodigo     = trim($row['PERCODIGO']);
    $pernombre 	   = trim($row['PERNOMBRE']);
    $perapelli 	   = trim($row['PERAPELLI']);
    $perpassword   = trim($row['PERPASSWORD']);
    $percorreo 	   = trim($row['PERCORREO']);
    $persuc        = trim($row['PERSUC']);
   
    //Pasamos los datos al html
    $tmpl->setCurrentBlock('browser');	
        $tmpl->setVariable('percodigo'	    , $percodigo 	);
        $tmpl->setVariable('pernombre'	    , $pernombre 	);
        $tmpl->setVariable('perapelli'	    , $perapelli 	);
        $tmpl->setVariable('perpassword'    , $perpassword  );
        $tmpl->setVariable('percorreo'	    , $percorreo 	);
        $tmpl->setVariable('persuc'			, $persuc 	    );
    $tmpl->parseCurrentBlock('browser');
}


sql_close($conn);//Cierre de conexion	
      
$tmpl->show();