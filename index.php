<?php
	if(!isset($_SESSION))  session_start();
	include($_SERVER["DOCUMENT_ROOT"].'/pardocuponera/func/zglobals.php'); //DEV
	//include($_SERVER["DOCUMENT_ROOT"].'/func/zglobals.php'); //PRD
			
	require_once GLBRutaFUNC.'/sigma.php';	
	require_once GLBRutaFUNC.'/zdatabase.php';
	require_once GLBRutaFUNC.'/zfvarias.php';
			
	$tmpl= new HTML_Template_Sigma();	
	
	$tmpl->loadTemplateFile('index.html');
	$conn= sql_conectar();//Apertura de Conexion

	$percodigo = (isset($_SESSION[GLBAPPPORT.'PERCODIGO']))? trim($_SESSION[GLBAPPPORT.'PERCODIGO']) : '';
	
	$percorreo = (isset($_POST['correo']))? trim($_POST['correo']) : '';
	$pwd = (isset($_POST['contraseña']))? trim($_POST['contraseña']) : '';

		if($percodigo == ''){
			if($percorreo=='' || $pwd==''){
				header('Location: login.php');
				exit;
			}else{
				$queryv="SELECT * FROM PER_MAEST WHERE PERCORREO='$percorreo'";
				$tablev=sql_query($queryv,$conn);
				$row= $tablev->Rows[0];
				$contra = trim($row['PERPASSWORD']);
				if (password_verify($pwd,$contra)) {
					
					$query="SELECT PERCODIGO, PERNOMBRE, PERAPELLI, PERCORREO, PERPASSWORD, PERSUC, CAMITM, CAMARTCOD FROM PER_MAEST WHERE PERCORREO='$percorreo'";
					//logerror($query);
					$Table=sql_query($query,$conn);
					
					if($Table->Rows_Count>0){
						$row= $Table->Rows[0];
						$percodigo = trim($row['PERCODIGO']);
	
							$_SESSION[GLBAPPPORT.'PERCODIGO'] 	= $percodigo;
							$_SESSION[GLBAPPPORT.'PERNOMBRE'] 	= trim($row['PERNOMBRE']);
							$_SESSION[GLBAPPPORT.'PERAPELLI'] 	= trim($row['PERAPELLI']);
							$_SESSION[GLBAPPPORT.'PERCORREO'] 	= trim($row['PERCORREO']);
							$_SESSION[GLBAPPPORT.'PERPASSWORD'] = trim($row['PERPASSWORD']);;
							$_SESSION[GLBAPPPORT.'PERSUC'] 		= trim($row['PERSUC']);
							
					}
				}else{
					header('Location: login.php');
			}
	
		}

	}


$percodigo = (isset($_SESSION[GLBAPPPORT.'PERCODIGO']))? trim($_SESSION[GLBAPPPORT.'PERCODIGO']) : '';
$pernombre = (isset($_SESSION[GLBAPPPORT.'PERNOMBRE']))? trim($_SESSION[GLBAPPPORT.'PERNOMBRE']) : '';
$perapelli = (isset($_SESSION[GLBAPPPORT.'PERAPELLI']))? trim($_SESSION[GLBAPPPORT.'PERAPELLI']) : '';

/**
 * Mandamos los valores al html
 */

$tmpl->setVariable('percodigo',$percodigo)  ;
$tmpl->setVariable('nombre',$pernombre)  ;
$tmpl->setVariable('apellido',$perapelli);

	//--------------------------------------------------------------------------------------------------------------
	$pathimagenes='sistema/imges/data/campania/';
		$conn= sql_conectar();//Apertura de Conexion
		
		$query = "SELECT CAMREG,CAMID,CAMNRO,CAMCEM,CAMCODCUP,CAMDESCRI,CAMVIGDDE,CAMVIGDDH,CAMVIGHTA, CAMVIGHTH, CAMDSCRUB, CAMESTCUP, CAMIMAGEN, ESTCODIGO FROM CAM_CABE WHERE ESTCODIGO = 1 AND CAMIMAGEN != 'null'";
		
		$Table = sql_query($query,$conn);
		

			for($i=0; $i < $Table->Rows_Count; $i++){
				$row= $Table->Rows[$i];
	   
				$camimagen 	= trim($row['CAMIMAGEN']);
				$camreg 	= trim($row['CAMREG']);

				$tmpl->setCurrentBlock('cupon');
				$tmpl->setVariable('camreg'		,$camreg);
				$tmpl->setVariable('camimagen'	, $pathimagenes.$camreg.'/'.$camimagen 	);
				$tmpl->parseCurrentBlock('cupon');	
			}

		sql_close($conn);	
	
	$tmpl->show();
	

