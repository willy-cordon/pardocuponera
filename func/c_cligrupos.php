<?
//--------------------------------------------------------------------------
//Funciones de Carga de Combo de Grupos de Clientes
function CCliGrupos($cmbnom,$ctmpl,$cconn,$cligrpcod){ //   ( TEMPLATE , CONEXION )
			
	$empcodigo = $_SESSION[GLBAPPPORT.'EMPCODBVDSIS'];		//Codigo de Empresa
	$usucodigo = $_SESSION[GLBAPPPORT.'USRCODBVDSIS'];		//Codigo de Usuario
	$idicodint = $_SESSION[GLBAPPPORT.'IDIINTBVDSIS'];     	//Codigo de Idioma (Interno)
		
	$ctmpl->setVariable('cfg_style_select', CFG_Style_Select ); //Estilo de Selects Iniciales
		
	$query="SELECT CLIGRPCOD,CLIGRPCOD||'-'||CLIGRPDES AS CLIGRPDES
			FROM CLI_GRUP
			WHERE EMPCODIGO=$empcodigo AND ESTCODIGO=1 ";

	$Table= sql_query($query,$cconn);
	for($i=0; $i < $Table->Rows_Count; $i++){
		$row= $Table->Rows[$i];

		$codigo= trim($row['CLIGRPCOD']); //Codigo
		$descri= trim($row['CLIGRPDES']); //Descripcion
				
		$selecc= ''; //Opcion Seleccionada			
		if($codigo == $cligrpcod)	$selecc = 'selected';
				
		$ctmpl->setCurrentBlock($cmbnom);		
		$ctmpl->setVariable('codgrupo', $codigo );
		$ctmpl->setVariable('desgrupo', $descri );
		$ctmpl->setVariable('selgrupo', $selecc );
		$ctmpl->parseCurrentBlock();
	}
}
//--------------------------------------------------------------------------
?>
